from django.core.cache import cache
from django.contrib.auth.models import User
from user_profile.models import ApiKey, Profile, Doctor
from api.tools import get_user_topic


class MyCache(object):
    LONG_TIME = 60 * 60 * 24


class AuthCache(MyCache):
    TTL_TOKEN = 60 * 60
    TTL_USERNAME = 60 * 60 * 24

    @classmethod
    def get_username(cls, user_id):
        cun_str = "{}{}".format("uid", user_id)
        c_str = cache.get(cun_str)
        if c_str:
            return c_str

        if isinstance(user_id, (int, long)):
            try:
                user = User.objects.only('username').get(pk=user_id)
            except User.DoesNotExist:
                print "User with id {} does not exist ".format(user_id)
                return None

        username = user.username
        cache.set(cun_str, username, cls.TTL_USERNAME)
        return username

    @classmethod
    def get_id_from_username(cls, username):
        cun_str = "{}-{}".format("un", username)
        c_str = cache.get(cun_str)
        if c_str:
            return c_str

        try:
            profile = Profile.objects.only('user_id').get(medical_number=username)
        except User.DoesNotExist:
            print "User with username {} does not exist ".format(username)
            return None

        user_id = profile.user_id
        cache.set(cun_str, user_id)
        return username

    @classmethod
    def get_id_from_token(cls, token):
        if not token:
            return None

        ct_str = "tuid_{}".format(token)
        c_token = cache.get(ct_str)
        if c_token:
            return c_token
        else:
            try:
                api = ApiKey.objects.only('user_id').get(token=token)
            except Exception:
                return None
            cache.set(ct_str, api.user_id, cls.TTL_TOKEN)
            return api.user_id

    @classmethod
    def user_from_token(cls, token):
        if not token:
            return None
        try:
            api = ApiKey.objects.only("user").get(token=token)
            user = User.objects.only("id", "is_active", "is_superuser", "is_staff").get(id=api.user_id)
            if not user.is_active:
                return None
            return user
        except ApiKey.DoesNotExist:
            return None
        except User.DoesNotExist:
            return None

        return None

    @classmethod
    def get_profile_from_id(cls, user_id, cur_user=None):
        import json
        from api.tools import media_abs_url

        cun_str = "{}_{}".format("puid", user_id)
        c_str = cache.get(cun_str)
        if c_str:
            return json.loads(c_str)

        if isinstance(user_id, (int, long)):
            try:
                profile = Profile.objects.only('first_name', 'user',
                                               'last_name', 'avatar',
                                               'medical_number', 'is_doctor',
                                               'credit', 'id', 'phone').get(user_id=user_id)

                json_data = profile.get_json(fields=['first_name', 'last_name', 'medical_number', 'is_doctor', 'credit', 'phone'])
                json_data["user_id"] = profile.user.id
                json_data["id"] = profile.id
                json_data["is_approve"] = False
                json_data["expertise"] = ""
                json_data["topic"] = get_user_topic(profile.user.id)
                json_data["phone"] = ""

                if cur_user and cur_user == user_id:
                    json_data["phone"] = profile.phone

                if profile.avatar:
                    json_data["avatar"] = media_abs_url(profile.avatar.url)
                else:
                    json_data["avatar"] = ""

                # Get doctor info
                if profile.is_doctor:
                    try:
                        doctor = Doctor.objects.only('is_approve', 'expertise').get(user__id=user_id)
                        json_data["is_approve"] = doctor.is_approve
                        json_data["expertise"] = doctor.expertise
                    except Exception:
                        # print "auth_cach get_profile_from_id: {}".format(str(e))
                        pass

                # Get user status
                key = "{}:{}".format("med", json_data["medical_number"])
                user_status = cache.get(key)
                try:
                    json_status = json.loads(user_status)
                    json_data["status"] = json_status
                except Exception:
                    json_data["status"] = {}

                cache.set(cun_str, json.dumps(json_data), cls.TTL_USERNAME)
                return json_data
            except Profile.DoesNotExist:
                print "profile with user_id {} does not exist ".format(user_id)
                return None
        return None

    @classmethod
    def remove_profile_by_id(cls, user_id):
        cun_str = "{}_{}".format("puid", user_id)
        cache.delete(cun_str)
