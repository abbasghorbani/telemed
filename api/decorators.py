from api.auth_cache import AuthCache
from api.http_response import unauthorized_response, forbidden_response, not_allowed_response


def check_user_auth(function):
    def wrap(request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION', None)
        cur_user = None

        if token:
            cur_user = AuthCache.user_from_token(token=token)
        if not cur_user or not token:
            return unauthorized_response()

        request.CUR_USER = cur_user
        return function(request, *args, **kwargs)
    return wrap


def is_doctor(function):
    def wrap(request, *args, **kwargs):

        if request.CUR_USER is None:
            return unauthorized_response()

        profile = AuthCache.get_profile_from_id(user_id=request.CUR_USER.id)
        if profile is None:
            return forbidden_response()

        if profile["is_doctor"]:
            return function(request, *args, **kwargs)
        else:
            return forbidden_response()
    return wrap


def method_is_post(function):
    def wrap(request, *args, **kwargs):
        if request.method != "POST":
            return not_allowed_response()
        else:
            return function(request, *args, **kwargs)
    return wrap
