try:
    import simplejson as json
except ImportError:
    import json

from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder


def bad_request_response(message=_("Bad request"), status=400):
    data = {
        'status': status,
        'message': message,
    }
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=400)


def not_found_response(status=404, message=_("Not found")):
    data = {
        'status': status,
        'message': message,
    }
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=404)


def unauthorized_response(message=_("authentication failed"), status=401):
    data = {
        'status': status,
        'message': message,
    }
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=401)


def forbidden_response(message=_("Access denied"), status=403):
    data = {
        'status': status,
        'message': message,
    }
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=403)


def return_json_data(data={}, meta={}, message=""):
    value = {}
    value['meta'] = meta
    value['objects'] = data
    value['message'] = message
    jdata = json.dumps(value, cls=DjangoJSONEncoder)
    return HttpResponse(jdata, content_type='application/json')


def not_allowed_response(message=_("Method not allowed"), status=405):
    data = {
        'message': message,
        'status': 405
    }
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=405)
