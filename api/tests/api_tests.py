# -*- coding: utf-8 -*-
import unittest
import os
import sys
import time

from django.test import Client
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from user_profile.models import ActivationCode, Profile, ApiKey, Country, Doctor, Document, EducationType, Education
from medical.models import MedicalRecord, DrugType, Prescribe
from datetime import datetime
from chat.models_mongo import Topic
from invoice.models import Invoice


class AuthTestCase(unittest.TestCase):

    def setUp(self):
        self.client = Client()

    def tearDown(self):
        try:
            p = Profile.objects.get(phone="989127973634")
            User.objects.filter(id=p.user_id).delete()
        except Exception:
            pass
        super(AuthTestCase, self).tearDown()

    def test_send_code(self):
        time.sleep(settings.SLEEP_TIME)
        url = "http://127.0.0.1:8000/api/v1/auth/send/code/"
        payload = {
            "phone": "09127973634"
        }

        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 200)

    def test_verify_code(self):
        try:
            ActivationCode.create_code(phone="989127973634", count=0)
            activation_code = ActivationCode.objects.only('code').get(phone="989127973634")
        except Exception:
            self.assertRaises()
            return

        url = "http://127.0.0.1:8000/api/v1/auth/verify/code/"

        payload = {"phone": "09127973634", "code": str(activation_code.code)}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 401)

    def test_register(self):
        try:
            ActivationCode.create_code(phone="989127973634", count=0)
            ActivationCode.objects.filter(phone="989127973634").update(is_active=True)
        except Exception:
            self.assertRaises()
            return
        url = "http://127.0.0.1:8000/api/v1/auth/register/"

        payload = {
            "first_name": "amir",
            "last_name": "abbas ghorbani",
            "ssn": "0010706119",
            "phone": "09127973634"
        }

        response = self.client.post(url, payload)
        print response.json()
        self.assertEqual(response.status_code, 200)

    def test_resend_active_cdoe(self):
        time.sleep(settings.SLEEP_TIME)
        url = "http://127.0.0.1:8000/api/v1/auth/resend/code/"

        payload = {"phone": "09127973634"}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 200)


class UserProfileTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        try:
            self.user = User.objects.create_user(username="amir", email="a.abbasghorbani@gmail.com")
            self.profile = Profile.create_user_profile(first_name="amir",
                                                       last_name="abbas ghorbani",
                                                       phone="989127973634",
                                                       user=self.user, ssn="0010706119",
                                                       is_doctor=True)
            self.doctor = Doctor.objects.create(user_id=self.user.id, is_approve=True)
            self.api_key = ApiKey.objects.get(user=self.user)
        except Exception:
            self.assertRaises(TypeError, lambda: self.testListNone[:1])
            return

    def tearDown(self):
        self.user.delete()
        super(UserProfileTestCase, self).tearDown()

    def test_get_profile(self):
        url = "http://127.0.0.1:8000/api/v1/user/profile/"

        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token,
        }

        response = self.client.get(url, **headers)
        # print(response.json())
        self.assertEqual(response.status_code, 200)

    def test_update_profile(self):
        url = "http://127.0.0.1:8000/api/v1/user/profile/update/"

        path = os.path.join(settings.BASE_DIR, 'api/tests/a.jpg')
        image = open(path, "r")

        headers = {
            'content-type': "multipart/form-data",
            'HTTP_AUTHORIZATION': self.api_key.token
        }
        payload = {
            "first_name": "saeed",
            "last_name": "naghdi",
            "birthdate": "1989-07-21",
            "gender": "2",
            "married": "0",
            "height": "187",
            "avatar": image,
            "weight": "90"
        }
        response = self.client.post(url, payload, **headers)
        # print(response.json())
        self.assertEqual(response.status_code, 200)

    def test_update_address(self):
        country = Country.objects.create(title="tehran", search="tehran")
        url = "http://127.0.0.1:8000/api/v1/user/address/update/"

        payload = {
            "country": country.id,
            "postal_code": "12",
            "address": "nadaram",
            "phone": "40440127"}
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token,
            'content-type': "application/x-www-form-urlencoded"
        }

        response = self.client.post(url, payload, **headers)
        # print(response.json())
        self.assertEqual(response.status_code, 200)

    def test_state_list(self):

        url = "http://127.0.0.1:8000/api/v1/user/state/list/"

        payload = {"parent_id": "1"}
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token,
            'content-type': "application/x-www-form-urlencoded"
        }

        response = self.client.post(url, payload, **headers)
        # print(response.json())
        self.assertEqual(response.status_code, 200)

    def test_license_list(self):
        url = "http://127.0.0.1:8000/api/v1/user/license/list/"
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.get(url, **headers)
        self.assertEqual(response.status_code, 200)

    def test_update_license(self):
        self.doctor.is_approve = False
        self.doctor.save()

        url = "http://127.0.0.1:8000/api/v1/user/update/license/"

        payload = {
            "license_type": "1",
            "medical_license": "1234",
            "expertise": "prostate"
        }
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token,
            'content-type': "application/x-www-form-urlencoded"
        }

        response = self.client.post(url, payload, **headers)
        # print(response.json())
        self.assertEqual(response.status_code, 200)

    def test_upload_docment(self):
        url = "http://127.0.0.1:8000/api/v1/user/upload/document/"

        headers = {
            'content-type': "multipart/form-data",
            'HTTP_AUTHORIZATION': self.api_key.token
        }
        path = os.path.join(settings.BASE_DIR, 'api/tests/a.jpg')
        image = SimpleUploadedFile(name='test_image.jpg', content=open(path, 'rb').read(), content_type='image/jpeg')
        payload = {"name": "new_doc", "path": image}
        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_delete_document(self):
        image_path = os.path.join(settings.BASE_DIR, 'api/tests/a.jpg')
        image = SimpleUploadedFile(name='test_image.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')
        doc = Document.objects.create(name="ne_test", doctor_id=self.doctor.id, path=image)

        url = "http://127.0.0.1:8000/api/v1/user/delete/document/"

        payload = {"document_id": doc.id}
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_upload_education(self):
        image_path = os.path.join(settings.BASE_DIR, 'api/tests/a.jpg')
        image = SimpleUploadedFile(name='test_image.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')
        country = Country.objects.create(title="tehran", search="tehran")
        edu = EducationType.objects.create(name="Medical")

        url = "http://127.0.0.1:8000/api/v1/user/upload/education/"

        payload = {
            "univercity": "dsds",
            "country": country.id,
            "education_type": edu.id,
            "file_path": image,
            "desc": "dsdsdsd"
        }
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_delete_education(self):
        country = Country.objects.create(title="tehran", search="tehran")
        edu_type = EducationType.objects.create(name="Medical")
        image_path = os.path.join(settings.BASE_DIR, 'api/tests/a.jpg')
        image = SimpleUploadedFile(name='test_image.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')

        edu = Education.objects.create(univercity="tehran", country_id=country.id,
                                       education_type_id=edu_type.id, file_path=image,
                                       desc="sdsdsdd", doctor_id=self.user.id)

        url = "http://127.0.0.1:8000/api/v1/user/delete/education/"
        payload = {
            "education_id": edu.id
        }
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_education_list(self):
        url = "http://127.0.0.1:8000/api/v1/user/education/list/"
        payload = {"parent_id": "sd"}
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_doctor_request(self):
        url = "http://127.0.0.1:8000/api/v1/user/requestDoctor/"
        payload = {"text": "lskdlksldk"}
        headers = {
            'HTTP_AUTHORIZATION': self.api_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)


class MedicalRecordTestCase(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        try:
            self.patient = User.objects.create_user(username="amir", email="a.abbasghorbani@gmail.com")
            self.doctor = User.objects.create_user(username="saeed", email="sd.naghdi@gmail.com")
            self.profile_pat = Profile.create_user_profile(first_name="amir",
                                                           last_name="abbas ghorbani",
                                                           phone="989127973634",
                                                           user=self.patient,
                                                           ssn="0010706119",
                                                           is_doctor=False)

            self.profile_doc = Profile.create_user_profile(first_name="saeed",
                                                           last_name="naghdi",
                                                           phone="989127973635",
                                                           user=self.doctor,
                                                           ssn="0010706111",
                                                           is_doctor=True)

            Doctor.objects.create(user_id=self.doctor.id, is_approve=True)
            self.doc_key = ApiKey.objects.get(user=self.doctor.id)
            self.pat_key = ApiKey.objects.get(user=self.patient.id)
            self.medical = MedicalRecord.objects.create(patient_id=self.patient.id,
                                                        doctor_id=self.doctor.id,
                                                        start_session=datetime.now(),
                                                        end_session=datetime.now())
            topic = Topic.create_topic(title="test",
                                       members=[self.patient.id, self.doctor.id],
                                       medical_record=self.medical.id,
                                       owner=self.doctor.id,
                                       patient=self.patient.id)
            self.topic = topic.hashcode
        except Exception:
            self.assertRaises(TypeError, lambda: self.testListNone[:1])
            return

    def tearDown(self):
        Topic.objects.filter(hashcode=self.topic).delete()
        self.patient.delete()
        self.doctor.delete()
        super(MedicalRecordTestCase, self).tearDown()

    def test_close_medical(self):

        url = "http://127.0.0.1:8000/api/v1/medical/close/"

        payload = {
            "medical_id": self.medical.id,
            "description": "lskdlskd"
        }
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_update_medical_record(self):
        url = "http://127.0.0.1:8000/api/v1/medical/update/"

        payload = {
            "medical_id": self.medical.id,
            "description": "djkksdjsjd"
        }
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token,
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_filter_by_doctor(self):

        url = "http://127.0.0.1:8000/api/v1/medical/filterBy/doctor/"

        payload = {"offset": "0", "order": "-"}

        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.get(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_filter_by_patient(self):
        url = "http://127.0.0.1:8000/api/v1/medical/filterBy/patient/"

        payload = {"offset": "0", "order": "-"}
        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token
        }

        response = self.client.get(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_filter_patient_per_doctor(self):
        url = "http://127.0.0.1:8000/api/v1/medical/filter/patient/perDoctor/"

        payload = {"patient_id": self.patient.id, "offset": 0, "order": "-"}
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_patients_list(self):
        url = "http://127.0.0.1:8000/api/v1/medical/doctor/patients/list/"

        payload = {"offset": "0"}
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.get(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_get_dialogs(self):
        url = "http://127.0.0.1:8000/api/v1/medical/getDialogs/"
        payload = {"offset": "0"}
        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token,
        }

        response = self.client.get(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_create_prescribe(self):
        drug_type = DrugType.objects.create(title="test")
        url = "http://127.0.0.1:8000/api/v1/medical/createPrescribe/"

        payload = {
            "medical_id": self.medical.id,
            "drug_type": drug_type.id,
            "period": 30,
            "count": 12
        }
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token,
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_remove_prescribe(self):
        drug_type = DrugType.objects.create(title="test")
        prescribe = Prescribe.objects.create(medical_id=self.medical.id, drug_id=drug_type.id, period=25, count=30)

        url = "http://127.0.0.1:8000/api/v1/medical/removePrescribe/"
        payload = {
            "prescribe_id": prescribe.id,
            "medical_id": self.medical.id
        }
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_get_drugs(self):
        parent = DrugType.objects.create(title="drug")
        drug = DrugType.objects.create(title="drug test", parent_id=parent.id)

        url = "http://127.0.0.1:8000/api/v1/medical/getDrugs/"
        payload = {"drug_name": "drug", "parent_id": drug.id}
        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }
        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)

    def test_get_type(self):
        url = "http://127.0.0.1:8000/api/v1/medical/getPrescribeType/"

        headers = {
            'HTTP_AUTHORIZATION': self.doc_key.token
        }

        response = self.client.get(url, {}, **headers)
        self.assertEqual(response.status_code, 200)

    def test_drug_reminder(self):
        drug_type = DrugType.objects.create(title="test")
        prescribe = Prescribe.objects.create(medical_id=self.medical.id, drug_id=drug_type.id, period=25, count=30)

        url = "http://127.0.0.1:8000/api/v1/medical/drugReminder/"
        payload = {"status": "true", "prescribe_id": prescribe.id}

        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token
        }

        response = self.client.post(url, payload, **headers)
        self.assertEqual(response.status_code, 200)


class InvoiceTestCase(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        try:
            self.patient = User.objects.create_user(username="amir", email="a.abbasghorbani@gmail.com")
            self.doctor = User.objects.create_user(username="saeed", email="sd.naghdi@gmail.com")
            self.profile_pat = Profile.create_user_profile(first_name="amir",
                                                           last_name="abbas ghorbani",
                                                           phone="989127973634",
                                                           user=self.patient,
                                                           ssn="0010706119",
                                                           is_doctor=False)

            self.profile_doc = Profile.create_user_profile(first_name="saeed",
                                                           last_name="naghdi",
                                                           phone="989127973635",
                                                           user=self.doctor,
                                                           ssn="0010706111",
                                                           is_doctor=True)

            Doctor.objects.create(user_id=self.doctor.id, is_approve=True)
            self.pat_key = ApiKey.objects.get(user=self.patient.id)
            self.medical = MedicalRecord.objects.create(patient_id=self.patient.id,
                                                        doctor_id=self.doctor.id,
                                                        start_session=datetime.now(),
                                                        end_session=datetime.now())
            topic = Topic.create_topic(title="test",
                                       members=[self.patient.id, self.doctor.id],
                                       medical_record=self.medical.id,
                                       owner=self.doctor.id,
                                       patient=self.patient.id)
            self.topic = topic.hashcode
            self.invoice = Invoice.create_invoice(amount=5000, user_id=self.patient.id, content_object=self.medical)
        except Exception:
            self.assertRaises(TypeError, lambda: self.testListNone[:1])
            return

    def tearDown(self):
        Topic.objects.filter(hashcode=self.topic).delete()
        self.patient.delete()
        self.doctor.delete()
        super(InvoiceTestCase, self).tearDown()

    def test_show_invoice(self):
        url = "http://127.0.0.1:8000/api/v1/invoice/show/{}/".format(self.invoice.invoice_number)
        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token
        }

        response = self.client.get(url, {}, **headers)
        self.assertEqual(response.status_code, 200)

    def test_invoice_payment(self):
        profile = Profile.objects.get(user_id=self.patient.id)
        profile.credit = 10000
        profile.save()

        url = "http://127.0.0.1:8000/api/v1/invoice/show/{}/".format(self.invoice.invoice_number)

        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token
        }

        response = self.client.get(url, {}, **headers)
        self.assertEqual(response.status_code, 200)

    def test_user_invoices(self):
        url = "http://127.0.0.1:8000/api/v1/invoice/list/"

        headers = {
            'HTTP_AUTHORIZATION': self.pat_key.token,
        }

        response = self.client.get(url, {}, **headers)
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    # unittest.main()
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "telemed.settings_local")

    from django.core.management import execute_from_command_line

    if 'test' in sys.argv:
        settings.DEBUG = False
        settings.TEMPLATE_DEBUG = False
        settings.PASSWORD_HASHERS = [
            'django.contrib.auth.hashers.MD5PasswordHasher',
        ]
        settings.MIDDLEWARE_CLASSES = [
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
        ]

    if 'test' in sys.argv and '--time' in sys.argv:
        sys.argv.remove('--time')

    execute_from_command_line(sys.argv)
