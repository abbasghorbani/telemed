from django.conf.urls import url
from api.v1.appointment import views, views_doctor

urlpatterns = [
    url(r'freedate/(?P<user_id>\d+)/$', views.doctor_freedate, name='api-v1-appointment-doctor-freedate'),
    url(r'freeday/$', views.freeday, name='api-v1-appointment-freeday'),
    url(r'freeday/(?P<doctor_type>\w+)/$', views.freeday, name='api-v1-appointment-freeday'),
    url(r'availableDoctor/$', views.available_doctor, name='api-v1-appointment-available-doctor'),
    url(r'availableDoctor/(?P<doctor_type>\w+)/$', views.available_doctor, name='api-v1-appointment-available-doctor'),
    url(r'setAppointment/$', views.set_appointment, name='api-v1-appointment-set-appointment'),
    url(r'setAppointment/(?P<doctor_type>\w+)/$', views.set_appointment, name='api-v1-appointment-set-appointment'),
    url(r'setDoctorAppointment/(?P<user_id>\d+)/$', views.set_doctor_appointment, name='api-v1-appointment-set-doctor-appointment'),
    url(r'doctorCancelSession/$', views.doctor_cancel_session, name='api-v1-appointment-doctor-cancel-session'),
    url(r'doctorCancelTime/$', views.doctor_cancel_time, name='api-v1-appointment-doctor-cancel-time'),
    url(r'doctorCancelDay/$', views.doctor_cancel_day, name='api-v1-appointment-doctor-cancel-day'),
    url(r'patientCancelSession/$', views.patient_cancel_session, name='api-v1-appointment-patient-cancel-session'),

    url(r'source/doctorSetDate/$', views_doctor.doctor_set_date, name='api-v1-appointment-doctor-set-date'),
    url(r'source/doctorRemoveDate/$', views_doctor.doctor_remove_date, name='api-v1-appointment-doctor-remove-date'),
    url(r'source/doctorRemoveDay/$', views_doctor.doctor_remove_day, name='api-v1-appointment-doctor-remove-day'),

    url(r'onlineAppointmentStatus/$', views.get_online_appointment_status, name='api-v1-appointment-online-appointment-status'),
    url(r'onlineAppointmentStatus/(?P<doctor_type>\w+)/$', views.get_online_appointment_status, name='api-v1-appointment-online-appointment-status'),
    url(r'setOnlineAppointment/$', views.online_appointment, name='api-v1-appointment-online-appointment'),
    url(r'setOnlineAppointment/(?P<doctor_type>\w+)/$', views.online_appointment, name='api-v1-appointment-online-appointment'),
    url(r'setOnlineDoctorAppointment/(?P<user_id>\d+)/$', views.online_doctor_appointment, name='api-v1-appointment-online-doctor-appointment'),
    url(r'returnMony/(?P<medical_record_id>\d+)/$', views.return_mony, name='api-v1-appointment-return-mony'),
]
