# -*- coding: utf-8 -*-
# from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
# from django.db.models import Count
from django.utils.translation import ugettext as _
from datetime import datetime, timedelta
from django.db.models import Q

from appointment.models_mongo import DoctorFreeDate, FreeDay
from medical.models import MedicalRecord, PENDING, APPROVE, STARTED
from api.http_response import bad_request_response, return_json_data, not_allowed_response, not_found_response, forbidden_response
from api.decorators import check_user_auth, method_is_post
from api.tools import get_time_int, get_int, get_current_time_int, get_doctor, get_type_time_int, is_valid_time_int
from user_profile.models import Doctor, QUARTER, HALF, HOUR, HOUR_HALF, PSYCHIATRIST, MEDICAL, PSYCHOLOGIST, Profile
from invoice.models import Invoice
from api.auth_cache import AuthCache


@csrf_exempt
@method_is_post
@check_user_auth
def doctor_freedate(request, user_id):

    doctor = get_doctor(user_id)
    if not doctor:
        return bad_request_response(message=_("Doctor not found."))

    appointment_date = request.POST.get("appointment_date", 0)
    appointment_type = get_int(request.POST.get("appointment_type", 15))
    today = datetime.today().date()

    if (doctor.appointment_type == HOUR_HALF and appointment_type not in [HALF, HOUR]) or (doctor.appointment_type != HOUR_HALF and doctor.appointment_type != appointment_type):
        return not_found_response(message=_("Invalid appointment type."))

    try:
        appointment_day = datetime.strptime(appointment_date, '%Y-%m-%d').date()
    except Exception:
        return bad_request_response(message=_("Invalid appointment date"))

    if appointment_day < today:
        return bad_request_response(message=_("Invalid datetime"))

    data = {}
    allObj = {}

    try:
        freedateObj = DoctorFreeDate.objects.only("quarter", "half", "hour").get(user=doctor.user_id, day=appointment_day)
        cur_time_int = get_current_time_int(appointment_type) + 1
        if appointment_type == QUARTER:
            hours = freedateObj.quarter
        elif appointment_type == HALF:
            hours = freedateObj.half
        elif appointment_type == HOUR:
            hours = freedateObj.hour

        hours = list(set(hours))
        hours.sort()
        i = 0
        for hour in hours:
            hour = get_int(hour)
            if today == appointment_day and cur_time_int > hour:
                continue
            allObj[i] = {"time_int": hour, "start_end": get_time_int(hour, appointment_type)}
            i += 1
    except DoctorFreeDate.DoesNotExist:
        pass
    except Exception as e:
        print str(e), "|| DoctorFreeDate models_mongo, doctor_freedate function"

    data = allObj

    return return_json_data(data)


@csrf_exempt
@check_user_auth
def freeday(request, doctor_type='medical'):
    if request.method != "POST":
        return not_allowed_response()

    appointment_date = request.POST.get("appointment_date", 0)
    appointment_type = get_int(request.POST.get("appointment_type", 15))
    today = datetime.today().date()

    if (doctor_type in ['psychiatrist', 'psychologist'] and appointment_type not in [HALF, HOUR]) or (doctor_type == 'medical' and appointment_type != QUARTER):
        return not_found_response(message=_("invalid appointment type."))
    if doctor_type == "medical":
        licenseType = MEDICAL
    elif doctor_type == "psychiatrist":
        licenseType = PSYCHIATRIST
    elif doctor_type == "psychologist":
        licenseType = PSYCHOLOGIST

    try:
        appointment_day = datetime.strptime(appointment_date, '%Y-%m-%d').date()
    except Exception:
        return bad_request_response()

    if appointment_day < today:
        return bad_request_response(message=_("bad datetime."))

    data = {}
    allObj = {}

    try:
        freedateObj = FreeDay.objects.only("quarter", "half", "hour").get(day=appointment_day, doctor_type=licenseType)
        cur_time_int = get_current_time_int(appointment_type) + 1
        if appointment_type == QUARTER:
            hours = freedateObj.quarter
        elif appointment_type == HALF:
            hours = freedateObj.half
        elif appointment_type == HOUR:
            hours = freedateObj.hour
        hours = list(set(hours))
        hours.sort()
        i = 0
        for hour in hours:
            hour = get_int(hour)
            if today == appointment_day and cur_time_int > hour:
                continue
            allObj[i] = {"time_int": hour, "start_end": get_time_int(hour, appointment_type)}
            i += 1
    except FreeDay.DoesNotExist:
        pass
    except Exception as e:
        print str(e), "|| FreeDay models_mongo, freedate function"

    data = allObj

    return return_json_data(data)


@csrf_exempt
@check_user_auth
def available_doctor(request, doctor_type='medical'):
    if doctor_type == "medical":
        licenseType = MEDICAL
    elif doctor_type == "psychiatrist":
        licenseType = PSYCHIATRIST
    elif doctor_type == "psychologist":
        licenseType = PSYCHOLOGIST

    allDoctor = []
    doctors = Doctor.objects.filter(is_approve=True, license_type=licenseType)
    for doctor in doctors:
        data = {}
        data = doctor.get_json()
        data['next_available'] = doctor.next_available(appointment_type=doctor.license_type)
        data['user_id'] = data["user"]
        data.pop('user')
        allDoctor.append(data)
    return return_json_data(data=allDoctor)


@csrf_exempt
@check_user_auth
def set_appointment(request, doctor_type='medical'):

    if request.method != "POST":
        return not_allowed_response()

    appointment_date = request.POST.get("appointment_date", None)  # given date, in format '2018-01-22'
    time_int = get_int(request.POST.get("time_int", 0))
    appointment_type = get_int(request.POST.get("appointment_type", 15))

    if (doctor_type in ['psychiatrist', 'psychologist'] and appointment_type not in [HALF, HOUR]) or (doctor_type == 'medical' and appointment_type != QUARTER):
        return not_found_response(message=_("invalid appointment type."))
    if doctor_type == "medical":
        licenseType = MEDICAL
    elif doctor_type == "psychiatrist":
        licenseType = PSYCHIATRIST
    elif doctor_type == "psychologist":
        licenseType = PSYCHOLOGIST

    if not is_valid_time_int(time_int, appointment_type):
        return not_found_response(message=_("invalid time int."))

    try:
        start, end = get_time_int(time_int, appointment_type, single=False)
        start_appointment = datetime.strptime(appointment_date + " " + start, "%Y-%m-%d %H:%M")
        end_appointment = datetime.strptime(appointment_date + " " + end, "%Y-%m-%d %H:%M")
    except Exception:
        return bad_request_response()

    appointment_day = start_appointment.date()

    # need to refact -- check patient have no session in this time -- bad query
    if MedicalRecord.objects.filter(Q(patient_id=request.CUR_USER.id, visit_day=appointment_day) and Q(Q(start_session__gte=start_appointment, start_session__lt=end_appointment) | Q(end_session__gt=start_appointment, start_session__lte=start_appointment))).count() > 0:
        return bad_request_response(message=_("reserve error, you already reserved this time."))

    # need to refact -- check doctor have no session in this time -- bad query
    if request.CUR_USER.profile.is_doctor and MedicalRecord.objects.filter(Q(doctor_id=request.CUR_USER.id, visit_day=appointment_day) and Q(Q(start_session__gte=start_appointment, start_session__lt=end_appointment) | Q(end_session__gt=start_appointment, start_session__lte=start_appointment))).count() > 0:
        return bad_request_response(message=_("reserve error, you have patient at this time."))

    # if start_appointment < datetime.today() + timedelta(minutes=15):
    if start_appointment < datetime.today():
        return bad_request_response(message=_("reserve error, this time is not available."))

    if appointment_type == QUARTER:
        app_filter = {"day": appointment_day, "quarter": time_int, "doctor_type": licenseType}
    elif appointment_type == HALF:
        app_filter = {"day": appointment_day, "half": time_int, "doctor_type": licenseType}
    elif appointment_type == HOUR:
        app_filter = {"day": appointment_day, "hour": time_int, "doctor_type": licenseType}
    else:
        return not_found_response(message=_("invalid appointment type."))

    available_time_int = FreeDay.objects.filter(**app_filter).count()
    if available_time_int == 0:
        return not_found_response(message=_("no free time at this time, please try again."))

    free_doctors = list(DoctorFreeDate.objects.only("user").filter(**app_filter).values_list("user",flat=True))

    if len(free_doctors) == 0:
        FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=appointment_type, doctor_type=licenseType)
        return not_found_response(message=_("find doctor error, please try again."))

    # start find doctor
    # need to refact -- recheck doctor have no patient in this time -- bad query
    visit_count = 0
    free_doctor = None

    # check doctor not reserver himself
    try:
        free_doctors.remove(request.CUR_USER.id)
    except Exception:
        pass

    for doctor in free_doctors:
        freeObj = MedicalRecord.objects.only("start_session", "end_session").filter(visit_day=appointment_day, doctor_id=doctor)
        doctor_visit_count = freeObj.count()

        # if freeObj.filter(Q(start_session__gte=start_appointment, start_session__lt=end_appointment)|Q(end_session__gt=start_appointment, start_session__lte=start_appointment)).count() > 0:
        #     doctor_obj = get_doctor(doctor)
        #     DoctorFreeDate.remove_time(day=appointment_day, time_int=time_int,doctor_id=doctor,doctor_type=doctor_obj.license_type)
        #     continue

        if doctor_visit_count <= visit_count and free_doctor is not None:
            visit_count = doctor_visit_count
            free_doctor = doctor
        else:
            free_doctor = doctor
    # end find doctor

    if free_doctor is None:
        FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=appointment_type, doctor_type=licenseType)
        return not_found_response(message=_("no available doctor at this time, please try again."))

    # Create medical record
    medical_record = MedicalRecord.objects.create(patient_id=request.CUR_USER.id, doctor_id=free_doctor, start_session=start_appointment, end_session=end_appointment, visit_day=appointment_day)

    # Get medical invoice
    invoice = Invoice.create_invoice(user_id=request.CUR_USER.id, amount=5000, content_object=medical_record)
    if invoice is None:
        return not_found_response(message=_("Invoice not found"))

    doctor_obj = get_doctor(free_doctor)
    # Remove time from doctor free time
    DoctorFreeDate.remove_time(day=appointment_day, time_int=time_int, doctor_id=free_doctor, appointment_type=appointment_type, doctor_type=doctor_obj.license_type)

    data = {"medical": medical_record.get_json(), "invoice": invoice.get_json()}
    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
def set_doctor_appointment(request, user_id):
    if request.method != "POST":
        return not_allowed_response()

    if user_id == request.CUR_USER.id:
        return bad_request_response(message=_("Invalid parameters."))

    doctor = get_doctor(user_id)
    if not doctor:
        return bad_request_response(message=_("Doctor not found"))

    if doctor.user_id == request.CUR_USER.id:
        return bad_request_response(message=_("Error in get appointment."))

    appointment_date = request.POST.get("appointment_date", 0)  # given date, in format '2018-01-22'
    time_int = get_int(request.POST.get("time_int", 0))
    appointment_type = get_int(request.POST.get("appointment_type", 15))

    if (doctor.appointment_type == HOUR_HALF and appointment_type not in [HALF, HOUR]) or (doctor.appointment_type != HOUR_HALF and doctor.appointment_type != appointment_type):
        return not_found_response(message=_("Invalid appointment type."))

    if not is_valid_time_int(time_int, appointment_type):
        return not_found_response(message=_("Invalid time int."))

    try:
        start, end = get_time_int(time_int, appointment_type, single=False)
        start_appointment = datetime.strptime(appointment_date + " " + start, "%Y-%m-%d %H:%M")
        end_appointment = datetime.strptime(appointment_date + " " + end, "%Y-%m-%d %H:%M")
    except Exception:
        return bad_request_response(message=_("Datetime error."))

    appointment_day = start_appointment.date()

    print start_appointment, datetime.today() + timedelta(minutes=15)

    if start_appointment < datetime.today() + timedelta(minutes=1):
        return bad_request_response(message=_("reserve error, this time is not available."))

    # need to refact -- check patient have no session in this time -- bad query
    if MedicalRecord.objects.filter(Q(patient_id=request.CUR_USER.id, visit_day=appointment_day) and Q(Q(start_session__gte=start_appointment, start_session__lt=end_appointment) | Q(end_session__gt=start_appointment, start_session__lte=start_appointment))).count() > 0:
        return bad_request_response(message=_("reserve error, you already reserved this time."))

    # need to refact -- check doctor have no session in this time -- bad query
    if request.CUR_USER.profile.is_doctor and MedicalRecord.objects.filter(Q(doctor_id=request.CUR_USER.id, visit_day=appointment_day) and Q(Q(start_session__gte=start_appointment, start_session__lt=end_appointment) | Q(end_session__gt=start_appointment, start_session__lte=start_appointment))).count() > 0:
        return bad_request_response(message=_("reserve error, you have patient at this time."))

    if appointment_type == QUARTER:
        app_filter = {"user": doctor.user_id, "day": appointment_day, "quarter": time_int}
    elif appointment_type == HALF:
        app_filter = {"user": doctor.user_id, "day": appointment_day, "half": time_int}
    elif appointment_type == HOUR:
        app_filter = {"user": doctor.user_id, "day": appointment_day, "hour": time_int}
    else:
        return not_found_response(message=_("invalid appointment type."))

    try:
        is_avalaible = DoctorFreeDate.objects.only("day").get(**app_filter)
    except Exception:
        is_avalaible = False

    if not is_avalaible:
        return not_found_response(message=_("doctor is not available"))

    medical_record = MedicalRecord.objects.create(patient_id=request.CUR_USER.id, doctor_id=doctor.user_id, start_session=start_appointment, end_session=end_appointment, visit_day=appointment_day)

    # Get medical invoice
    invoice = Invoice.create_invoice(user_id=request.CUR_USER.id, amount=5000, content_object=medical_record)
    if invoice is None:
        return not_found_response(message=_("Invoice not set."))

    DoctorFreeDate.remove_time(day=appointment_day, time_int=time_int, doctor_id=doctor.user_id, appointment_type=appointment_type, doctor_type=doctor.license_type)
    data = {"medical": medical_record.get_json(), "invoice": invoice.get_json()}

    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
def doctor_cancel_session(request):
    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    if request.method != "POST" or not doctor:
        return not_allowed_response()

    session_id = request.POST.get("medical_record", None)

    try:
        session_id = get_int(session_id)
        medical_record = MedicalRecord.objects.get(doctor_id=request.CUR_USER.id, id=session_id, status__in=[APPROVE, PENDING])
    except Exception as e:
        print str(e)
        return bad_request_response(message=_("session not set."))

    if medical_record.start_session <= datetime.today():
        return bad_request_response(message=_("session canceled error."))

    medical_record.return_mony()
    medical_record.delete()

    return return_json_data(message=_("session canceled successfully"))


@csrf_exempt
@check_user_auth
def doctor_cancel_time(request):
    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    if request.method != "POST" or not doctor:
        return not_allowed_response()

    start_date = request.POST.get("start_date", 0)  # given date, in format '2018-01-22 18:30'
    end_date = request.POST.get("end_date", 0)  # given date, in format '2018-01-22 18:30'

    try:
        start_appointment = datetime.strptime(start_date, "%Y-%m-%d %H:%M")
        end_appointment = datetime.strptime(end_date, "%Y-%m-%d %H:%M")
        start_day = start_appointment.date()
        end_day = end_appointment.date()
    except Exception:
        return bad_request_response()

    if start_appointment <= datetime.today():
        return bad_request_response(message=_("time cancel error."))

    if start_day != end_day:
        return bad_request_response(message=_("date range must be in the same day."))

    if doctor.appointment_type != HOUR_HALF:
        time_int_list = get_type_time_int(doctor.appointment_type, start_appointment, end_appointment)
        print time_int_list
        print start_appointment, end_appointment
        for time_int in time_int_list:
            DoctorFreeDate.remove_time(day=start_day, time_int=time_int, doctor_id=request.CUR_USER.id, appointment_type=doctor.appointment_type, doctor_type=doctor.license_type)
    elif doctor.appointment_type == HOUR_HALF:
        time_int_list = get_type_time_int(HALF, start_appointment, end_appointment)
        for time_int in time_int_list:
            DoctorFreeDate.remove_time(day=start_day, time_int=time_int, doctor_id=request.CUR_USER.id, appointment_type=HALF, doctor_type=doctor.license_type)

    medical_record = MedicalRecord.objects.filter(visit_day=start_day, start_session__gte=start_appointment, start_session__lte=end_appointment, doctor_id=request.CUR_USER.id)
    for medical in medical_record:
        medical.return_mony()

    medical_record.delete()

    return return_json_data(message=_("time canceled successfully"))


@csrf_exempt
@check_user_auth
def doctor_cancel_day(request):
    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    if request.method != "POST" or not doctor:
        return not_allowed_response()

    appointment_date = request.POST.get("appointment_date", 0)  # given date, in format '2018-01-22'

    try:
        appointment_day = datetime.strptime(appointment_date, '%Y-%m-%d').date()
    except Exception:
        return bad_request_response()

    if appointment_day <= datetime.today().date():
        return bad_request_response(message=_("time cancel error."))

    try:
        doctor_free_date = DoctorFreeDate.objects.get(day=appointment_day, user=request.CUR_USER.id)
        if doctor.appointment_type == QUARTER:
            hours = doctor_free_date.quarter
            for time_int in hours:
                FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=doctor.appointment_type, doctor_type=doctor.license_type)
        elif doctor.appointment_type == HALF:
            hours = doctor_free_date.half
            for time_int in hours:
                FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=doctor.appointment_type, doctor_type=doctor.license_type)
        elif doctor.appointment_type == HOUR:
            hours = doctor_free_date.hour
            for time_int in hours:
                FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=doctor.appointment_type, doctor_type=doctor.license_type)
        elif doctor.appointment_type == HOUR_HALF:
            hours = doctor_free_date.half
            for time_int in hours:
                FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=HALF, doctor_type=doctor.license_type)
            hours = doctor_free_date.hour
            for time_int in hours:
                FreeDay.remove_time(day=appointment_day, time_int=time_int, appointment_type=HOUR, doctor_type=doctor.license_type)
        doctor_free_date.delete()
    except Exception as e:
        print str(e), "|| next available, user_profile models"

    medical_record = MedicalRecord.objects.filter(visit_day=appointment_day, doctor_id=request.CUR_USER.id)
    for medical in medical_record:
        medical.return_mony()

    medical_record.delete()

    return return_json_data(message=_("time canceled successfully"))


@csrf_exempt
@check_user_auth
def patient_cancel_session(request):
    if request.method != "POST":
        return not_allowed_response()

    session_id = request.POST.get("medical_record", None)

    if session_id is None:
        return bad_request_response(message=_("cancel error, medical_record not set."))

    try:
        session_id = get_int(session_id)
        medical_record = MedicalRecord.objects.get(id=session_id, patient_id=request.CUR_USER.id, status__in=[APPROVE, PENDING])
    except Exception:
        return bad_request_response(message=_("cancel error, get reserved time error."))

    # if medical_record.start_session < datetime.today() + timedelta(minutes=15):
    if medical_record.start_session < datetime.today():
        return bad_request_response(message=_("cancel error, You must act more than an hour earlier."))

    doctor = get_doctor(medical_record.doctor_id)
    if not doctor:
        return bad_request_response()

    if doctor.appointment_type != HOUR_HALF:
        time_int_list = get_type_time_int(doctor.appointment_type, medical_record.start_session, medical_record.end_session)
        for time_int in time_int_list:
            DoctorFreeDate.append_time(medical_record.visit_day, time_int, doctor.user_id, doctor.appointment_type, doctor.license_type)
    elif doctor.appointment_type == HOUR_HALF:
        half_time_int_list = get_type_time_int(HALF, medical_record.start_session, medical_record.end_session)
        for time_int in half_time_int_list:
            DoctorFreeDate.append_time(medical_record.visit_day, time_int, doctor.user_id, HALF, doctor.license_type)

    medical_record.return_mony()
    medical_record.delete()

    return return_json_data(message=_("time canceled successfully"))


@csrf_exempt
@check_user_auth
def get_online_appointment_status(request, doctor_type='medical'):
    now = datetime.now()
    later = now + timedelta(minutes=+15)
    need_increase = False
    profile = AuthCache.get_profile_from_id(user_id=request.CUR_USER.id)
    if profile["credit"] < 5000:
        need_increase = True

    free_doctors = list(Doctor.objects.filter(is_available=True, license_type=MEDICAL, status=Doctor.AVAILABLE).values_list("user",flat=True).exclude(user_id=request.CUR_USER.id))
    none_free_doctors = list(MedicalRecord.objects.filter(end_session__gte=now, start_session__lte=later, doctor_id__in=free_doctors, status__in=[APPROVE, STARTED]).values_list("doctor",flat=True))

    available_doctors_count = len(set(free_doctors) - set(none_free_doctors))
    if available_doctors_count > 0:
        data = {}
        data["message"] = _("Doctor is available")
        data["available_count"] = available_doctors_count
        data["need_increase"] = need_increase
        data["credit"] = profile["credit"]
        data["amount"] = 5000
        return return_json_data(data=data)
    else:
        return not_found_response(message=_("No available Doctor now, Please try again later."))


@csrf_exempt
@check_user_auth
def online_appointment(request, doctor_type='medical'):
    now = datetime.now()
    later = now + timedelta(minutes=+15)
    profile = AuthCache.get_profile_from_id(user_id=request.CUR_USER.id)
    if profile["credit"] < 5000:
        return forbidden_response(message=_("please increase your credit before request to appointment."))

    appointment_day = datetime.now().date()

    free_doctors = list(Doctor.objects.filter(is_available=True, license_type=MEDICAL, status=Doctor.AVAILABLE).values_list("user",flat=True).exclude(user_id=request.CUR_USER.id))
    none_free_doctors = list(MedicalRecord.objects.filter(end_session__gte=now, start_session__lte=later, doctor_id__in=free_doctors, status__in=[APPROVE, STARTED]).values_list("doctor",flat=True))

    available_doctors = set(free_doctors) - set(none_free_doctors)

    # manage select doctor that have less visit today
    visit_count = 0
    free_doctor = None
    try:
        available_doctors.remove(request.CUR_USER.id)
    except Exception:
        pass

    for doctor in available_doctors:
        doctor_visit_count = MedicalRecord.objects.filter(visit_day=appointment_day, doctor_id=doctor).count()

        if doctor_visit_count <= visit_count and free_doctor is not None:
            visit_count = doctor_visit_count
            free_doctor = doctor
        else:
            free_doctor = doctor

    if free_doctor is None:
        return not_found_response(message=_("no available doctor at this time, please try again."))
    # end of select doctor

    # Create medical record
    medical_record = MedicalRecord.objects.create(patient_id=request.CUR_USER.id, doctor_id=free_doctor, start_session=now, end_session=later, visit_day=appointment_day)

    # Get medical invoice
    invoice = Invoice.create_invoice(user_id=request.CUR_USER.id, amount=5000, content_object=medical_record)
    if invoice is None:
        return not_found_response(message=_("Invoice not found"))

    result = Profile.dec_credit(user_id=request.CUR_USER.id, amount=invoice.amount)
    if not result:
        return forbidden_response(message=_("Error occurred in reducing credit"))

    invoice.status = Invoice.PAID
    invoice.save()
    invoice.after_paid_invoice(online=True)
    # start send notification
    data = {"medical": medical_record.get_json()}

    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
def online_doctor_appointment(request, user_id):
    """
        Get a doctor user_id and check if this doctor is available now?.
        If the doctor is available , you will have time to meet the doctor.
    """
    cur_user_id = request.CUR_USER.id
    profile = AuthCache.get_profile_from_id(user_id=cur_user_id)
    if profile["credit"] < 5000:
        return bad_request_response(message=_("please increase your credit before request to appointment."))

    doctor = get_doctor(user_id)
    if not doctor or user_id == cur_user_id:
        return bad_request_response(message=_("doctor not found."))

    if not doctor.is_available:
        return bad_request_response(message=_("doctor is not available now. try schadule appointment to visit him."))

    now = datetime.now()
    later = now + timedelta(minutes=+15)
    busy_medical_count = MedicalRecord.objects.filter(doctor_id=user_id, start_session__lte=later, start_session__gte=now).count()
    if busy_medical_count > 0:
        return bad_request_response(message=_("doctor is busy now. try schadule appointment to visit him."))

    medical_record = MedicalRecord.objects.create(patient_id=cur_user_id, doctor_id=user_id, start_session=now, end_session=later, visit_day=now.date())

    # Get medical invoice
    invoice = Invoice.create_invoice(user_id=cur_user_id, amount=5000, content_object=medical_record)
    if invoice is None:
        return not_found_response(message=_("Invoice not found"))

    result = Profile.dec_credit(user_id=cur_user_id, amount=invoice.amount)
    if not result:
        return bad_request_response(message=_("Error occurred in reducing credit"))

    invoice.status = Invoice.PAID
    invoice.save()
    Invoice.after_paid_invoice(online=True)
    # start send notification
    data = {"medical_record":  medical_record.get_json()}
    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
def return_mony(request, medical_record_id):
    """
        Doctor can return mony
    """
    cur_user_id = request.CUR_USER.id
    doctor = get_doctor(cur_user_id)
    if not doctor:
        return bad_request_response(message="Doctor not found.")

    medical_record = MedicalRecord.get_medical_record(medical_id=medical_record_id)
    if medical_record is None or medical_record.status == PENDING:
        return bad_request_response(message="Invalid  request.")

    if medical_record.doctor_id != cur_user_id:
        return bad_request_response(message="Error in parameters.")

    invoice = Invoice.get_invoice_by_medical_id(medical_record_id)
    if not invoice:
        return bad_request_response(message="Invoice not found, please contact us.")

    invoice.return_mony(cancel=False)

    data = {"medical_record": medical_record.get_json()}
    return return_json_data(data=data)
