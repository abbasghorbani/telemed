# -*- coding: utf-8 -*-
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from datetime import datetime

from api.decorators import check_user_auth
from appointment.models_mongo import SourceFreeDate, DAYS_LIST, DoctorFreeDate, FreeDay
from api.http_response import bad_request_response, return_json_data, not_allowed_response
from api.tools import get_int, get_doctor, get_type_time_int, get_next_weekday
from user_profile.models import QUARTER, HALF, HOUR, HOUR_HALF, PSYCHIATRIST, MEDICAL, PSYCHOLOGIST


@csrf_exempt
@check_user_auth
def doctor_set_date(request):
    if request.method != "POST":
        return not_allowed_response()

    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    appointment_day = get_int(request.POST.get("appointment_day", 0))
    start_date = request.POST.get("start_date", "")  # given date, in format '18:30'
    end_date = request.POST.get("end_date", "")  # given date, in format '18:30'

    if appointment_day not in DAYS_LIST:
        return bad_request_response(message=_("appointment day is not in range"))

    try:
        appointment_date = datetime.today().strftime('%Y-%m-%d')
        start_appointment = datetime.strptime(appointment_date + " " + start_date, "%Y-%m-%d %H:%M")
        end_appointment = datetime.strptime(appointment_date + " " + end_date, "%Y-%m-%d %H:%M")
    except Exception:
        return bad_request_response()

    if start_appointment > end_appointment:
        return bad_request_response()

    try:
        source = SourceFreeDate.objects.get(user=request.CUR_USER.id, day=appointment_day, doctor_type=doctor.license_type)
    except Exception:
        source = SourceFreeDate.objects.create(user=request.CUR_USER.id, day=appointment_day, doctor_type=doctor.license_type)

    if doctor.appointment_type != HOUR_HALF:
        time_int_list = get_type_time_int(doctor.appointment_type, start_appointment, end_appointment)
        if doctor.appointment_type == QUARTER:
            for time_int in time_int_list:
                source.quarter.append(time_int)
        elif doctor.appointment_type == HALF:
            for time_int in time_int_list:
                source.half.append(time_int)
        elif doctor.appointment_type == HOUR:
            for time_int in time_int_list:
                source.hour.append(time_int)
        source.save()
        append_appointment_time(request.CUR_USER.id, appointment_day, time_int_list, doctor.appointment_type, doctor.license_type)
    elif doctor.appointment_type == HOUR_HALF:
        half_time_int_list = get_type_time_int(HALF, start_appointment, end_appointment)
        hour_time_int_list = get_type_time_int(HOUR, start_appointment, end_appointment)

        for time_int in half_time_int_list:
            source.half.append(time_int)

        for time_int in hour_time_int_list:
            source.hour.append(time_int)
        source.save()
        append_appointment_time(request.CUR_USER.id, appointment_day, half_time_int_list, HALF, doctor.license_type)
        append_appointment_time(request.CUR_USER.id, appointment_day, hour_time_int_list, HOUR, doctor.license_type)

    return return_json_data(message=_("time seted successfully"))


@csrf_exempt
@check_user_auth
def doctor_remove_date(request):
    if request.method != "POST":
        return not_allowed_response()

    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    appointment_day = get_int(request.POST.get("appointment_day", 0))
    start_date = request.POST.get("start_date", "")  # given date, in format '18:30'
    end_date = request.POST.get("end_date", "")  # given date, in format '18:30'

    if appointment_day not in DAYS_LIST:
        return bad_request_response(message=_("appointment day is not in range"))

    try:
        appointment_date = datetime.today().strftime('%Y-%m-%d')
        start_appointment = datetime.strptime(appointment_date + " " + start_date, "%Y-%m-%d %H:%M")
        end_appointment = datetime.strptime(appointment_date + " " + end_date, "%Y-%m-%d %H:%M")
    except Exception:
        return bad_request_response()

    if start_appointment > end_appointment:
        return bad_request_response()

    try:
        source = SourceFreeDate.objects.get(user=request.CUR_USER.id, day=appointment_day)
    except Exception:
        return return_json_data(message=_("time remove successfully"))

    if doctor.appointment_type != HOUR_HALF:
        time_int_list = get_type_time_int(doctor.appointment_type, start_appointment, end_appointment)
        if doctor.appointment_type == QUARTER:
            for time_int in time_int_list:
                try:
                    source.quarter.remove(time_int)
                except Exception:
                    pass
        elif doctor.appointment_type == HALF:
            for time_int in time_int_list:
                try:
                    source.half.remove(time_int)
                except Exception:
                    pass
        elif doctor.appointment_type == HOUR:
            for time_int in time_int_list:
                try:
                    source.hour.remove(time_int)
                except Exception:
                    pass
        source.save()
        remove_appointment_time(request.CUR_USER.id, appointment_day, time_int_list, doctor.appointment_type, doctor.license_type)
    elif doctor.appointment_type == HOUR_HALF:
        half_time_int_list = get_type_time_int(HALF, start_appointment, end_appointment)
        hour_time_int_list = get_type_time_int(HOUR, start_appointment, end_appointment)

        for time_int in half_time_int_list:
            try:
                source.half.remove(time_int)
            except Exception:
                pass
        for time_int in hour_time_int_list:
            try:
                source.hour.remove(time_int)
            except Exception:
                pass
        source.save()
        remove_appointment_time(request.CUR_USER.id, appointment_day, half_time_int_list, HALF, doctor.license_type)

    return return_json_data(message=_("time remove successfully"))


@csrf_exempt
@check_user_auth
def doctor_remove_day(request):
    if request.method != "POST":
        return not_allowed_response()

    doctor = get_doctor(request.CUR_USER.id)
    if not doctor:
        return bad_request_response()

    appointment_day = get_int(request.POST.get("appointment_day", 0))

    if appointment_day not in DAYS_LIST:
        return bad_request_response(message=_("appointment day is not in range"))

    try:
        source = SourceFreeDate.objects.get(user=request.CUR_USER.id, day=appointment_day)
    except Exception:
        return return_json_data(message=_("time removed successfully"))
    source.delete()
    remove_appointment_day(request.CUR_USER.id, appointment_day)
    return return_json_data(message=_("time removed successfully"))


def append_appointment_time(user_id, day, time_int_list, appointment_type, doctor_type):
    appointment_datetime = get_next_weekday(datetime.today(), day)
    appointment_day = appointment_datetime.date()

    for time_int in time_int_list:
        DoctorFreeDate.append_time(appointment_day, time_int, user_id, appointment_type, doctor_type)
    return True


def remove_appointment_time(user_id, day, time_int_list, appointment_type, doctor_type):
    appointment_datetime = get_next_weekday(datetime.today(), day)
    appointment_day = appointment_datetime.date()

    for time_int in time_int_list:
        DoctorFreeDate.remove_time(appointment_day, time_int, user_id, appointment_type, doctor_type)
    return True


def remove_appointment_day(user_id, day):
    appointment_datetime = get_next_weekday(datetime.today(), day)
    appointment_day = appointment_datetime.date()

    try:
        doctor_freedate = DoctorFreeDate.objects.get(day=appointment_day, user=user_id)
        for time_int in doctor_freedate.quarter:
            FreeDay.remove_time(appointment_day, time_int, QUARTER, doctor_freedate.doctor_type)

        for time_int in doctor_freedate.half:
            FreeDay.remove_time(appointment_day, time_int, HALF, doctor_freedate.doctor_type)
        doctor_freedate.delete()
    except Exception:
        pass
    return True


def cron_set_appointment(day):
    if day not in DAYS_LIST:
        return False

    appointment_datetime = get_next_weekday(datetime.today(), day)
    appointment_day = appointment_datetime.date()

    if appointment_day <= datetime.today().date():
        return False

    sources = SourceFreeDate.objects.filter(day=day)
    for source in sources:
        set_appointment_day(source, appointment_day)
    return True


def set_appointment_day(source, appointment_day):
    doctor = get_doctor(source.user)
    if not doctor:
        return False

    try:
        doctor_freedate = DoctorFreeDate.objects.get(user=source.user, day=appointment_day, doctor_type=doctor.license_type)
    except Exception:
        doctor_freedate = DoctorFreeDate.objects.create(user=source.user, day=appointment_day, doctor_type=doctor.license_type)

    try:
        free_day = FreeDay.objects.get(day=appointment_day, doctor_type=doctor.license_type)
    except Exception:
        free_day = FreeDay.objects.create(day=appointment_day, doctor_type=doctor.license_type)

    doctor_freedate.quarter = source.quarter
    doctor_freedate.half = source.half
    doctor_freedate.hour = source.hour
    doctor_freedate.save()

    for time_int in source.quarter:
        free_day.quarter.append(time_int)

    for time_int in source.half:
        free_day.half.append(time_int)

    for time_int in source.hour:
        free_day.hour.append(time_int)
    free_day.save()

    return True

