from django.conf.urls import url
from api.v1.authentication import views

urlpatterns = [
    url(r'send/code/$', views.send_code, name='api-v1-auth-send-code'),
    url(r'resend/code/$', views.resend_activation_code, name='api-v1-auth-resend-active-code'),
    url(r'verify/code/$', views.verify_code, name='api-v1-auth-verify-code'),
    url(r'register/$', views.register, name='api-v1-auth-register'),
    url(r'logout/$', views.logout, name='api-v1-auth-logout'),

]
