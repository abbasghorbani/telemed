# -*- coding: utf-8 -*-
import re

from django.contrib.auth.models import User
from django.http import UnreadablePostError
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from api.decorators import method_is_post
from api.auth_cache import AuthCache
from api.tools import normalize_phone, validate_mobile, normalize_number, allow_get_code, is_valid_ssn, str2bool
from api.http_response import bad_request_response, return_json_data,\
    unauthorized_response, forbidden_response

from user_profile.models import ApiKey, ActivationCode, Profile, Doctor, PhoneData


@csrf_exempt
@method_is_post
def send_code(request):

    phone = request.POST.get("phone", None)
    if not phone:
        return bad_request_response(message=_('Please fill in phone number field'))

    # Normalize and validate phone number
    phone = normalize_number(phone)
    norm_phone = normalize_phone(phone)
    if not validate_mobile(norm_phone):
        return bad_request_response(message=_('The entered phone number is not valid'))

    # Check attempts count
    is_allow, count, msg = allow_get_code(norm_phone)
    if not is_allow:
        return forbidden_response(message=msg)

    # Create activation code and set to cache
    res = ActivationCode.create_code(phone=norm_phone, count=count)
    if not res:
        return bad_request_response(message=_('Please try again'))

    return return_json_data(message=_("Activation code sent"))


@csrf_exempt
@method_is_post
def verify_code(request):

    verify_code = request.POST.get('code', None)
    phone = request.POST.get('phone', None)
    phone_data = request.POST.get('phone_data', None)

    if not verify_code or not phone:
        return bad_request_response(msg='Error in parameters')

    # Check verify code
    verify_code = normalize_number(verify_code)
    if not re.search(r'^\d{4}$', verify_code):
        return bad_request_response(message=_('Invalid verify code'))

    # Normalize phone number
    phone = normalize_number(phone)
    norm_phone = normalize_phone(phone)
    if not validate_mobile(norm_phone):
        return bad_request_response(message=_('The entered phone number is not valid'))

    # Check exists activation code for this phone number
    try:
        active_code = ActivationCode.objects.only('is_active').get(code=verify_code, phone=norm_phone)
    except ActivationCode.DoesNotExist:
        return bad_request_response(message=_("The entered code is not valid"))

    # Check user already active
    if active_code.is_active:
        return bad_request_response(message=_("user already actived"))

    # Check user already registered
    active_code.is_active = True
    active_code.save()
    try:
        profile = Profile.objects.only('user').get(phone=norm_phone)
    except Profile.DoesNotExist:
        profile = None

    if not profile:
        return unauthorized_response(message=_("User not registered"))
    else:
        PhoneData.update_phone_data(user_id=profile.user_id, data=phone_data)
        token = ApiKey.update_token(profile.user_id)
        if token is None:
            return bad_request_response(message=_('Please try again'))

        pro_json = AuthCache.get_profile_from_id(user_id=profile.user_id, cur_user=profile.user_id)
        data = {"profile": pro_json, "token": token}
        active_code.delete()
        return return_json_data(data=data)


@csrf_exempt
@method_is_post
def register(request):

    first_name = request.POST.get("first_name", None)
    last_name = request.POST.get("last_name", None)
    ssn = request.POST.get("ssn", None)
    phone = request.POST.get('phone', None)
    phone_data = request.POST.get('phone_data', None)
    is_doctor = str2bool(request.POST.get('is_doctor', "False"))

    if first_name is None or ssn is None or last_name is None or phone is None:
        return bad_request_response(message=_('Error in parameters'))

    # Normalize phone
    phone = normalize_number(phone)
    norm_phone = normalize_phone(phone)
    if not validate_mobile(norm_phone):
        return bad_request_response(message=_('The entered phone number is not valid'))

    # Check parameters
    if re.match("^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِa-zA-Z_\.! ‌]{3,50}$", str(first_name)) is None:
        return bad_request_response(message=_('first_name bad characters'))

    if re.match("^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِa-zA-Z_\.! ‌]{3,50}$", str(last_name)) is None:
        return bad_request_response(message=_('last_name bad characters'))

    # Validation SSN
    normalize_ssn = normalize_number(ssn)
    if not re.search(r'^\d{10}$', normalize_ssn) or not is_valid_ssn(normalize_ssn):
        return bad_request_response(message=_('Invalid ssn'))

    # Check user already registered
    if Profile.is_exist_ssn(normalize_ssn):
        return bad_request_response(message=_('ssn already exists'))

    if Profile.is_exist_phone(phone=norm_phone):
        return bad_request_response(message=_('phone already exists'))

    # Check user already is authenticated
    try:
        active_phone = ActivationCode.objects.only('is_active').get(phone=norm_phone)
        if not active_phone:
            return bad_request_response(message=_('The phone number entered is not authenticated'))
    except Exception as e:
        print str(e), " || register function"
        return bad_request_response(message=_('The phone number entered is not authenticated'))

    try:
        # Create user
        email = "{}@{}.com".format(norm_phone, norm_phone)
        user = User.objects.create_user(username=norm_phone,
                                        email=email,
                                        password=norm_phone)
        status, message, profile = Profile.create_user_profile(first_name=first_name,
                                                               last_name=last_name,
                                                               ssn=normalize_ssn,
                                                               user=user,
                                                               phone=norm_phone,
                                                               is_doctor=is_doctor)

        # Create doctor instance if is_doctor true
        if is_doctor:
            Doctor.objects.create(user_id=user.id)
        # Delete activation code
        active_phone.delete()

        if not status:
            return bad_request_response(message=message)

        PhoneData.update_phone_data(user_id=user.id, data=phone_data)
        api_key = ApiKey.objects.only('token').get(user_id=user.id)

        # Prepare response
        data = {"profile": profile.get_json(), "token": api_key.token}
        return return_json_data(data=data)
    except Exception as e:
        print str(e), " || Register function: error in user creation"
        return bad_request_response(message=_("Error in user creation"))


@csrf_exempt
@method_is_post
def resend_activation_code(request):

    try:
        phone = request.POST.get("phone", None)
    except UnreadablePostError:
        return bad_request_response()

    if not phone:
        return bad_request_response(message=_('Please fill in phone field'))

    phone = normalize_number(phone)
    norm_phone = normalize_phone(phone)
    is_allow, count, msg = allow_get_code(norm_phone)
    if not is_allow:
        return forbidden_response(message=msg)

    if not validate_mobile(norm_phone):
        return bad_request_response(message=_('The entered phone number is not valid'))

    res = ActivationCode.create_code(phone=norm_phone, count=count, resend=True)
    if not res:
        return bad_request_response(message=_('Please try again'))

    return return_json_data(message=_('The activation code has been sent to you.'))


def logout(request):

    return return_json_data(message=_('Successfully Logout'))
