from django.conf.urls import url
from api.v1.chat import views

urlpatterns = [
    url(r'responseInvite/$', views.response_invite, name='api-v1-chat-responseInvite'),
    url(r'startChat/$', views.start_chat, name='api-v1-chat-start'),
]

API_VERSION = 'v1'
urls = {
    # '^'+API_VERSION+'/topic/[a-zA-Z]+/$': {'function': 'api.v1.chat.views_mqtt.save_message'},
    '^'+API_VERSION+'/topic/s/[a-zA-Z0-9]+/$': {'function': 'api.v1.chat.views_mqtt.manage_packet'},
    # '^'+API_VERSION+'/topic/configTopic/[a-zA-Z]+/$': {'function': 'api.v1.chat.views_mqtt.manage_packet'},
    '^'+API_VERSION+'/topic/responseInvite/[a-zA-Z]+/$': {'function': 'api.v1.chat.views_mqtt.manage_packet'},
    '^'+API_VERSION+'/topic/leave/[a-zA-Z]+/$': {'function': 'api.v1.chat.views_mqtt.manage_packet'},
    '^[$]SYS/brokers/[a-z@0-9./]+/clients/[a-zA-Z:0-9_/-]+/disconnected': {'function': 'api.v1.chat.views_mqtt.update_last_login'},
    '^[$]SYS/brokers/[a-z@0-9./]+/clients/[a-zA-Z:0-9_/-]+/connected': {'function': 'api.v1.chat.views_mqtt.update_last_login'},
}
