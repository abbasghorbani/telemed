# -*- coding: utf-8 -*-
import json

from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from chat.models_mongo import Topic, MqttAcl
from media.models import File
from user_profile.models import Doctor

from api.decorators import check_user_auth
from api.http_response import not_found_response, return_json_data, bad_request_response, forbidden_response
from api.auth_cache import AuthCache
from api.tools import get_user_topic, get_topic_uri, get_server_topic_uri, str2bool, get_int, local2UTC
from api.v1.chat.views_mqtt import send_join_message

from notification.message import answer_invite_message, new_members_message, join_message
from notification.notif import Notification
from medical.models import MedicalRecord, APPROVE, STARTED
from datetime import timedelta, datetime
from chat.tasks import end_chat
from chat.models import FreeswithchNumber


@csrf_exempt
@check_user_auth
def response_invite(request):

    user_id = request.CUR_USER.id
    hashcode = request.POST.get("topic", None)
    is_accept = str2bool(request.POST.get("is_accept", "False"))

    # if user not accept remove invite topic
    if hashcode is None:
        return bad_request_response(message=_("Invalid parameters"))

    # checking current user must be a doctor
    profile = AuthCache.get_profile_from_id(user_id=user_id)
    if profile is None:
        return bad_request_response(message=_("Profile not Found"))

    is_doctor = profile.get("is_doctor", False)
    is_approve = profile.get("is_approve", False)
    if not is_doctor or (is_doctor and not is_approve):
        return forbidden_response(message=_("You are not a doctor"))

    # Ckecking topic
    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print "chat views.py: {}".format(str(e))
        return not_found_response(message=_("Topic not found"))

    if user_id in topic.members:
        return forbidden_response(message=_("You are not allowed to do this"))

    if user_id not in topic.pending_invited:
        return forbidden_response(message=_("You are not allowed to do this"))

    if topic.is_ended:
        text = "Topic {} is ended".format(hashcode)
        message = _(text)
        return forbidden_response(message=message)

    # Send response of invite on doctor topic
    doctor_topic = get_user_topic(topic.owner)
    json_data = answer_invite_message(topic, user_id, is_accept)
    msgs = []
    msgs.append({
        'topic': doctor_topic,
        'payload': json_data,
        'qos': 1,
        'retain': False,
    })
    notif = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    notif.send()

    if is_accept:
        after_accept_invite(hashcode, profile, topic, user_id)

    topic.update_invited_user_list(flag="pull", user_ids=[user_id])

    return return_json_data(message=_("Your operation is successfully is done"))


def after_accept_invite(hashcode, profile, topic, user_id):
    """
    This function performs the update of topic members,
    update access to the files in the topic
    and updates the user access that is invited to the topic.
    """
    topic.update(__raw__={"$push": {"members": {"$each": [user_id]}}})
    # topic.upadte(push_all__members=[user_id])
    try:
        File.objects.filter(hashcode=hashcode).update(members=json.dumps(topic.members))
    except Exception as e:
        print "views_mqtt after_accept_invite: {}".format(str(e))

    group_topic = get_topic_uri(hashcode)
    server_topic = get_server_topic_uri(hashcode)

    # Update new member acl
    MqttAcl.update_acl(username=profile["medical_number"],
                       publish=[server_topic],
                       subscribe=[group_topic],
                       flag="push")

    # Send new member notif on topic
    new_member_data = new_members_message(topic, [user_id])
    msgs = []
    msgs.append({
        'topic': group_topic,
        'payload': new_member_data,
        'qos': 1,
        'retain': False,
    })

    # Send join notif on new member topic
    json_data = join_message(topic)
    user_topic = get_user_topic(user_id)
    msgs.append({
        'topic': user_topic,
        'payload': json_data,
        'qos': 1,
        'retain': False,
    })

    notif = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    notif.send()

    return True


@csrf_exempt
@check_user_auth
def start_chat(request):
    """
    This function take a medical_id and start a session between doctor and patient
    """
    cur_user_id = request.CUR_USER.id
    exists = False
    medical_id = get_int(request.POST.get("medical_id", 0))
    if medical_id == 0:
        return bad_request_response(message=_("Invalid parameters"))

    # Get doctor profile
    doc_profile = AuthCache.get_profile_from_id(user_id=cur_user_id)
    if doc_profile is None:
        return bad_request_response(message=_("Doctor profile not found"))

    if doc_profile["is_doctor"] and doc_profile["is_approve"]:
        # Is there an open session for every doctor ?
        exists_started_chat = MedicalRecord.is_exists_started(doctor_id=cur_user_id)
        if exists_started_chat:
            return forbidden_response(message=_("The open session is exists.Please close the open session and move on to the next meeting."))

        # Get medical record
        medical_obj = MedicalRecord.get_medical_record(medical_id=medical_id)
        if medical_obj is None:
            return not_found_response(message=_("Medical record not found"))

        if medical_obj.status != APPROVE:
            return not_found_response(message=_("Medical record not found"))

        if medical_obj.doctor_id != cur_user_id:
            return forbidden_response()

        cur_time = datetime.now()
        start = medical_obj.start_session - timedelta(minutes=1)
        end = medical_obj.start_session + timedelta(minutes=5)

        if start <= cur_time < end:
            first_name = doc_profile["first_name"]
            last_name = doc_profile["last_name"]

            title = "{} {} {}".format(_("Meeting with Dr."), first_name, last_name)
            members = [medical_obj.patient_id, medical_obj.doctor_id]

            # Get topic
            try:
                topic = Topic.objects.get(medical_record=medical_obj.id)
                exists = True
            except Topic.DoesNotExist:
                # Create xml file for video or voice call
                status, data = FreeswithchNumber.create_call_requirement(title=title, user_ids=members)
                if not status:
                    # print data["message"]
                    return bad_request_response(message=_("Unfortunately , it is impossible to make voice calls and video calls. please try later again."))

                topic = Topic.create_topic(title=title,
                                           members=members,
                                           medical_record=medical_obj.id,
                                           owner=medical_obj.doctor_id,
                                           patient=medical_obj.patient_id,
                                           numbers=data['numbers'])
            if exists:
                data = {"topic": topic.get_json()}
                return return_json_data(data=data)

            # Update doctor status
            Doctor.update_status(user_id=int(doc_profile["user_id"]), status=Doctor.BUSY)

            # Update medical status
            medical_obj.status = STARTED
            medical_obj.save()

            # Create topic and set permission
            group_topic = get_topic_uri(topic.hashcode)
            server_topic = get_server_topic_uri(topic.hashcode)

            for member in members:
                profile = AuthCache.get_profile_from_id(user_id=member)
                if profile is None:
                    print "chat views start_chat: profile with user_id {} does not exist".format(member)
                    continue

                MqttAcl.update_acl(username=profile["medical_number"],
                                   subscribe=[group_topic],
                                   publish=[server_topic],
                                   flag="push")

            # Send join message for all member
            send_join_message(topic.hashcode, members)

            # Send an notification to end the conversation and save task_id in medical record
            res = end_chat.apply_async(args=[topic.hashcode], eta=local2UTC(medical_obj.end_session))
            MedicalRecord.update_task_id(task_id=res.task_id, medical_id=medical_obj.id)
            data = {"topic": topic.get_json()}
            return return_json_data(data=data)
        else:
            return forbidden_response(message=_("You do not have access to this appointment"))
    else:
        return forbidden_response()
