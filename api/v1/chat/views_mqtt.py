# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from datetime import datetime
import paho.mqtt.publish as publish

from django.conf import settings
from django.utils.translation import ugettext as _
from django.core.cache import cache

from chat.models_mongo import Message, Topic, SEND_MESSAGE, INVITE, KICK,\
    GET_HISTORY, LEAVE, MqttAcl

from user_profile.models import Doctor
from api.auth_cache import AuthCache
from api.tools import prepare_data, get_user_topic, get_topic_uri,\
    get_server_topic_uri

from notification.notif import Notification
from notification.message import join_message, invite_message,\
    kick_message, leave_message,\
    history_message, end_chat_request_message, error_message


def multiple_pubish(msgs):
    publish.multiple(msgs,
                     hostname=settings.MQTT_HOST,
                     port=settings.MQTT_PORT,
                     keepalive=settings.MQTTT_KEEPALIVE,
                     auth=settings.MQTT_AUTH)


def single_pubish(topic, payload, qos=1, retain=False):
    publish.single(topic=topic,
                   payload=payload,
                   hostname=settings.MQTT_HOST,
                   port=settings.MQTT_PORT,
                   keepalive=settings.MQTTT_KEEPALIVE,
                   auth=settings.MQTT_AUTH,
                   qos=qos,
                   retain=retain)


def manage_packet(data, topic):
    """
    Checking packet_type field and handle received data
    """
    normal_data = prepare_data(data)
    if normal_data is None:
        print "views_mqtt manage_packet: error in convert to dict"
        return
    packet_type = normal_data.get("packet_type", None)
    if packet_type is None:
        print "Packet type is none"
        return

    if normal_data["packet_type"] == INVITE:
        send_invite(data)

    elif normal_data["packet_type"] == KICK:
        kick(data)

    # elif normal_data["packet_type"] == ANSWER_INVITE:
    #     response_invite(data)

    elif normal_data["packet_type"] == LEAVE:
        leave(data)

    elif normal_data["packet_type"] == GET_HISTORY:
        get_history(data)

    elif normal_data["packet_type"] == SEND_MESSAGE:
        try:
            payload = normal_data["payload"]
            message_type = int(payload.get("message_type", 1))
            if message_type not in [1, 2, 3, 4, 5, 6]:
                print "views_mqtt manage_packet: message_type not found"
                return
            payload["topic"] = topic
            payload["packet_id"] = normal_data.get("packet_id")
            payload["create_at"] = datetime.now().strftime("%s")
            payload["packet_type"] = normal_data.get("packet_type")
            payload["message_type"] = message_type
            payload["reply_to_message"] = payload.get("reply_to_message", {})
            payload["caption"] = payload.get("caption", "")
            payload["audio"] = payload.get("audio", {})
            payload["document"] = payload.get("document", {})
            payload["video"] = payload.get("video", {})
            payload["photo"] = payload.get("photo", {})
            payload["media_message"] = payload.get("media_message", {})
            payload["new_members"] = payload.get("new_members", [])
            payload["left_members"] = payload.get("left_members", [])

        except Exception as e:
            print str(e), "|| views_mqtt manage_packet: get payload error"
            return

        try:
            message = Message(**payload)
        except Exception as e:
            print str(e), "|| views_mqtt manage_packet: error in initial message"
            message = None

        if message is None:
            print "views_mqtt manage_packet: error in convert to Message object"
            return

        res = message.update_from_user(normal_data["token"])
        if not res:
            print "views_mqtt manage_packet: error in update_from_user"
            return

        status = message.send_message(payload)
        if not status:
            send_error("manage_packet send massage error", message.packet_id)

        try:
            message.save()
        except Exception as e:
            print str(e), "manage_packet save massage error"

    else:
        print "views_mqtt manage_packet: packet unknown"
        return


def save_message(data):
    """ listen on # and save all packet that packet type is SEND_MESSAGE """
    normal_data = prepare_data(data)

    if normal_data is None:
        print "views_mqtt save_message: error in convert to dict"
        return
    try:
        message = Message(**normal_data["payload"])
    except Exception as e:
        print str(e), " views_mqtt save_message: error in initial message "
        message = None

    if message is None:
        print "views_mqtt save_message: error in convert to Message object"
        return

    # Save message
    try:
        is_valid, msg = message.is_valid()
        if not is_valid:
            print "views_mqtt save_message: {}".format(msg)
            return
        message.save()
    except Exception as e:
        print str(e), "views_mqtt save_message: error in save message "
        return
    return


def send_invite(data):
    """
    Subscribe on group topic
    and get invite request from owner of topic
    and checking request then everything is ok publish on invited users topic
    """
    """
    Send invite data
    {
        "packet_id": "455454454",
        "packet_type": 2,
        "token": "dsdsdsdsds",
        "payload": {
            "user_ids: [2,3],
            "topic": "dsdsdsdsd"
        }
    }
    """
    # Checking parameters
    normal_data = prepare_data(data)
    if normal_data is None:
        print "views_mqtt send_invite: error in convert to dict"
        return

    user_ids = normal_data["payload"].get("user_ids", [])
    hashcode = normal_data["payload"].get("topic", None)

    # Checking sender status.The user must be a doctor
    from_user_id = AuthCache.get_id_from_token(token=normal_data["token"])
    if from_user_id is None:
        print "views_mqtt send_invite: user with token {} does not exist".format(normal_data["token"])
        return

    profile = AuthCache.get_profile_from_id(user_id=from_user_id)
    if profile is None:
        print "views_mqtt send_invite: profile with user_id {} does not exist".format(from_user_id)
        return
    is_doctor = profile.get("is_doctor", False)
    is_approve = profile.get("is_approve", False)
    if not is_doctor or (is_doctor and not is_approve):
        msg = _("You are not a doctor")
        send_error(msg, from_user_id)
        return

    if len(hashcode) == 0 or not isinstance(user_ids, list) or len(user_ids) == 0:
        print "views_mqtt send_invite: error in data"
        return

    # Checking exist topic
    try:
        obj = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print "views_mqtt send_invite: ".format(str(e))
        message = _("Topic is wrong")
        send_error(message, from_user_id)
        return

    # Checking from_user_id is owner
    if obj.owner != from_user_id:
        message = _("Access denied")
        send_error(message, from_user_id)
        return

    # Checking topic is not ended
    if obj.is_ended:
        message = _("Topic is ended")
        send_error(message, from_user_id)
        return

    # Check member count
    if len(user_ids) > 4 or len(obj.members) + len(user_ids) > 6 or len(obj.pending_invited) > 5:
        message = _("Too many member")
        send_error(message, from_user_id)
        return

    # Checking all user is doctor and Checking does not invite the user himself
    msgs = []
    invited_user_ids = []
    for user_id in set(user_ids):
        if user_id in obj.members or user_id in obj.pending_invited:
            continue
        profile = AuthCache.get_profile_from_id(user_id=user_id)
        if profile is None:
            print "views_mqtt send_invite: profile with user_id {} does not exist".format(user_id)
            continue

        is_doctor = profile.get("is_doctor", False)
        is_approve = profile.get("is_approve", False)
        if not is_doctor or (is_doctor and not is_approve):
            print "views_mqtt send_invite: user is not a doctor"
            continue

        invited_user_ids.append(user_id)
        json_data = invite_message(obj, from_user_id, user_id)
        msgs.append({
            'topic': get_user_topic(user_id),
            'payload': json_data,
            'qos': 2,
            'retain': False,
        })

    # Send packet
    if len(msgs) > 0:
        obj.update_invited_user_list(user_ids=invited_user_ids, flag="push")
        multiple_pubish(msgs)
    return


def send_join_message(hashcode, members=[]):
    """
    Join data
    {"packet_id": "455454454",
     "packet_type": 3,
     "payload": {"id": "45444455444",
                 "title": "test",
                 "hashcode": "hkkjjjkj",
                 "members": [{"first_name": "amir",
                              "last_name": "ghorbani",
                              "is_doctor": False,
                              "medical_number": "H121545454"}],
                 "member_count": 2,
                 "topic": "v1/topic/hkkjjjkj/"}
    }
    """
    try:
        obj = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print "views_mqtt send_join_message: {}".format(str(e))
        return

    json_data = join_message(obj)
    msgs = []
    for member in members:
        user_topic = get_user_topic(member)
        msgs.append({
            'topic': user_topic,
            'payload': json_data,
            'qos': 1,
            'retain': False,
        })

    # Send packet
    multiple_pubish(msgs)
    return


def kick(data):
    """
    Kick data
     {
        "packet_id": "455454454",
        "packet_type": 6,
        "token": "dsdsd"
        "payload": {
            "topic": "sDSDdsdd",
            "user_ids": [2,3]
        }
    }

    """
    normal_data = prepare_data(data)
    if normal_data is None:
        print "kick function, error in convert to dict"
        return

    # Sender request must be a doctor and must be topic owner
    sender_user_id = AuthCache.get_id_from_token(token=normal_data["token"])
    if sender_user_id is None:
        print "views_mqtt kick: user with token {} does not exist".format(normal_data["token"])
        return

    profile = AuthCache.get_profile_from_id(user_id=sender_user_id)
    if profile is None:
        print "views_mqtt kick: profile with user_id {} does not exist".format(sender_user_id)
        return

    is_doctor = profile.get("is_doctor", False)
    is_approve = profile.get("is_approve", False)
    if not is_doctor or (is_doctor and not is_approve):
        message = _("You are not a doctor")
        send_error(message, sender_user_id)
        return

    # Checking paramaters
    users_id = normal_data["payload"].get("users_id", [])
    hashcode = normal_data["payload"].get("topic", None)

    if not isinstance(users_id, list) or len(users_id) == 0 or len(hashcode) == 0:
        print "views_mqtt kick: invalid paramaters"
        return

    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print "views_mqtt kick: {}".format(str(e))
        return

    if topic.is_ended:
        text = "Topic {} is ended".format(topic.title)
        message = _(text)
        send_error(message, topic.owner)
        return

    members = set(users_id)
    users_id = []
    for member in members:
        if member in [topic.owner, topic.patient]:
            continue
        if member not in topic.members:
            continue
        users_id.append(member)

    if users_id == []:
        message = _("Invalid users_id parameter")
        send_error(message, topic.owner)
        return

    json_data = kick_message(topic, users_id)
    topic_uri = get_topic_uri(hashcode)
    server_topic = get_server_topic_uri(hashcode)

    # Send notif on topic
    msgs = []
    msgs.append({
        'topic': topic_uri,
        'payload': json_data,
        'qos': 1,
        'retain': False,
    })

    # Send notif on kicked members topic
    for member in users_id:
        profile = AuthCache.get_profile_from_id(user_id=member)
        if profile is None:
            print "views_mqtt kick: profile with user_id {} does not exist".format(member)
            continue

        user_topic = get_user_topic(member)

        MqttAcl.update_acl(username=profile["medical_number"],
                           publish=[server_topic],
                           subscribe=[topic_uri],
                           flag="pull")
        msgs.append({
            'topic': user_topic,
            'payload': json_data,
            'qos': 1,
            'retain': False,
        })

    sender = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    sender.send()

    try:
        topic.update(pull_all__members=users_id)
    except Exception as e:
        print str(e), " ||kick function error"

    return


def leave(data):
    """
    leave data
     {
        "packet_id": "455454454",
        "packet_type": 9,
        "token": "sdsdsdsd",
        "payload": {
            "topic": "sDSDdsdd",
        }
    }

    """
    normal_data = prepare_data(data)
    if normal_data is None:
        print "views_mqtt leave: error in convert to dict"
        return

    token = normal_data.get("token", None)
    hashcode = normal_data["payload"].get("topic", None)

    if len(token) == 0 or len(hashcode) == 0:
        print "views_mqtt leave: invalid parameters"
        return

    # Checking sender status
    user_id = AuthCache.get_id_from_token(token=token)
    if user_id is None:
        print "views_mqtt leave: user with token {} does not exist".format(normal_data["token"])
        return

    profile = AuthCache.get_profile_from_id(user_id=user_id)
    if profile is None:
        print "views_mqtt leave: profile with user_id {} does not exist".format(user_id)
        return

    is_doctor = profile.get("is_doctor", False)
    is_approve = profile.get("is_approve", False)
    if not is_doctor or (is_doctor and not is_approve):
        message = _("You are not a doctor")
        send_error(message, user_id)
        return

    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print str(e), " || leave function, get topic error"
        return

    if topic.is_ended:
        text = "Topic {} is ended".format(hashcode)
        message = _(text)
        send_error(message, user_id)
        return

    if user_id in [topic.owner, topic.patient]:
        message = _("You are not allowed left this topic")
        send_error(message, user_id)
        return

    if user_id not in topic.members:
        message = _("You are not allowed do this")
        send_error(message, user_id)
        return

    user_topic = get_user_topic(hashcode)
    group_topic = get_topic_uri(hashcode)
    server_topic = get_server_topic_uri(hashcode)

    try:
        MqttAcl.update_acl(username=profile["medical_number"],
                           publish=[server_topic],
                           subscribe=[group_topic],
                           flag="pull")
    except Exception as e:
        print "views_mqtt leave: {} {}".format(str(e), "update mqtt acl error")
        return
    try:
        topic.update(pull_all__members=[user_id])
    except Exception as e:
        print str(e), " ||leave function, update member error"
        print "views_mqtt leave: {} {}".format(str(e), "update member error")
        return

    # Send notif
    json_data = leave_message(topic, user_id)
    msgs = []
    for topic in [group_topic, user_topic]:
        msgs.append({
            'topic': topic,
            'payload': json_data,
            'qos': 1,
            'retain': False,
        })
    notif = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    notif.send()

    return


def get_history(data):
    """
    Get History request data
    {
        "packet_id": "455454454",
        "packet_type": "10",
        "token":"dssds",
        "payload": {
            "topic": "sDSDdsdd",
            "offset": "0",
            "order": "-"
        }
    }

    History response data
    {
        "packet_id": "455454454",
        "packet_type": 10,
        "payload": [msg1, msg2]
    }

    """
    normal_data = prepare_data(data)
    if normal_data is None:
        print "views_mqtt get_history: error in convert to dict"
        return

    hashcode = normal_data["payload"].get("topic", None)
    offset = normal_data["payload"].get("offset", 0)
    order = normal_data["payload"].get("order", "+")
    limit = 10

    from_user_id = AuthCache.get_id_from_token(token=normal_data["token"])
    if from_user_id is None or len(hashcode) == 0:
        print "views_mqtt get_history: invalid parameters"
        return

    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print "views_mqtt get_history: {} {}".format(str(e), "get topic error")
        return

    if from_user_id not in topic.members:
        message = _("You are not allowed do this")
        send_error(message, from_user_id)
        return

    if order == "-":
        order = "-id"
    else:
        order = "id"

    messages = Message.objects.filter(topic=hashcode).order_by(order)[offset: offset + limit]
    msgs = []
    for message in messages:
        msgs.append(message.to_json())

    json_data = history_message(from_user_id, msgs)
    user_topic = get_user_topic(from_user_id)
    single_pubish(topic=user_topic, payload=json_data)
    return


def end_chat_request(hashcode):
    """
    End chat data

    {"packet_id": "455454454",
     "packet_type": 11,
     "payload":{
                 "topic": {"id": "45444455444",
                           "title": "test",
                           "hashcode": "hkkjjjkj",
                           "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/"},
                 "message": "The length of time allocated to this patient has been completed.
                             Would you like to continue your visit?",
                 "choices": {"yes": True, "No": False}
                }
    }
    """

    try:
        topic = Topic.objects.get(hashcode=hashcode)
    except Exception as e:
        print str(e), "end_chat_request function, error get topic"
        return

    json_data = end_chat_request_message(topic)
    user_topic = get_user_topic(topic.owner)
    single_pubish(topic=user_topic, payload=json_data)
    return


def send_error(msg, user_id):
    error_msg = error_message(msg)
    user_topic = get_user_topic(user_id)
    single_pubish(topic=user_topic, payload=error_msg)

# def send_delete_message(msg, topic):
#     error_msg = error_message(msg)
#     user_topic = get_user_topic(user_id)
#     single_pubish(topic=user_topic, payload=error_msg)


def update_last_login(data, topic):
    try:
        json_data = json.loads(str(data))
    except Exception as e:
        print str(e), " || views_mqtt.py: update_last_login function"
        return
    online = False
    username = json_data["username"]
    timestamp = json_data["ts"]

    if "connack" in json_data:
        online = True

    user_id = AuthCache.get_id_from_username(username)
    if user_id is None:
        "views_mqtt update_last_login: username {} does not exist".format(username)
        return

    profile = AuthCache.get_profile_from_id(user_id=user_id)
    if profile is None:
        "views_mqtt update_last_login: profile with user_id {} does not exist".format(user_id)
        return

    res = {"online": online, "timestamp": timestamp}
    key = "{}:{}".format("med", username)
    cache.set(key, json.dumps(res))

    profile["status"] = res
    cun_str = "{}_{}".format("puid", user_id)
    cache.set(cun_str, json.dumps(profile))

    if not online:
        Doctor.update_availability(user_id=profile["user_id"], is_available=online)
    return
