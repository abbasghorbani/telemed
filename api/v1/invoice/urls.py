from django.conf.urls import url
from api.v1.invoice import views

urlpatterns = [
    url(r'show/(?P<invoice_number>[0-9a-zA-Z]+)/$', views.show_invoice, name='api-v1-invoice-show'),
    url(r'payment/(?P<invoice_number>[0-9a-zA-Z]+)/$', views.invoice_payment, name='api-v1-invoice-payment'),
    url(r'list/$', views.user_invoices, name='api-v1-invoices-list'),
    url(r'creditLogs/$', views.credit_logs, name='api-v1-credit-logs'),
]
