# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from datetime import datetime, timedelta

from invoice.models import Invoice, CreditLog
from user_profile.models import Profile

from api.decorators import check_user_auth
from api.http_response import not_found_response, return_json_data, bad_request_response
from api.tools import get_int, get_next_url
from api.auth_cache import AuthCache


@check_user_auth
def show_invoice(request, invoice_number):
    current_user = request.CUR_USER
    invoice = Invoice.get_invoice_by_id(invoice_number=invoice_number, user_id=current_user.id)
    if invoice is None:
        return not_found_response(message=_("Invoice Not found"))

    data = invoice.get_json()
    return return_json_data(data=data)


@check_user_auth
def invoice_payment(request, invoice_number):
    current_user = request.CUR_USER
    prof = AuthCache.get_profile_from_id(user_id=current_user.id)
    if prof is None:
        return not_found_response(message=_("Profile not found"))

    try:
        invoice = Invoice.objects.get(invoice_number=invoice_number, user_id=current_user.id, status=Invoice.UNPAID)
    except Exception as e:
        print str(e)
        return not_found_response(message=_("Invoice not found"))

    # Checking medical record not expire
    now = datetime.now()
    start_time = invoice.content_object.start_session + timedelta(minutes=10)
    if now > start_time:
        return not_found_response(message=_("Invoice not found"))

    if int(prof['credit']) < invoice.amount:
        return bad_request_response(message=_("User credit not enough"))

    # Decrease profile credit
    result = Profile.dec_credit(user_id=current_user.id, amount=invoice.amount)
    if not result:
        return bad_request_response(message=_("Error occurred in reducing credit"))

    invoice.status = Invoice.PAID
    invoice.save()

    # Start visit with doctor
    invoice.after_paid_invoice()
    data = invoice.get_json()
    return return_json_data(data=data)


@check_user_auth
def user_invoices(request):
    offset = get_int(request.GET.get("offset", 0))
    limit = 10
    current_user = request.CUR_USER
    invoices_list = Invoice.user_all_invoices(user_id=current_user.id, limit=limit, offset=offset)
    meta = {
            "next": get_next_url(url_name="api-v1-invoices-list", offset=offset + limit + 1),
            "limit": 10,
            "total_count": 0
        }
    return return_json_data(data=invoices_list, meta=meta)


@check_user_auth
def credit_logs(request):
    current_user_id = request.CUR_USER.id
    offset = int(request.GET.get("offset", 0))
    limit = 10
    data = CreditLog.get_credit_logs(user_id=current_user_id, offset=offset)
    meta = {
        "next": get_next_url(url_name="api-v1-credit-logs", offset=offset + limit),
        "limit": 10,
        "total_count": 0
    }
    return return_json_data(data=data, meta=meta)
