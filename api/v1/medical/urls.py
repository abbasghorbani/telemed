from django.conf.urls import url
from api.v1.medical import views, views_prescribe

urlpatterns = [
    url(r'close/$', views.close_medical_record, name='api-v1-medical-close'),
    url(r'update/$', views.update_medical_record, name='api-v1-medical-update'),
    url(r'filterBy/doctor/$', views.filter_by_doctor, name='api-v1-medical-filter-by-doctor'),
    url(r'filterBy/patient/$', views.filter_by_patient, name='api-v1-medical-filter-by-patient'),
    url(r'filter/patient/perDoctor/$', views.filter_patient_per_doctor, name='api-v1-medical-filter-by-both'),
    url(r'doctor/patients/list/$', views.patients_list, name='api-v1-medical-doctor-patients-list'),
    url(r'getPatientDialogs/$', views.get_patient_dialogs, name='api-v1-medical-get-patient-dialogs'),
    url(r'getDoctorDialogs/$', views.get_doctor_dialogs, name='api-v1-medical-get-doctor-dialogs'),
    url(r'createPrescribe/$', views_prescribe.create_prescribe, name='api-v1-medical-create-prescribe'),
    url(r'removePrescribe/$', views_prescribe.remove_prescribe, name='api-v1-medical-remove-prescribe'),
    url(r'getDrugs/$', views_prescribe.get_drugs, name='api-v1-medical-get-drugs'),
    url(r'getPrescribeType/$', views_prescribe.get_type, name='api-v1-medical-get-prescribe-type'),
    url(r'drugReminder/$', views_prescribe.drug_reminder, name='api-v1-medical-set-drug-reminder'),

]
