import redis

from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _

from celery.task.control import revoke

from medical.models import MedicalRecord, STARTED, ENDED
from invoice.models import Invoice
from chat.models_mongo import Topic
from user_profile.models import Doctor, Profile

from api.auth_cache import AuthCache
from api.decorators import method_is_post, check_user_auth
from api.tools import get_int, get_next_url
from api.http_response import bad_request_response, not_found_response, return_json_data, forbidden_response

redis_conn = redis.Redis(settings.REDIS_DB, db=settings.REDIS_DB_NUMBER)


@csrf_exempt
@method_is_post
@check_user_auth
def close_medical_record(request):
    cur_user = request.CUR_USER.id
    medical_id = get_int(request.POST.get("medical_id", 0))
    desc = request.POST.get("description", "")

    if medical_id == 0:
        return bad_request_response(message=_('Invalid parameter'))

    try:
        medical_record = MedicalRecord.objects.get(id=medical_id)
    except Exception:
        return not_found_response()

    if medical_record.status not in [STARTED, ENDED]:
        return not_found_response(message=_("Medical record not found"))

    if cur_user != medical_record.doctor_id:
        return forbidden_response()

    if medical_record.status == STARTED:
        # Update doctor inc credit
        try:
            invoice = Invoice.get_invoice_by_medical_id(medical_id=medical_record.id)
        except Exception:
            return not_found_response(message=_("Invoice not found"))
        Profile.inc_credit(user_id=cur_user, amount=invoice.amount)

    # Close topic
    Topic.close_topic(medical_id=medical_id)

    # Update medical record close session
    medical_record.update_close_session(description=desc)

    # Update doctor status
    Doctor.update_status(user_id=cur_user, status=Doctor.AVAILABLE)

    # Revoke medical tack_id and update it
    if medical_record.task_id:
        revoke(medical_record.task_id, terminate=True)
        MedicalRecord.update_task_id(medical_id=medical_record.id)

    message = _("Successfully close medical record")
    return return_json_data(message=message)


@csrf_exempt
@check_user_auth
@method_is_post
def update_medical_record(request):
    medical_id = get_int(request.POST.get("medical_id", None))
    desc = request.POST.get("description", "")
    if medical_id == 0 or desc == "":
        return bad_request_response(message=_('Invalid parameter'))

    try:
        medical_record = MedicalRecord.objects.get(id=medical_id)
    except Exception:
        return not_found_response()

    if request.CUR_USER.id != medical_record.doctor_id:
        return forbidden_response()

    medical_record.desc = desc
    medical_record.save()
    data = medical_record.get_json()
    return return_json_data(data=data)


@check_user_auth
def filter_by_doctor(request):
    """
    A doctor's visit timeline with patients
    """
    doctor_id = request.CUR_USER.id
    offset = get_int(request.GET.get("offset", 0))
    order = request.GET.get("order", "+")

    profile = AuthCache.get_profile_from_id(user_id=doctor_id)
    if profile is None:
        return not_found_response(message=_("Profile not found"))

    if not profile["is_doctor"] or (profile["is_doctor"] and not profile["is_approve"]):
        return forbidden_response()

    if order == "-":
        order_by = "-end_session"
    else:
        order_by = "end_session"

    data, meta = MedicalRecord.filter_by_doctor(doctor_id=doctor_id, offset=offset, order=order_by)
    return return_json_data(data=data, meta=meta)


@check_user_auth
def filter_by_patient(request):
    """
    A patient's visit timeline with doctors
    """
    patient_id = request.CUR_USER.id
    offset = get_int(request.GET.get("offset", 0))
    order = request.GET.get("order", "+")

    if order == "-":
        order_by = "-id"
    else:
        order_by = "id"

    data, meta = MedicalRecord.filter_by_patient(patient_id=patient_id, offset=offset, order=order_by)
    return return_json_data(data=data, meta=meta)


@csrf_exempt
@check_user_auth
@method_is_post
def filter_patient_per_doctor(request):
    """
    A doctor's visit timeline with a patient
    """
    doctor_id = request.CUR_USER.id
    patient_id = get_int(request.POST.get("patient_id", 0))
    offset = get_int(request.POST.get("offset", 0))
    order = request.POST.get("order", "+")

    if patient_id == 0:
        return bad_request_response(message=_('Invalid parameter'))

    profile = AuthCache.get_profile_from_id(user_id=doctor_id)
    if profile is None:
        return not_found_response(message=_("Profile not found"))

    if not profile["is_doctor"] or (profile["is_doctor"] and not profile["is_approve"]):
        return forbidden_response()

    if order == "-":
        order_by = "-id"
    else:
        order_by = "id"

    data, meta = MedicalRecord.filter_by_both(patient_id=patient_id, doctor_id=doctor_id, offset=offset, order=order_by)
    return return_json_data(data=data, meta=meta)


@check_user_auth
def patients_list(request):
    offset = get_int(request.GET.get("offset", 0))
    doctor_id = request.CUR_USER.id
    profile = AuthCache.get_profile_from_id(user_id=doctor_id)
    if profile is None:
        return not_found_response(message=_("Profile not found"))

    if not profile["is_doctor"] or (profile["is_doctor"] and not profile["is_approve"]):
        return forbidden_response()

    if offset > 100:
        meta = {
            "next": get_next_url(url_name="api-v1-medical-doctor-patients-list", offset=0),
            "limit": 10,
            "total_count": 100
        }
        return return_json_data(meta=meta)
    data, meta = MedicalRecord.doctor_patients_list(user_id=doctor_id, offset=offset)
    return return_json_data(data=data, meta=meta)


@check_user_auth
def get_patient_dialogs(request):
    from chat.models_mongo import Topic

    user_id = request.CUR_USER.id
    offset = get_int(request.GET.get("offset", 0))
    limit = 10
    new_offset = offset + limit
    medical_ids = []
    topics_dict = {}
    data = []

    # Get topics
    topics = Topic.objects.filter(is_ended=False, patient=user_id)[offset: new_offset]
    for topic in topics:
        medical_ids.append(topic.medical_record)
        topics_dict[topic.medical_record] = topic.get_json()

    # Get medical record
    records = MedicalRecord.objects.filter(id__in=medical_ids)
    for rec in records:
        to_json = rec.get_json()
        data.append(to_json)

    meta = {
        "next": get_next_url(url_name="api-v1-medical-get-patient-dialogs", offset=new_offset + 1),
        "limit": 10,
        "total_count": 0
    }

    return return_json_data(data=data, meta=meta)


@check_user_auth
def get_doctor_dialogs(request):
    from chat.models_mongo import Topic

    user_id = request.CUR_USER.id
    offset = get_int(request.GET.get("offset", 0))
    limit = 10
    new_offset = offset + limit
    medical_ids = []
    topics_dict = {}
    data = []

    # Get topics
    topics = Topic.objects.filter(members=user_id, is_ended=False, patient__ne=user_id)[offset: new_offset]
    for topic in topics:
        medical_ids.append(topic.medical_record)
        topics_dict[topic.medical_record] = topic.get_json()

    # Get medical record
    records = MedicalRecord.objects.filter(id__in=medical_ids)
    for rec in records:
        to_json = rec.get_json()
        data.append(to_json)

    meta = {
        "next": get_next_url(url_name="api-v1-medical-get-doctor-dialogs", offset=new_offset + 1),
        "limit": 10,
        "total_count": 0
    }

    return return_json_data(data=data, meta=meta)
