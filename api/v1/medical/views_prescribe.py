from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _

from medical.models import MedicalRecord, Prescribe, DrugType

from api.http_response import bad_request_response, not_found_response, return_json_data, forbidden_response
from api.decorators import method_is_post, check_user_auth
from api.tools import get_int, get_next_url, str2bool
from reminder.tools import set_drug_reminder, cancel_drug_reminder


@csrf_exempt
@check_user_auth
@method_is_post
def create_prescribe(request):
    medical_id = get_int(request.POST.get("medical_id", 0))
    drug_type = get_int(request.POST.get("drug_type", 0))
    period = get_int(request.POST.get("period", 0))
    count = get_int(request.POST.get("count", 0))

    if medical_id == 0 or period == 0 or count == 0:
        return bad_request_response(message=_("Invalid parameters"))

    # check current user is medical doctor
    medical_obj = MedicalRecord.get_medical_record(medical_id=medical_id)
    if medical_obj is None:
        return not_found_response(message=_("Medical record not found."))
    if medical_obj.doctor_id != request.CUR_USER.id:
        return forbidden_response()

    prescribe = Prescribe.create_prescribe(medical_id=medical_id,
                                           drug_type=drug_type,
                                           period=period,
                                           count=count)

    return return_json_data(data=prescribe.get_json())


@csrf_exempt
@check_user_auth
@method_is_post
def remove_prescribe(request):
    prescribe_id = get_int(request.POST.get("prescribe_id", 0))
    medical_id = get_int(request.POST.get("medical_id", 0))

    if prescribe_id == 0:
        return bad_request_response(message=_("Invalid parameters"))

    medical = MedicalRecord.get_medical_record(medical_id=medical_id)
    if medical is None:
        return not_found_response(message=_("Medical record not found"))

    if medical.doctor_id != request.CUR_USER.id:
        return forbidden_response()

    Prescribe.objects.filter(id=prescribe_id).delete()
    return return_json_data(message=_("Successfully removed prescribe"))


@csrf_exempt
@check_user_auth
@method_is_post
def get_drugs(request):
    offset = get_int(request.GET.get("offset", 0))
    parent_id = get_int(request.POST.get("parent_id", 0))
    drug_name = request.POST.get("drug_name", "")
    limit = 10

    if parent_id == 0 or len(drug_name) < 2:
        return bad_request_response(message=_("Invalid parameters"))

    drugs = DrugType.objects.filter(title__istartswith=drug_name,
                                    parent_id=parent_id)\
                            .order_by("id")[offset:offset + limit]
    data = []
    for drug in drugs:
        data.append(drug.get_json())

    meta = {
        "next": get_next_url(url_name="api-v1-medical-get-drugs",
                             offset=offset + limit + 1),
        "limit": 10,
        "total_count": 0
    }
    return return_json_data(data=data, meta=meta)


@check_user_auth
def get_type(request):
    typs = DrugType.objects.filter(parent_id__isnull=True)
    data = []

    for typ in typs:
        data.append(typ.get_json())

    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
@method_is_post
def drug_reminder(request):
    status = str2bool(request.POST.get("status", False))
    prescribe_id = get_int(request.POST.get("prescribe_id", 0))

    if prescribe_id == 0:
        return bad_request_response(message=_("Invalid parameters"))

    obj = Prescribe.get_prescribe_by_id(prescribe_id=prescribe_id)
    if obj.medical.patient_id != request.CUR_USER.id:
        return forbidden_response()

    if obj.reminder != status:
        obj.reminder = status
        obj.save()

    if status:
        set_drug_reminder(prescribe_message="", prescribe_id=obj.id, user_id=request.CUR_USER.id, start_time="now")
    else:
        cancel_drug_reminder(obj.task_id)

    return return_json_data(data=obj.get_json())
