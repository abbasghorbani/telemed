# -*- coding: utf-8 -*-

from django.conf.urls import url
from api.v1.ticketing import views

urlpatterns = [
    url(r'create/$', views.create_ticket, name='api-v1-ticket-create'),
    url(r'sendMessage/(?P<ticket_number>[0-9]+)/$', views.send_message, name='api-v1-ticket-sendMessage'),
    url(r'getUserTickets/$', views.get_tickets, name='api-v1-ticket-get-user-tickets'),
    url(r'getUserTicketMessage/(?P<ticket_number>[0-9]+)/$', views.get_ticket_messages, name='api-v1-ticket-get-user-ticket-messages'),
]
