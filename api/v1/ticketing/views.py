# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from api.decorators import check_user_auth, method_is_post
from api.http_response import bad_request_response, return_json_data, forbidden_response
from ticketing.forms import TicketForm, MessageForm
from django.http import UnreadablePostError
from ticketing.models import Ticket, Message
from api.tools import get_int, get_next_url


@csrf_exempt
@method_is_post
@check_user_auth
def create_ticket(request):
    current_user_id = request.CUR_USER.id

    try:
        form = TicketForm(request.POST)
    except UnreadablePostError:
        return bad_request_response()

    try:
        msg_form = MessageForm(request.POST, request.FILES)
    except UnreadablePostError:
        return bad_request_response()

    if not form.is_valid():
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        return bad_request_response(message={field: message[0]})

    if not msg_form.is_valid():
        message = ""
        field = ""
        for k, v in msg_form.errors.iteritems():
            message = v
            field = k
        return bad_request_response(message={field: message[0]})

    try:
        # Save ticket
        model = form.save(commit=False)
        model.user_id = current_user_id
        model.save()
        # ticket = model.get_json()

        # Save message
        msg_model = msg_form.save(commit=False)
        msg_model.ticket_id = model.id
        msg_model.user_id = current_user_id
        msg_model.save()
        msg_obj = msg_model.get_json()
        data = {"message": msg_obj}
        return return_json_data(data=data)
    except Exception as e:
        print "api ticketing create_ticket: {}".format(str(e))
        return bad_request_response(message=_("Invalid parameters"))


@csrf_exempt
@method_is_post
@check_user_auth
def send_message(request, ticket_number):
    form = MessageForm(request.POST, request.FILES)

    if not form.is_valid():
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        return bad_request_response(message={field: message[0]})
    try:
        ticket = Ticket.objects.get(ticket_number=ticket_number)
    except Exception as e:
        print "api ticketing send_message: {}".format(str(e))
        return bad_request_response(message=_("Invalid parameters"))

    if ticket.status == Ticket.CLOSE:
        return bad_request_response(message=_("This ticket is closed"))
    if not request.CUR_USER.is_superuser or not request.CUR_USER.is_staff:
        if ticket.user_id != request.CUR_USER.id:
            return forbidden_response()

    model = form.save(commit=False)
    model.user_id = request.CUR_USER.id
    model.ticket_id = ticket.id
    model.save()
    msg_obj = model.get_json()
    data = {"message": msg_obj}
    return return_json_data(data=data)


@check_user_auth
def get_tickets(request):
    offset = get_int(request.GET.get("offset", 0))
    limit = 10
    new_offset = offset + limit

    data = []
    tickets = Ticket.objects.filter(user_id=request.CUR_USER.id)[offset: new_offset]
    for ticket in tickets:
        data.append(ticket.get_json())

    meta = {
        "next": get_next_url(url_name="api-v1-ticket-get-user-tickets", offset=new_offset + 1),
        "limit": 10,
        "total_count": 0
    }

    return return_json_data(data=data, meta=meta)


@check_user_auth
def get_ticket_messages(request,ticket_number):
    offset = get_int(request.GET.get("offset", 0))
    limit = 10
    new_offset = offset + limit
    try:
        Ticket.objects.get(id=ticket_number, user_id=request.CUR_USER.id)
    except:
        return bad_request_response(message=_("ticket not found."))
    data = []
    messages = Message.objects.filter(ticket_id=ticket_number)[offset: new_offset]
    for message in messages:
        data.append(message.get_json())

    meta = {
        "next": get_next_url(url_name="api-v1-ticket-get-user-ticket-messages", offset=new_offset + 1),
        "limit": 10,
        "total_count": 0
    }

    return return_json_data(data=data, meta=meta)
