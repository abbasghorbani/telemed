from django.conf.urls import url, include

urlpatterns = [
    url(r'^auth/', include('api.v1.authentication.urls')),
    url(r'^user/', include('api.v1.user.urls')),
    url(r'^appointment/', include('api.v1.appointment.urls')),
    url(r'^medical/', include('api.v1.medical.urls')),
    url(r'^invoice/', include('api.v1.invoice.urls')),
    url(r'^ticket/', include('api.v1.ticketing.urls')),
    url(r'^chat/', include('api.v1.chat.urls')),
]
