# -*- coding: utf-8 -*-

from django.conf.urls import url
from api.v1.user import views

urlpatterns = [
    url(r'profile/$', views.user_profile, name='api-v1-user-profile'),
    url(r'profile/update/$', views.update_profile, name='api-v1-user-profile-update'),

    url(r'address/update/$', views.update_address, name='api-v1-user-address-update'),

    url(r'state/list/$', views.state_list, name='api-v1-user-state-list'),

    url(r'license/list/$', views.license_list, name='api-v1-user-license-list'),
    url(r'update/license/$', views.update_license, name='api-v1-user-update-license'),

    url(r'upload/document/$', views.upload_document, name='api-v1-user-upload-document'),
    url(r'delete/document/$', views.delete_document, name='api-v1-user-delete-document'),

    url(r'upload/education/$', views.upload_education, name='api-v1-user-upload-education'),
    url(r'delete/education/$', views.delete_education, name='api-v1-user-delete-education'),
    url(r'education/list/$', views.education_list, name='api-v1-user-education-list'),
    url(r'requestDoctor/$', views.doctor_request, name='api-v1-user-doctor-request'),
    url(r'availability/$', views.change_availability, name='api-v1-user-change-availability'),
    url(r'updateAvatar/$', views.update_avatar, name='api-v1-user-update-avatar'),
    url(r'removeAvatar/$', views.remove_avatar, name='api-v1-user-remove-avatar'),

]
