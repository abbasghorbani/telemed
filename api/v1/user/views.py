# -*- coding: utf-8 -*-
import re
import os

from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from django.http import UnreadablePostError
from django.conf import settings

from api.decorators import check_user_auth, is_doctor, method_is_post
from api.tools import get_int, str2bool
from api.http_response import return_json_data, bad_request_response, not_found_response, forbidden_response

from user_profile.models import Profile, Address, Country, LICENSE_TYPE, Doctor, Document, Education, EducationType
from user_profile.forms import ProfileForm, AddressForm, DoctorForm, DocumentForm, EducationForm, DoctorRequestForm, UpdateAvatarForm
from api.auth_cache import AuthCache


@check_user_auth
def user_profile(request):
    cur_user = request.CUR_USER
    pro_json = AuthCache.get_profile_from_id(user_id=cur_user.id)
    return return_json_data(data={'profile': pro_json})


@csrf_exempt
@check_user_auth
@method_is_post
def update_profile(request):

    profile = None
    current_user_id = request.CUR_USER.id

    first_name = request.POST.get("first_name", None)
    last_name = request.POST.get("last_name", None)
    birthdate = request.POST.get("birthdate", None)
    gender = request.POST.get("gender", None)

    if first_name is None or last_name is None or birthdate is None or gender is None:
        return bad_request_response(message=_('Error in parameters'))

    # Check parameters
    if re.match("^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِa-zA-Z_\.! ‌]{3,50}$", str(first_name)) is None:
        return bad_request_response(message=_('first_name bad characters'))

    if re.match("^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِa-zA-Z_\.! ‌]{3,50}$", str(last_name)) is None:
        return bad_request_response(message=_('last_name bad characters'))

    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
            return forbidden_response(message=_("You are not allowed to change this profile."))

    try:
        form = ProfileForm(request.POST, instance=profile)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.save()
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@method_is_post
def update_address(request):

    current_user_id = request.CUR_USER.id
    try:
        user_address = Address.objects.get(user_id=current_user_id)
    except Exception:
        user_address = None

    if user_address is None:
        try:
            form = AddressForm(request.POST)
        except UnreadablePostError:
            return bad_request_response()

        if form.is_valid():
            model = form.save(commit=False)
            model.user_id = current_user_id
            model.save()
            data = model.get_json()
            return return_json_data(data=data)
        else:
            message = ""
            field = ""
            for k, v in form.errors.iteritems():
                message = v
                field = k
            msg = {field: message[0]}
            return bad_request_response(message=msg)
    else:
        try:
            form = AddressForm(request.POST, instance=user_address)
        except UnreadablePostError:
            return bad_request_response()

        if form.is_valid():
            model = form.save(commit=False)
            if model.user_id != current_user_id:
                return forbidden_response(message=_("You are not allowed to change this address."))

            model.save()
            data = model.get_json()
            return return_json_data(data=data)
        else:
            message = ""
            field = ""
            for k, v in form.errors.iteritems():
                message = v
                field = k
            msg = {field: message[0]}
            return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@method_is_post
def state_list(request):
    parent_id = get_int(request.POST.get("parent_id", 0))
    resp = Country.get_states(obj_id=parent_id)
    return return_json_data(data=resp)


@check_user_auth
@is_doctor
def license_list(request):
    data = dict((y, x) for x, y in LICENSE_TYPE)
    return return_json_data(data=data)


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def update_license(request):
    current_user_id = request.CUR_USER.id
    try:
        user_details = Doctor.objects.get(user_id=current_user_id)
    except Exception:
        user_details = None

    if user_details is None:
        try:
            form = DoctorForm(request.POST)
        except UnreadablePostError:
            return bad_request_response()

        if form.is_valid():
            model = form.save(commit=False)
            model.user_id = current_user_id
            model.save()
            data = model.get_json()
            return return_json_data(data=data)
        else:
            message = ""
            field = ""
            for k, v in form.errors.iteritems():
                message = v
                field = k
            msg = {field: message[0]}
            return bad_request_response(message=msg)
    else:

        if user_details.is_approve:
            return bad_request_response(message=_("You are was approved and can not change your license.Please contact to administrator."))

        try:
            form = DoctorForm(request.POST, instance=user_details)
        except UnreadablePostError:
            return bad_request_response()

        if form.is_valid():
            model = form.save(commit=False)
            if model.user_id != current_user_id:
                return forbidden_response()

            model.save()
            data = model.get_json()
            return return_json_data(data=data)
        else:
            message = ""
            field = ""
            for k, v in form.errors.iteritems():
                message = v
                field = k
            msg = {field: message[0]}
            return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def upload_document(request):
    name = request.POST.get("name", None)
    # Check parameters
    if name and re.match("^[آ-ی a-zA-Z_]{3,40}", str(name)) is None:
        return bad_request_response(message=_('name bad characters'))

    try:
        form = DocumentForm(request.POST, request.FILES)
    except UnreadablePostError:
        return bad_request_response()

    try:
        doctor = Doctor.objects.get(user_id=request.CUR_USER.id)
    except Exception:
        return not_found_response(message=_("Doctor not found"))

    if form.is_valid():
        model = form.save(commit=False)
        model.doctor_id = doctor.id
        model.save()
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def delete_document(request):
    document_id = get_int(request.POST.get("document_id", 0))
    if document_id == 0:
        return bad_request_response(message=_("Invalid parameters"))

    try:
        doctor = Doctor.objects.get(user_id=request.CUR_USER.id)
    except Exception:
        return not_found_response(message=_("Doctor not found"))

    err, doc = Document.get_document(obj_id=document_id, doctor_id=doctor.id)
    if not err:
        return not_found_response(message=_("Document not found"))

    doc.delete()
    return return_json_data(message=_("Successfully deleted"))


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def upload_education(request):

    cur_user_id = request.CUR_USER.id
    univercity = request.POST.get("univercity", None)
    desc = request.POST.get("desc", None)

    if univercity is None:
        return bad_request_response(message=_('Please fill in univercity field'))

    if re.match("^[آ-ی a-zA-Z]{3,60}", str(univercity)) is None:
        return bad_request_response(message=_('name bad characters'))

    # Check parameters
    if desc and re.match("^[آ-ی a-zA-Z\n]+$", str(desc)) is None:
        return bad_request_response(message=_('name bad characters'))

    try:
        form = EducationForm(request.POST, request.FILES)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.doctor_id = cur_user_id
        model.save()
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def delete_education(request):
    cur_user_id = request.CUR_USER.id
    education_id = get_int(request.POST.get("education_id", 0))
    if education_id == 0:
        return bad_request_response(message=_("Invalid parameters"))

    err, edu = Education.get_education(obj_id=education_id, doctor_id=cur_user_id)
    if not err:
        return not_found_response(message=_("Education not found"))

    edu.delete()
    return return_json_data(message=_("Successfully deleted"))


@csrf_exempt
@check_user_auth
@is_doctor
@method_is_post
def education_list(request):
    parent_id = get_int(request.POST.get("parent_id", 0))
    resp = EducationType.get_type(obj_id=parent_id)
    return return_json_data(data=resp)


@csrf_exempt
@check_user_auth
@method_is_post
def doctor_request(request):

    cur_user_id = request.CUR_USER.id
    text = request.POST.get("text", None)

    if text is None:
        return bad_request_response(message=_('Please fill in text field'))

    try:
        form = DoctorRequestForm(request.POST)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.user_id = cur_user_id
        model.save()
        return return_json_data(message="Successfully sent your request")
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@csrf_exempt
@check_user_auth
@method_is_post
def change_availability(request):
    status = request.POST.get("status", None)
    user_id = request.CUR_USER.id
    if status is None:
        return bad_request_response(message=_("Invalid parameters"))

    profile = AuthCache.get_profile_from_id(user_id=user_id)
    if profile is None:
        return not_found_response(message=_("Profile not found"))

    # sure user is doctor
    if profile["is_doctor"] and profile["is_approve"]:
        Doctor.update_availability(user_id=user_id, is_available=str2bool(status))
        return return_json_data(message=_("Successfully update"))
    else:
        return forbidden_response()


@csrf_exempt
@check_user_auth
@method_is_post
def update_avatar(request):

    current_user_id = request.CUR_USER.id
    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
        return forbidden_response(message=_("You are not allowed to change this profile."))

    old_avatar = os.path.join(settings.MEDIA_ROOT, profile.avatar.name)

    try:
        form = UpdateAvatarForm(request.POST, request.FILES, instance=profile)
    except UnreadablePostError:
        return bad_request_response()

    if form.is_valid():
        model = form.save(commit=False)
        model.save()
        try:
            os.remove(old_avatar)
        except Exception:
            pass
        data = model.get_json()
        return return_json_data(data=data)
    else:
        message = ""
        field = ""
        for k, v in form.errors.iteritems():
            message = v
            field = k
        msg = {field: message[0]}
        return bad_request_response(message=msg)


@check_user_auth
def remove_avatar(request):
    current_user_id = request.CUR_USER.id
    try:
        profile = Profile.objects.get(user_id=current_user_id)
    except Exception:
        return not_found_response(message=_('Profile not found'))

    if profile.user_id != current_user_id:
        return forbidden_response(message=_("You are not allowed to change this profile."))

    profile.avatar.delete()
    return return_json_data(data=profile.get_json())
