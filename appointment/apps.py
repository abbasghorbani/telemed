from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class AppointmentConfig(AppConfig):
    name = 'appointment'
    verbose_name = _('Appointment')
