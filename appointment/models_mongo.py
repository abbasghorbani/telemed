# -*- coding: utf-8
from mongoengine import fields, connect, Document
from django.conf import settings
from django.utils.translation import ugettext as _
from api.tools import get_other_time_int, normalize_mongo_time_int

from user_profile.models import QUARTER, HALF, HOUR

connect(db=settings.MONGO_DB, host=settings.MONGO_DB_HOST)


SATURDAY = 5
SUNDAY = 6
MONDAY = 0
TUESDAY = 1
WEDNESDAY = 2
THURSDAY = 3
FRIDAY = 4

DAYS_LIST = [SATURDAY, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY]
DAYS = (
    (SATURDAY, _('saturday')),
    (SUNDAY, _('sunday')),
    (MONDAY, _('monday')),
    (TUESDAY, _('tuesday')),
    (WEDNESDAY, _('wednesday')),
    (THURSDAY, _('thursday')),
    (FRIDAY, _('friday')),
    )


class SourceFreeDate(Document):
    user = fields.IntField()
    day = fields.IntField(unique_with='user', choices=DAYS)
    quarter = fields.ListField(fields.IntField())
    half = fields.ListField(fields.IntField())
    hour = fields.ListField(fields.IntField())
    doctor_type = fields.IntField()

    def update(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(SourceFreeDate, self).update(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(SourceFreeDate, self).save(*args, **kwargs)


class DoctorFreeDate(Document):
    user = fields.IntField()
    day = fields.DateTimeField(unique_with='user')
    quarter = fields.ListField(fields.IntField())
    half = fields.ListField(fields.IntField())
    hour = fields.ListField(fields.IntField())
    doctor_type = fields.IntField()
    
    def update(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(DoctorFreeDate, self).update(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(DoctorFreeDate, self).save(*args, **kwargs)

    @classmethod
    def remove_time(cls, day, time_int, doctor_id, appointment_type, doctor_type):
        try:
            doctor_freeday = cls.objects.get(day=day, user=doctor_id, doctor_type=doctor_type)
        except Exception as e:
            print str(e), "|| DoctorFreeDate remove_time"
            return

        if appointment_type == QUARTER:
            doctor_freeday.update(pull__quarter=time_int)
        elif appointment_type == HALF:
            doctor_freeday.update(pull__half=time_int)
            hour_time_int = time_int/2
            doctor_freeday.update(pull__hour=hour_time_int)
        elif appointment_type == HOUR:
            doctor_freeday.update(pull__hour=time_int)
            half_time_int = (time_int * HOUR) / HALF
            doctor_freeday.update(pull_all__half=[half_time_int, half_time_int+1])
        FreeDay.remove_time(day, time_int, appointment_type, doctor_type)

    @classmethod
    def append_time(cls, day, time_int, doctor_id, appointment_type, doctor_type):
        try:
            doctor_freeday = cls.objects.get(day=day, user=doctor_id, doctor_type=doctor_type)
        except Exception:
            doctor_freeday = cls.objects.create(day=day, user=doctor_id, doctor_type=doctor_type)

        if appointment_type == QUARTER:
            doctor_freeday.update(push__quarter=time_int)
        elif appointment_type == HALF:
            doctor_freeday.update(push__half=time_int)
            if time_int % 2 == 0:
                new_time_int = time_int + 1
            else:
                new_time_int = time_int - 1
            if cls.objects.only("half").filter(day=day, user=doctor_id, half=new_time_int, doctor_type=doctor_type).count() > 0:
                times = get_other_time_int(time_int, appointment_type)
                doctor_freeday.update(__raw__={"$push": {"hour": {"$each": times}}})
                # doctor_freeday.update(push_all__hour=times)
        elif appointment_type == HOUR:
            doctor_freeday.update(push__hour=time_int)
            times = get_other_time_int(time_int, appointment_type)
            doctor_freeday.update(__raw__={"$push": {"half": {"$each": times}}})
            # doctor_freeday.update(push_all__half=times)
        FreeDay.append_time(day, time_int, appointment_type, doctor_freeday.doctor_type)


class FreeDay(Document):
    day = fields.DateTimeField(unique_with='doctor_type')
    quarter = fields.ListField(fields.IntField())
    half = fields.ListField(fields.IntField())
    hour = fields.ListField(fields.IntField())
    doctor_type = fields.IntField()

    def update(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(FreeDay, self).update(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.quarter = normalize_mongo_time_int(self.quarter, 95)
        self.half = normalize_mongo_time_int(self.half, 47)
        self.hour = normalize_mongo_time_int(self.hour, 23)
        return super(FreeDay, self).save(*args, **kwargs)

    @classmethod
    def remove_time(cls, day, time_int, appointment_type, doctor_type):
        try:
            freeday = cls.objects.get(day=day, doctor_type=doctor_type)
        except Exception as e:
            print str(e), "|| FreeDay remove_time"
            return

        if appointment_type == QUARTER:
            filters = {"day": day, "quarter": time_int, "doctor_type":doctor_type}
        if appointment_type == HALF:
            filters = {"day": day, "half": time_int, "doctor_type":doctor_type}
        if appointment_type == HOUR:
            filters = {"day": day, "hour": time_int, "doctor_type":doctor_type}

        if DoctorFreeDate.objects.only("user").filter(**filters).count() == 0:
            if appointment_type == QUARTER:
                freeday.update(pull__quarter=time_int)
            elif appointment_type == HALF:
                freeday.update(pull__half=time_int)
                hour_time_int = time_int/2
                if DoctorFreeDate.objects.only("user").filter(day=day, hour=hour_time_int, doctor_type=doctor_type).count() == 0:
                    freeday.update(pull__hour=hour_time_int)
            elif appointment_type == HOUR:
                freeday.update(pull__hour=time_int)
                half_time_int = (time_int * HOUR) / HALF
                if DoctorFreeDate.objects.only("user").filter(day=day, hour=half_time_int, doctor_type=doctor_type).count() == 0:
                    freeday.update(pull__half=half_time_int)
                if DoctorFreeDate.objects.only("user").filter(day=day, hour=half_time_int+1, doctor_type=doctor_type).count() == 0:
                    freeday.update(pull__half=half_time_int+1)

    @classmethod
    def append_time(cls, day, time_int, appointment_type, doctor_type):
        try:
            freeday = cls.objects.get(day=day, doctor_type=doctor_type)
        except Exception:
            freeday = cls.objects.create(day=day, doctor_type=doctor_type)

        if appointment_type == QUARTER:
            freeday.update(push__quarter=time_int)
        elif appointment_type == HALF:
            freeday.update(push__half=time_int)
            if time_int % 2 == 0:
                new_time_int = time_int + 1
            else:
                new_time_int = time_int - 1
            if cls.objects.only("half").filter(day=day, half=new_time_int, doctor_type=doctor_type).count() > 0:
                times = get_other_time_int(time_int, appointment_type)
                freeday.update(__raw__={"$push": {"hour": {"$each": times}}})
                # freeday.update(push_all__hour=times)
        elif appointment_type == HOUR:
            freeday.update(push__hour=time_int)
            times = get_other_time_int(time_int, appointment_type)
            freeday.update(__raw__={"$push": {"half": {"$each": times}}})
            # freeday.update(push_all__half=times)
