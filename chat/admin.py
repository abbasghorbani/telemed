# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import FreeswithchNumber
from django.contrib import admin


class FreeswithchNumberAdmin(admin.ModelAdmin):
    list_display = ('number', 'status')
    list_filter = ("status",)


admin.site.register(FreeswithchNumber, FreeswithchNumberAdmin)
