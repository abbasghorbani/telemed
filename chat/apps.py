# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class ChatConfig(AppConfig):
    name = 'chat'
    verbose_name = _('Chat')
