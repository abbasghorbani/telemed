# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
from django.core.management.base import BaseCommand
from django.utils.module_loading import import_module
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):

        def on_connect(client, userdata, flags, rc):
            print("Connected with result code " + str(rc))
            client.subscribe("#")
            client.subscribe("$SYS/brokers/+/clients/+/connected")
            client.subscribe("$SYS/brokers/+/clients/+/disconnected")

        def on_message(client, userdata, msg):
            print(msg.topic)
            try:
                get_function(topic=msg.topic, message=msg.payload)
            except Exception as e:
                print str(e)

        client = mqtt.Client(client_id="server-subMqtt-13970302", clean_session=False, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")
        client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
        client.on_connect = on_connect
        client.on_message = on_message

        try:
            client.connect(settings.MQTT_HOST, settings.MQTT_PORT, settings.MQTTT_KEEPALIVE)
            print "connect to mqtt"
        except Exception, e:
            print str(e)

        client.loop_forever()


def get_function(topic, message):
    url = get_url(str(topic))
    if url == "":
        print "URL not fond"
        return
    mod_name, func_name = url['function'].rsplit('.', 1)
    mod = import_module(mod_name)
    func = getattr(mod, func_name)
    string = topic.split("/")
    result = func(message, string[-2])
    return result


def get_url(string):
    from api.v1.chat.urls import urls
    import re

    for k, v in urls.items():
        if re.match(k, string) is None:
            continue
        return v
    return ""
