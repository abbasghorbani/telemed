# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
import requests

rSession = requests.Session()


class FreeswithchNumber(models.Model):
    class Meta:
        db_table = 'freeswithchNumber'
        verbose_name_plural = _("freeswithch Numbers")
        verbose_name = _("freeswithch Number")
    BUSY = 1
    AVAILABLE = 2
    NUMBER_STATUS = (
        (BUSY, _("Busy")),
        (AVAILABLE, _("Available"))
    )
    number = models.CharField(max_length=4, verbose_name=_("Number"), default=1001, unique=True)
    status = models.IntegerField(verbose_name=_("Status"), choices=NUMBER_STATUS, default=AVAILABLE)

    @classmethod
    def create_call_requirement(cls, title, user_ids=[]):

        url = settings.FREESWITCH_HOST + "/callRequirement"
        stmt = ""
        for i, v in enumerate(user_ids):
            if i == len(user_ids) - 1:
                stmt += str(v)
            else:
                stmt = str(v) + ","
        payload = {"user_ids": stmt, "title": title}
        res = rSession.post(url, data=payload)
        data = res.json()
        if res.status_code != 200:
            return False, data
        return True, data

    @classmethod
    def free_numbers(cls, numbers=[]):
        cls.objects.filter(number__in=numbers).update(status=cls.AVAILABLE)
        url = settings.FREESWITCH_HOST + "/deleteFile"
        stmt = ""
        for i, v in enumerate(numbers):
            if i == len(numbers) - 1:
                stmt += str(v)
            else:
                stmt = str(v) + ","
        payload = {"numbers": stmt}
        rSession.post(url, data=payload)
        # data = res.json()
        # if res.status_code != 200:
        #     return False, data
        # return True, data
