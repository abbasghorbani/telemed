# -*- coding: utf-8
import json
from hashlib import sha256
from mongoengine import fields, connect, Document

from django.conf import settings
from django.db.models.signals import post_save, post_delete
from api.auth_cache import AuthCache

from user_profile.models import Profile


connect(db=settings.MONGO_CHAT_DB,
        host=settings.MONGO_DB_HOST,
        alias="chat-db")
connect(db=settings.MONGO_MQTT_DB,
        host=settings.MONGO_DB_HOST,
        alias="mqtt-db")

SEND_MESSAGE = 1
INVITE = 2
START_CHAT = 3
ANSWER_INVITE = 4
JOIN_MESSAGE = 5
KICK = 6
NOTIF_MESSAGE = 7
NEW_MEMBER = 8
LEAVE = 9
GET_HISTORY = 10
END_CHAT = 11
DELETE = 12
EDITE = 13
ERROR = 20

TEXT = 1
PHOTO = 2
VIDEO = 3
AUDIO = 4
MEDIAMESSAGE = 5
FILE = 6


class AudioType(fields.EmbeddedDocument):
    fileID = fields.DynamicField()
    duration = fields.DynamicField()
    mimeType = fields.DynamicField()
    fileSize = fields.DynamicField()
    url = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(AudioType, self).__init__(*args, **kwargs)
        self.fileID = kwargs.get('fileID')
        self.duration = kwargs.get('duration')
        self.mimeType = kwargs.get('mimeType')
        self.fileSize = kwargs.get('fileSize')
        return


class PhotoSize(fields.EmbeddedDocument):
    fileID = fields.DynamicField()
    width = fields.DynamicField()
    height = fields.DynamicField()
    fileSize = fields.DynamicField()
    url = fields.DynamicField()
    mimeType = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(PhotoSize, self).__init__(*args, **kwargs)
        self.fileID = kwargs.get('fileID')
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.fileSize = kwargs.get('fileSize')
        self.url = kwargs.get('url')
        return


class PhotoType(fields.EmbeddedDocument):
    photo = fields.EmbeddedDocumentField(PhotoSize)
    thumbnail = fields.ListField(fields.DynamicField(), default=[])

    def __init__(self, *args, **kwargs):
        super(PhotoType, self).__init__(*args, **kwargs)
        self.photo = kwargs.get('photo')
        return


class DocumentType(fields.EmbeddedDocument):
    fileID = fields.DynamicField()
    mimeType = fields.DynamicField()
    fileSize = fields.DynamicField()
    url = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(DocumentType, self).__init__(*args, **kwargs)
        self.fileID = kwargs.get('fileID')
        self.mimeType = kwargs.get('mimeType')
        self.fileSize = kwargs.get('fileSize')
        self.url = kwargs.get('url')
        return


class VideoType(fields.EmbeddedDocument):
    fileID = fields.DynamicField()
    width = fields.DynamicField()
    height = fields.DynamicField()
    thumbnail = fields.EmbeddedDocumentField(PhotoSize)
    duration = fields.DynamicField()
    fileSize = fields.DynamicField()
    mimeType = fields.DynamicField()
    url = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(VideoType, self).__init__(*args, **kwargs)
        self.fileID = kwargs.get('fileID')
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.thumbnail = kwargs.get('thumbnail')
        self.duration = kwargs.get('duration')
        self.fileSize = kwargs.get('fileSize')
        self.mimeType = kwargs.get('mimeType')
        return


class MediaMessage(fields.EmbeddedDocument):
    fileID = fields.DynamicField()
    duration = fields.DynamicField()
    mimeType = fields.DynamicField()
    fileSize = fields.DynamicField()
    thumbnail = fields.EmbeddedDocumentField(PhotoSize)

    def __init__(self, *args, **kwargs):
        super(MediaMessage, self).__init__(*args, **kwargs)
        self.fileID = kwargs.get('fileID')
        self.duration = kwargs.get('duration')
        self.mimeType = kwargs.get('mimeType')
        self.fileSize = kwargs.get('fileSize')
        self.thumbnail = kwargs.get('thumbnail')
        return


class FromUser(fields.EmbeddedDocument):
    user_id = fields.DynamicField()
    first_name = fields.DynamicField()
    last_name = fields.DynamicField()
    avatar = fields.DynamicField()
    medical_number = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(FromUser, self).__init__(*args, **kwargs)


class UserType(fields.EmbeddedDocument):
    user_id = fields.DynamicField()
    first_name = fields.DynamicField()
    last_name = fields.DynamicField()
    avatar = fields.DynamicField()
    medical_number = fields.DynamicField()

    def __init__(self, *args, **kwargs):
        super(UserType, self).__init__(*args, **kwargs)
        # try:
        #     self.user_id = kwargs.get('user_id')
        #     profile = AuthCache.get_profile_from_id(self.user_id)
        #     self.first_name = profile["first_name"]
        #     self.last_name = profile["last_name"]
        #     self.avatar = profile["avatar"]
        #     self.medical_number = profile["medical_number"]
        # except Exception as e:
        #     print str(e), "chat api: model_mongo: UserType class"
        # return


class Topic(Document):
    title = fields.StringField()
    hashcode = fields.StringField(unique=True)
    members = fields.ListField(fields.IntField(), default=[])
    is_ended = fields.BooleanField(default=False)
    medical_record = fields.IntField()
    owner = fields.IntField()
    patient = fields.IntField()
    pending_invited = fields.ListField(fields.IntField(), default=[])
    call_info = fields.DynamicField()

    meta = {'indexes': ['hashcode'], "db_alias": "chat-db"}

    def get_json(self):
        from api.tools import get_topic_uri, get_server_topic_uri
        from api.auth_cache import AuthCache

        members_obj = []
        for member in self.members:
            p = AuthCache.get_profile_from_id(user_id=member)
            if p is None:
                print "chat models_mongo Topic class: profile with user_id {} does not exist".format(member)
                continue
            members_obj.append(p)

        data = {
            "id": str(self.id),
            "title": self.title,
            "hashcode": self.hashcode,
            "members": members_obj,
            "member_count": len(self.members),
            "is_ended": self.is_ended,
            "topic": get_topic_uri(self.hashcode),
            "publish": get_server_topic_uri(self.hashcode),
            "medical_id": self.medical_record,
            "doctor": AuthCache.get_profile_from_id(user_id=self.owner),
            "call_info": json.loads(self.call_info)
        }
        return data

    def update_invited_user_list(self, user_ids, flag="push"):
        if flag == "push":
            query = {"$push": {"pending_invited": {"$each": user_ids}}}
        else:
            query = {"$pullAll": {"pending_invited": {"$each": user_ids}}}

        self.update(__raw__=query)

    @classmethod
    def create_topic(cls, title, medical_record, owner, patient, members=[], numbers=[]):
        from api.tools import get_random_string

        hashcode = get_random_string(10)
        status = True
        while status:
            count = cls.objects.only('hashcode')\
                .filter(hashcode=hashcode).count()
            if count == 0:
                try:
                    call_info = json.dumps({owner: numbers[0], patient: numbers[1]})
                    obj = cls.objects.create(title=title,
                                             hashcode=hashcode,
                                             members=members,
                                             owner=owner,
                                             patient=patient,
                                             medical_record=medical_record,
                                             call_info=call_info)
                    print "create topic {}".format(hashcode)
                    status = False
                    return obj
                except Exception as e:
                    print str(e), " || chat models_mongo: Topic create_topic function"
                    return None
        return None

    @classmethod
    def is_exists(cls, hashcode):
        try:
            count = cls.objects.only('hashcode').\
                filter(hashcode__exists=hashcode).count()
            if count == 0:
                return False
            else:
                return True
        except Exception as e:
            print str(e), " || chat models_mongo: Topic is_exists function"
            return False

    @classmethod
    def is_member(cls, user_id, hashcode):
        try:
            count = cls.objects.only('hashcode').\
                filter(members=user_id, hashcode=hashcode).count()
            if count == 0:
                return False
            else:
                return True
        except Exception as e:
            print str(e), " || chat models_mongo: topic is_member function"
            return False

    @classmethod
    def close_topic(cls, medical_id):
        from api.auth_cache import AuthCache
        from api.tools import get_topic_uri, get_server_topic_uri
        from models import FreeswithchNumber

        try:
            topic = cls.objects.get(medical_record=medical_id)
        except Exception as e:
            text = " ||chat models_mongo: Topic close_topic with medical_id:{} not found".format(medical_id)
            print str(e), text
            return

        topic.is_ended = True
        topic.save()

        # Topics
        group_topic = get_topic_uri(hashcode=topic.hashcode)
        server_topic = get_server_topic_uri(hashcode=topic.hashcode)
        for member in topic.members:
            profile = AuthCache.get_profile_from_id(user_id=member)
            if profile is None:
                print "chat models_mongo close_topic: profile with user_id {} does not exist".format(member)
                continue
            MqttAcl.update_acl(username=profile["medical_number"],
                               flag="pull",
                               subscribe=[group_topic],
                               publish=[server_topic])

        cls.send_close_session(topic.members, topic)

        # Free number in freeswitch
        call_info = json.loads(topic.call_info)
        FreeswithchNumber.free_numbers(numbers=call_info.values())

    @classmethod
    def send_close_session(cls, members, topic):
        from api.tools import get_user_topic
        from notification.message import leave_message
        from api.v1.chat.views_mqtt import multiple_pubish

        msgs = []
        for member in members:
            json_data = leave_message(topic, member)
            user_topic = get_user_topic(member)
            msgs.append({
                'topic': user_topic,
                'payload': json_data,
                'qos': 1,
                'retain': False,
            })

        # Send packet
        multiple_pubish(msgs)
        return

    @classmethod
    def get_topic_obj(cls, hashcode):
        try:
            topic = cls.objects.get(hashcode=hashcode)
        except Exception:
            topic = None

        return topic


class Message(Document):
    topic = fields.StringField(required=True)
    packet_id = fields.DynamicField(required=True)
    create_at = fields.IntField(required=True)
    packet_type = fields.IntField(required=True)
    message_type = fields.IntField(required=True)
    from_user = fields.EmbeddedDocumentField(FromUser)
    reply_to_message = fields.DynamicField(null=True)
    text = fields.DynamicField(null=True)
    caption = fields.DynamicField(null=True)
    audio = fields.EmbeddedDocumentField(AudioType, null=True)
    document = fields.EmbeddedDocumentField(DocumentType, null=True)
    photo = fields.EmbeddedDocumentField(PhotoType, null=True)
    video = fields.EmbeddedDocumentField(VideoType, null=True)
    media_message = fields.EmbeddedDocumentField(MediaMessage, null=True)
    new_members = fields.ListField(fields.EmbeddedDocumentField(FromUser), default=[])
    left_members = fields.ListField(fields.EmbeddedDocumentField(FromUser), default=[])

    meta = {'indexes': ['from_user', 'topic', 'packet_id'],
            "db_alias": "chat-db"}

    def __init__(self, *args, **kwargs):

        for key, value in kwargs.items():
            try:
                self.__dict__[key] = value
            except Exception as e:
                print str(e), "chat models_mongo: init function error"
                print key
        super(Message, self).__init__(*args, **kwargs)
        return

    def get_json(self):
        obj_dict = json.loads(self.to_json())
        try:
            data = obj_dict.pop("_id")
        except Exception:
            data = obj_dict
        return data

    def is_valid(self):
        if self.topic is None or self.packet_id is None or self.create_at is None or self.packet_type is None:
            msg = "chat models_mongo is_valid: Invalid parameters"
            return False, msg

        topic = Topic.get_topic_obj(hashcode=self.topic)
        if topic is None:
            msg = "chat models_mongo is_valid: topic not found"
            return False, msg

        if self.from_user.user_id not in topic.members:
            msg = "chat models_mongo is_valid: from_user can not allowed send message to this topic"
            return False, msg

        return True, ""

    def update_from_user(self, token):
        user_id = AuthCache.get_id_from_token(token=token)
        if user_id is None:
            print "user with token {} does not exist".format(token)
            return False

        profile = AuthCache.get_profile_from_id(user_id=user_id)
        if Profile is None:
            print "Profile with user_id {} does not exist".format(user_id)
            return False
        data = {
            "user_id": profile["user_id"],
            "first_name": profile["first_name"],
            "last_name": profile["last_name"],
            "avatar": profile["avatar"],
            "medical_number": profile["medical_number"]
        }
        self.from_user = FromUser(**data)
        return True

    def send_message(self, data):
        from api.v1.chat.views_mqtt import single_pubish
        from api.tools import get_topic_uri

        status, msg = self.is_valid()
        if not status:
            print msg, " || chat models_mongo send_message: message is not valid"
            return False

        try:
            msg = self.get_json()
            data["payload"] = msg
            topic = get_topic_uri(self.topic)
            single_pubish(topic, json.dumps(data), qos=2)
            return True
        except Exception as e:
            print str(e), " || chat models_mongo: send_message function"
            return False


class MqttUser(Document):
    username = fields.StringField(unique=True)
    password = fields.StringField()
    is_superuser = fields.BooleanField()

    meta = {'indexes': ['username'], "db_alias": "mqtt-db"}

    @classmethod
    def create_mqtt_user(cls, sender, instance, *args, **kwargs):
        if kwargs["created"]:
            from api.tools import get_user_topic
            publish = []
            subscribe = []
            is_superuser = instance.user.is_superuser

            if is_superuser:
                username = instance.user.username
                hash_password = sha256(settings.MQTT_PASSWORD).hexdigest()
            else:
                username = instance.medical_number
                hash_password = sha256(instance.phone).hexdigest()

            cls.objects.create(username=username,
                               password=hash_password,
                               is_superuser=is_superuser)

            try:
                topic = get_user_topic(instance.user.id)
                if is_superuser:
                    publish = ['#']
                    subscribe = ['#']
                else:
                    subscribe = [topic]
                    publish = []

                MqttAcl.objects.create(username=username,
                                       publish=publish,
                                       subscribe=subscribe)
            except Exception as e:
                print str(e), " || chat models_mongo: MqttUser create_user function"

    @classmethod
    def remove_mqtt_user(cls, sender, instance, *args, **kwargs):
        try:
            # Remove mqtt users
            is_superuser = instance.user.is_superuser
            if is_superuser:
                username = instance.user.username
            else:
                username = instance.medical_number

            cls.objects.filter(username=username).delete()
            # Remove user acl
            MqttAcl.remove_user_acl(username=username)
        except Exception as e:
            print str(e), " || chat models_mongo: MqttUser remove_user function"


class MqttAcl(Document):
    publish = fields.ListField(default=[])
    subscribe = fields.ListField(default=[])
    username = fields.StringField(unique=True)

    meta = {'indexes': ['username'], "db_alias": "mqtt-db"}

    @classmethod
    def create_acl(cls, sender, instance, *args, **kwargs):
        if kwargs["created"]:
            from api.tools import get_user_topic
            topic = get_user_topic(instance.user.id)
            try:
                cls.objects.create(username=instance.medical_number,
                                   subscribe=[topic],
                                   publish=[])
            except Exception as e:
                print str(e), " || chat models_mongo: MqttACL create_acl function"

    @classmethod
    def update_acl(cls, username, flag, subscribe=[], publish=[]):
        if flag == "pull":
            query = {"$pullAll": {"subscribe": subscribe, "publish": publish}}
            # query = {"pull_all__subscribe": subscribe,
            #          "pull_all__publish": publish}
        else:
            query = {"$push": {"subscribe": {"$each": subscribe}, "publish": {"$each": publish}}}
            # query = {"push_all__subscribe": subscribe,
            #          "push_all__publish": publish}

        try:
            cls.objects.filter(username=username).update(__raw__=query)
        except Exception as e:
            print str(e), " || chat models_mongo: MqttAcl update_acl function occurred error"

    @classmethod
    def remove_user_acl(cls, username):
        cls.objects.filter(username=username).delete()


""" Signal """
post_save.connect(MqttUser.create_mqtt_user, sender=Profile)
post_delete.connect(MqttUser.remove_mqtt_user, sender=Profile)
