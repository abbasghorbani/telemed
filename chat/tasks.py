# -*- coding: utf-8

from telemed.celery import app

from chat.models_mongo import Topic, MqttAcl

from api.auth_cache import AuthCache
from api.v1.chat.views_mqtt import send_join_message, end_chat_request
from api.tools import get_topic_uri, get_server_topic_uri, local2UTC
from datetime import datetime


@app.task(name="chat.start_chat")
def start_chat(title, medical_record, owner, patient, start_session, end_session, members=[], numbers=[]):

    if datetime.fromtimestamp(int(start_session)) < datetime.now():
        print "The time of this medical_record:{} is over ".format(medical_record)
        return

    # Create topic
    topic = Topic.create_topic(title=title,
                               members=members,
                               medical_record=medical_record,
                               owner=owner,
                               patient=patient,
                               numbers=numbers)
    hashcode = topic.hashcode
    if hashcode is None:
        print "topic for medial_record:{} does not create".format(medical_record)
        return

    # Close session
    end = local2UTC(datetime.fromtimestamp(int(end_session)))
    # total_secodns = ((diff.days * 86400) + diff.seconds)
    end_chat.apply_async(args=[hashcode], eta=end)

    # Update members acl
    group_topic = get_topic_uri(hashcode)
    server_topic = get_server_topic_uri(hashcode)

    for member in members:
        profile = AuthCache.get_profile_from_id(user_id=member)
        if profile is None:
            print "tasks start_chat: profile with user_id {} does not exist".format(member)
            continue

        MqttAcl.update_acl(username=profile["medical_number"],
                           subscribe=[group_topic],
                           publish=[server_topic],
                           flag="push")

    # Send Invite for all member
    send_join_message(hashcode, members)
    print "create :::::::::::::::::::::::::::::"


@app.task(name="chat.end_chat")
def end_chat(hashcode):
    end_chat_request(hashcode)


# @app.task(name="chat.hello")
# def hello():
#     # from django.utils import timezone
#     from datetime import timedelta, datetime

#     print "hello"
#     end = local2UTC(datetime.now() + timedelta(seconds=20))
#     hello.apply_async(eta=end)
