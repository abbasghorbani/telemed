package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

var mySQLCon *sql.DB

const (
	BUSY          = 1
	AVAILABLE     = 2
	MsgBadRequest = "Bad request."
	MsgNotFound   = "Not found."
)

// Response struct
type Response struct {
	Message string   `json:"message"`
	Numbers []string `json:"numbers"`
}

// GetMysqlConn establish a new connection
func GetMysqlConn() *sql.DB {
	if mySQLCon == nil {
		con, err := sql.Open("mysql", "telemed:-)(**Z{QT@tcp(192.168.11.50:3306)/telemed")
		if err != nil {
			panic(err.Error())
		}
		mySQLCon = con
		mySQLCon.SetMaxIdleConns(5000)
	}
	return mySQLCon
}

// HTTPError http error struct
type HTTPError struct {
	Msg    string `json:"message"`
	Status int    `json:"status"`
}

// NewHTTPError new http error
func NewHTTPError(msg string, status int) *HTTPError {
	return &HTTPError{
		Msg:    msg,
		Status: status}
}

func (e *HTTPError) Error() string { return e.Msg }

// GetJSON get json http error
func (e *HTTPError) GetJSON() []byte {
	j, _ := json.Marshal(e)
	return j
}

// WriteResponse write response
func WriteResponse(w http.ResponseWriter, msg []byte, status int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	w.Write(msg)
}

// WriteResponseOK write bad request
func WriteResponseOK(w http.ResponseWriter, msg []byte) {
	WriteResponse(w, msg, http.StatusOK)
}

// WriteResponseBadRequest write bad request
func WriteResponseBadRequest(w http.ResponseWriter, msg []byte) {
	// msg := NewHTTPError(message, http.StatusBadRequest).GetJSON()
	WriteResponse(w, msg, http.StatusBadRequest)
}

// GetFreeswitchNumber
func GetFreeswitchNumber(limit int) ([]string, error) {
	con := GetMysqlConn()
	query := `SELECT number FROM telemed.freeswithchNumber WHERE status=2 limit ?`
	rows, err := con.Query(query, limit)
	numbersList := []string{}

	if err != nil {
		log.Println(err.Error())
		return numbersList, err
	}

	defer rows.Close()
	for rows.Next() {
		var number string
		err = rows.Scan(&number)
		if err != nil {
			log.Println(err.Error())
			return numbersList, err
		}
		numbersList = append(numbersList, number)
	}
	if len(numbersList) < limit {
		err := errors.New("All communications lines are currently occupied .Please try again.")
		log.Println(err.Error())
		return numbersList, err
	}
	return numbersList, nil
}

// CreateXMLFile get user ids and create xml file
func CreateXMLFile(userIDs []string, numbers []string, title string) error {
	callerName := "$${outbound_caller_name}"
	callerID := "$${outbound_caller_id}"
	content := `<include>
				<user id="%s">
					<params>
						<param name="password" value="%s"/>
						<param name="vm-password" value="%s"/>
					</params>
					<variables>
						<variable name="toll_allow" value="domestic,international,local"/>
						<variable name="accountcode" value="%s"/>
						<variable name="user_context" value="default"/>
						<variable name="effective_caller_id_name" value="%s"/>
						<variable name="effective_caller_id_number" value="%s"/>
						<variable name="outbound_caller_id_name" value="%s"/>
						<variable name="outbound_caller_id_number" value="%s"/>
						<variable name="callgroup" value="techsupport"/>
					</variables>
				</user>
			</include>`

	con := GetMysqlConn()
	for i, userID := range userIDs {
		intUserID, err := strconv.Atoi(userID)
		if err != nil {
			log.Println(err.Error())
			return err
		}
		phone := ""
		query := `SELECT phone FROM telemed.profile WHERE user_id=?`
		err = con.QueryRow(query, intUserID).Scan(&phone)
		if err != nil {
			log.Println(err.Error())
			return err
		}
		accountcode := numbers[i]
		password := phone
		fileContent := fmt.Sprintf(content, accountcode, password, accountcode, accountcode, title, accountcode, callerName, callerID)
		d1 := []byte(fileContent)
		filename := fmt.Sprintf("/usr/local/freeswitch/conf/directory/default/%s.xml", accountcode)
		err = ioutil.WriteFile(filename, d1, 0644)
		if err != nil {
			log.Println(err.Error())
			return err
		}
	}
	return nil
}

// UpdateFreeswitchNumber get numbers and update status
func UpdateFreeswitchNumber(status int, numbers []string) error {
	con := GetMysqlConn()
	sql := "UPDATE telemed.freeswithchNumber SET status=? where number IN ("
	for i, val := range numbers {
		if i == len(numbers)-1 {
			sql = sql + val + ")"
		} else {
			sql = sql + val + ","
		}
	}

	_, err := con.Exec(sql, status)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

// CreateFreeswitchRequirement create
func CreateFreeswitchRequirement(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	response := new(Response)
	if err != nil {
		log.Println(err.Error())
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}

	strUserIds := r.FormValue("user_ids")
	title := r.FormValue("title")
	userIDs := strings.Split(strUserIds, ",")

	if len(userIDs) == 0 || len(title) == 0 {
		err := errors.New("Invalid parameters")
		log.Println(err.Error())
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}
	numbersList, err := GetFreeswitchNumber(len(userIDs))
	if err != nil {
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}

	err = CreateXMLFile(userIDs, numbersList, title)
	if err != nil {
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}

	err = UpdateFreeswitchNumber(BUSY, numbersList)
	if err != nil {
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}

	response.Message = "successfully create permission file"
	response.Numbers = numbersList
	res, _ := json.Marshal(response)
	WriteResponseOK(w, res)
	return
}

func DeleteXMLFiles(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	response := new(Response)
	if err != nil {
		log.Println(err.Error())
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}
	strFilesname := r.FormValue("numbers")
	filenames := strings.Split(strFilesname, ",")

	if len(filenames) == 0 {
		err := errors.New("Invalid parameters")
		log.Println(err.Error())
		response.Message = err.Error()
		res, _ := json.Marshal(response)
		WriteResponseBadRequest(w, res)
		return
	}
	for _, val := range filenames {
		path := fmt.Sprintf("/usr/local/freeswitch/conf/directory/default/%s.xml", val)
		var err = os.Remove(path)
		if err != nil {
			log.Println(err.Error())
			response.Message = err.Error()
			res, _ := json.Marshal(response)
			WriteResponseBadRequest(w, res)
			return
		}
		fmt.Println("==> done deleting file")
	}
	response.Message = "successfully remove files"
	res, _ := json.Marshal(response)
	WriteResponseOK(w, res)
	return
}
func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)
	router := mux.NewRouter()
	router.HandleFunc("/callRequirement", CreateFreeswitchRequirement).Methods("POST")
	router.HandleFunc("/deleteFile", DeleteXMLFiles).Methods("POST")
	fmt.Println("Starting server at http://127.0.0.1:2000")
	log.Fatal(http.ListenAndServe(":2000", router))
}
