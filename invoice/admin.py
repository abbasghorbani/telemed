# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import CreditLog, Invoice


class CreditLogAdmin(admin.ModelAdmin):
    list_display = ("user", "status", "amount", "create_at")
    raw_id_fields = ("user",)


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ("user", "amount", "status", "description", "invoice_number", "content_type", "object_id", )
    raw_id_fields = ("user",)
    list_filter = ("status",)


# class IsVeryBenevolentFilter(admin.SimpleListFilter):
#     title = 'Created'
#     parameter_name = 'Created'

#     def lookups(self, request, model_admin):
#         return (
#             ('Today', 'today'),
#             ('Yesterday', 'yesterday'),
#             ('Last week', 'last_week'),
#             ('Last month', 'last_month'),
#         )

#     def queryset(self, request, queryset):
#         value = self.value()
#         if value == 'Yes':
#             return queryset.filter(benevolence_factor__gt=75)
#         elif value == 'No':
#             return queryset.exclude(benevolence_factor__gt=75)
#         return queryset


admin.site.register(CreditLog, CreditLogAdmin)
admin.site.register(Invoice, InvoiceAdmin)
