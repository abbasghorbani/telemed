# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class InvoiceConfig(AppConfig):
    name = 'Invoice'
    verbose_name = _('Invoice')

