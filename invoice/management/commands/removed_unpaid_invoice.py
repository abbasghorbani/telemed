# -*- coding:utf-8 -*-
import time
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType

from invoice.models import Invoice
from appointment.models_mongo import DoctorFreeDate
from user_profile.models import HALF, HOUR_HALF

from api.tools import get_doctor, get_type_time_int


class Command(BaseCommand):

    def handle(self, *args, **options):
        while True:

            diff = int((datetime.now() - timedelta(minutes=10)).strftime("%s"))
            content_type = ContentType.objects.only('id').get(app_label="medical", model="medicalrecord")
            invoices = Invoice.objects.filter(status=Invoice.UNPAID, create_at__lte=diff, content_type=content_type.id)

            for invoice in invoices:
                if invoice.content_object is None:
                    try:
                        print "delete invoice {}".format(invoice.id)
                        invoice.delete()
                    except Exception as e:
                        print str(e)
                    continue

                doctor_id = invoice.content_object.doctor_id
                doctor = get_doctor(doctor_id, is_approve=False)
                if not doctor:
                    print "doctor with id:{} not found".format(doctor_id)
                    continue

                if doctor.appointment_type != HOUR_HALF:
                    time_int_list = get_type_time_int(doctor.appointment_type, invoice.content_object.start_session, invoice.content_object.end_session)
                    for time_int in time_int_list:
                        DoctorFreeDate.append_time(invoice.content_object.visit_day, time_int, doctor.user_id, doctor.appointment_type, doctor.license_type)
                elif doctor.appointment_type == HOUR_HALF:
                    half_time_int_list = get_type_time_int(HALF, invoice.content_object.start_session, invoice.content_object.end_session)
                    for time_int in half_time_int_list:
                        DoctorFreeDate.append_time(invoice.content_object.visit_day, time_int, doctor.user_id, HALF, doctor.license_type)
                try:
                    invoice.content_object.delete()
                    print "delete medical {}".format(invoice.object_id)
                    print "delete invoice {}".format(invoice.id)
                    invoice.delete()
                except Exception as e:
                    print str(e)

            time.sleep(10*60)
