# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid
from datetime import datetime, timedelta

from django.db import models
from django.contrib.auth.models import User
from django.core import serializers
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.models import ContentType

from medical.models import APPROVE

from api.tools import simple_user_json, local2UTC, get_user_topic
from api.auth_cache import AuthCache

from notification.tasks import send_notif
from notification.notif import Notification
from notification.message import start_chat_message


class Invoice(models.Model):
    class Meta:
        db_table = 'invoice'
        verbose_name_plural = _("Invoices")
        verbose_name = _("Invoice")
        unique_together = ("object_id", "content_type")

    PAID = 1
    UNPAID = 2
    RETURN = 3

    STATUS_CHOICES = (
        (PAID, _('Paid')),
        (UNPAID, _('unpaid')),
        (RETURN, _('return')),
    )

    user = models.ForeignKey(User, verbose_name=_("User"))
    amount = models.IntegerField(default=0, null=True, blank=True, verbose_name=_("Amount"))
    status = models.IntegerField(choices=STATUS_CHOICES, default=UNPAID, verbose_name=_("Mode"))
    description = models.TextField(verbose_name=_("Description"), null=True, blank=True)
    invoice_number = models.CharField(max_length=36, verbose_name=_("Invoce number"), db_index=True)
    object_id = models.PositiveIntegerField(verbose_name=_("Object ID"))
    content_type = models.ForeignKey(ContentType, verbose_name=_("Type"))
    content_object = GenericForeignKey('content_type', 'object_id')
    create_at = models.IntegerField(verbose_name=_("Create at"), default=0)

    def save(self, *args, **kwargs):
        if not self.id:
            # make a UUID based on the host ID and current time
            self.invoice_number = str(uuid.uuid1()).replace("-", "")
            self.create_at = datetime.now().strftime("%s")
        super(Invoice, self).save(*args, **kwargs)

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['user'] = simple_user_json(self.user)
        data['content_type'] = self.content_object.get_json()

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    def after_paid_invoice(self, online=False):
        from medical.models import MedicalRecord

        content_type = ContentType.objects.only('id').get(app_label="medical", model="medicalrecord")
        if self.content_type_id == content_type.id:
            # Update doctor visited list
            self.content_object.update_doctor_visited_list()

            # Get doctor info from cache
            doc_profile = AuthCache.get_profile_from_id(user_id=self.content_object.doctor_id)
            if doc_profile is None:
                first_name = ""
                last_name = ""
            else:
                first_name = doc_profile["first_name"]
                last_name = doc_profile["last_name"]

            title = "{} {} {}".format(_("Meeting with Dr."), first_name, last_name)

            # update medical_record status
            self.content_object.status = APPROVE
            self.content_object.save()

            # Create notif data
            members = [self.content_object.patient_id, self.content_object.doctor_id]
            msgs = []

            json_data = start_chat_message(self.content_object, title)
            for member in members:
                msgs.append({
                    'topic': get_user_topic(member),
                    'payload': json_data,
                    'qos': 1,
                    'retain': False,
                })
            if online:
                # send notif to users
                sender = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
                sender.send()

            else:
                start_time = self.content_object.start_session - timedelta(seconds=30)
                result = send_notif.apply_async(args=[msgs], eta=local2UTC(start_time))
                MedicalRecord.update_task_id(task_id=result.task_id, medical_id=self.content_object.id)
        else:
            print "Invoce models || after_paid_invoice function Error"

    @classmethod
    def get_invoice_by_id(cls, invoice_number, user_id):
        try:
            invoice = cls.objects.get(invoice_number=invoice_number, user_id=user_id)
            return invoice
        except Exception as e:
            print str(e)
            return None

    @classmethod
    def get_invoice_by_medical_id(cls, medical_id):
        try:
            obj = cls.objects.get(object_id=medical_id)
            return obj
        except Exception as e:
            print str(e)
            return None

    @classmethod
    def user_all_invoices(cls, user_id, limit=10, offset=0, start_date=0, end_date=0):
        if start_date != 0 and end_date != 0:
            invoices = cls.objects.filter(user_id=user_id, create_at__range=(start_date, end_date)).order_by("-id")[offset:limit + offset]
        else:
            invoices = cls.objects.filter(user_id=user_id).order_by("-id")[offset:limit + offset]
        data = []
        for invoice in invoices:
            data.append(invoice.get_json())
        return data

    @classmethod
    def create_invoice(cls, amount, user_id, content_object):
        try:
            obj = cls.objects.create(user_id=user_id, amount=amount, content_object=content_object)
            return obj
        except Exception as e:
            print str(e)
            return None


class CreditLog(models.Model):
    class Meta:
        db_table = 'credit_log'
        verbose_name_plural = _("CreditLogs")
        verbose_name = _("CreditLog")

    UNCOMPLETED = 0
    COMPLETED = 1
    FAKERY = 2
    SERVER_ERROR = 3

    INCREASE = 1
    DECREASE = 2

    STATUS_CHOICES = (
        (UNCOMPLETED, _('Uncompleted')),
        (COMPLETED, _('Completed')),
        (FAKERY, _('Fakery')),
        (SERVER_ERROR, _('Server error')),
    )

    MODE_CHOICES = (
        (INCREASE, _("Increase")),
        (DECREASE, _("Decrease"))
    )
    REVERSE_STATUS = {0: _('Uncompleted'), 1: _('Completed'), 2: _('Fakery'), 3: _('Server error')}
    REVERSE_MODE = {1: _('Increase'), 2: _('Decrease')}

    user = models.ForeignKey(User, verbose_name=_("User"))
    create_at = models.IntegerField(verbose_name=_("Create at"), default=0)
    amount = models.IntegerField(blank=True, null=True, verbose_name=_("Amount"))
    cur_credit = models.IntegerField(default=0, verbose_name=_("Current credit"))
    mode = models.IntegerField(default=INCREASE, verbose_name=_("Mode"), choices=MODE_CHOICES)
    status = models.IntegerField(default=UNCOMPLETED, choices=STATUS_CHOICES, verbose_name=_("Status"))
    trans_id = models.CharField(max_length=250, blank=True,
                                null=True, db_index=True, verbose_name=_("Transaction ID"))

    def save(self, *args, **kwargs):
        if not self.id:
            self.create_at = datetime.now().strftime("%s")
        super(CreditLog, self).save(*args, **kwargs)

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['user'] = simple_user_json(self.user)

        data['status'] = self.REVERSE_STATUS[self.status]
        data['mode'] = self.REVERSE_MODE[self.mode]

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    @classmethod
    def create_credit_log(cls, user_id, amount, status, mode, trans_id=None):
        profile = AuthCache.get_profile_from_id(user_id=user_id)
        if profile is None:
            print "invoice models create_credit_log : profile with user_id {} does not exist".format(user_id)
            return False

        cur_credit = int(profile["credit"])
        if cur_credit < 0:
            cur_credit = 0

        try:
            cls.objects.create(user_id=user_id, amount=amount,
                               status=status, trans_id=trans_id,
                               mode=mode, cur_credit=cur_credit)
            return True
        except Exception as e:
            print str(e)
            return False

    @classmethod
    def get_credit_logs(cls, user_id, offset=0, limit=10):
        objects = cls.objects.filter(user_id=user_id).order_by('-id')[offset:offset + limit]
        result = []
        for obj in objects:
            result.append(obj.get_json())
        return result
