# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Register your models here.

from django.contrib import admin
from models import File


class FileAdmin(admin.ModelAdmin):
    list_display = ('owner', 'members', 'topic', "fileID", "mime_type", "create_at")
    raw_id_fields = ("owner",)


admin.site.register(File, FileAdmin)