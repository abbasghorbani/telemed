# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.core import serializers

from api.tools import simple_user_json
import json


class File(models.Model):
    class Meta:
        db_table = 'file'
        verbose_name_plural = _("files")
        verbose_name = _("file")

    # info = JSONField(verbose_name=_("info"))
    info = models.TextField(verbose_name=_("info"))
    topic = models.CharField(max_length=50, verbose_name=_("topic"))
    members = models.CharField(max_length=100, verbose_name=_("members"))
    owner = models.ForeignKey(User, verbose_name=_("file owner"))
    fileID = models.CharField(verbose_name=_("FileID"), max_length=100, db_index=True)
    mime_type = models.CharField(verbose_name="Mime type", max_length=100)
    create_at = models.CharField(verbose_name=_("create at"), max_length=10)
    path = models.CharField(verbose_name=_("path"), max_length=255, default="")

    def get_json(self, fields=None, exclude=None):
        def need_fields(file_object):
            final_object = {}
            if fields:
                final_object = {
                    field: file_object[field] for field in fields
                }
            else:
                final_object = file_object

            if exclude:
                if not fields:
                    final_object = file_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['owner'] = simple_user_json(user=self.owner)
        try:
            data["info"] = json.loads(self.info)
        except Exception as e:
            print str(e)
            data["info"] = ""

        for k in data.keys():
            if k in ["topic", "members", "path", "id"]:
                data.pop(k)
                continue
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data
