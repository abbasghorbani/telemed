from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'getInfo/(?P<file_id>\w+)/$', views.get_file_info, name='get-file-info'),
    url(r'download/(?P<filename>[0-9a-zA-Z.]+)/$', views.download, name='download-file'),
    url(r'^(?P<filename>[0-9a-zA-Z_.]+)/$', views.serve_file, name='serve-file'),
]