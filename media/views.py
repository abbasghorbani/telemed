# -*- coding: utf-8 -*-
import os
import json

from django.conf import settings
from django.http import HttpResponse, Http404

from models import File
from api.decorators import check_user_auth
from api.http_response import not_found_response, return_json_data


@check_user_auth
def get_file_info(request, file_id):
    try:
        f = File.objects.get(fileID=file_id)
    except Exception:
        return not_found_response()
    data = f.get_json()
    return return_json_data(data=data)


@check_user_auth
def download(request, filename):
    name = filename.split(".")
    try:
        f = File.objects.get(fileID=name[0])
    except Exception as e:
        print str(e)
        raise Http404

    members = json.loads(f.members)
    file_path = os.path.join(settings.MEDIA_ROOT, f.path, filename)

    if request.CUR_USER.is_superuser or request.CUR_USER.id in members:
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type=f.mime_type)
                response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(file_path)
                return response
    raise Http404


@check_user_auth
def serve_file(request, filename):
    string = filename.split(".")[0]
    name = string.split("_")
    if len(name) == 2:
        name = name[1]
    else:
        name = name[0]

    try:
        f = File.objects.get(fileID=name)
    except Exception as e:
        print str(e)
        raise Http404

    members = json.loads(f.members)
    if request.CUR_USER.is_superuser or request.CUR_USER.id in members:
        file_path = os.path.join(settings.MEDIA_ROOT, f.path, filename)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type=f.mime_type)
                return response
    raise Http404
