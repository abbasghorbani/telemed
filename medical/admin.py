from django.contrib import admin
from models import MedicalRecord


class MedicalRecordAdmin(admin.ModelAdmin):
    list_display = ('patient', 'doctor', 'visit_day', 'start_session', 'end_session', 'desc')
    search_fields = ['patient__username', 'patient__id']
    raw_id_fields = ("patient", "doctor")
    list_filter = ('status',)


admin.site.register(MedicalRecord, MedicalRecordAdmin)
