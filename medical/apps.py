from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class MedicalConfig(AppConfig):
    name = 'medical'
    verbose_name = _('Medical')
