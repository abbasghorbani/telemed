# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date, datetime
import redis

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.core import serializers
from django.db.models.signals import post_save
from celery.task.control import revoke

from api.auth_cache import AuthCache
from api.tools import get_next_url
from chat.models_mongo import Topic

redis_conn = redis.Redis(settings.REDIS_DB, db=settings.REDIS_DB_NUMBER)

PENDING = 0
APPROVE = 1
STARTED = 2
ENDED = 3

MEDICALRECORD_STATUS = (
    (PENDING, _('pending')),
    (APPROVE, _('approve')),
    (STARTED, _('started')),
    (ENDED, _('ended')),
)


class MedicalRecord(models.Model):
    class Meta:
        db_table = 'medicalrecord'
        verbose_name_plural = _("MedicalRecord")
        verbose_name = _("MedicalRecord")

    patient = models.ForeignKey(User, verbose_name=_("patient"), related_name='patient')
    doctor = models.ForeignKey(User, verbose_name=_("doctor"), related_name='doctor')
    visit_day = models.DateField(verbose_name=_("visit day"), default=date.today)
    start_session = models.DateTimeField(verbose_name=_("start session"), null=True, blank=True)
    end_session = models.DateTimeField(verbose_name=_("end session"), null=True, blank=True)
    close_session = models.DateTimeField(verbose_name=_("close session"), null=True, blank=True)
    status = models.IntegerField(verbose_name=_("medicalrecord status"), choices=MEDICALRECORD_STATUS, default=PENDING)
    desc = models.TextField(verbose_name=_("doctor description"), null=True, blank=True)
    create_at = models.IntegerField(verbose_name=_("Create at"), default=0)
    task_id = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Task ID"))

    def save(self, *args, **kwargs):
        if not self.id:
            self.create_at = int(datetime.now().strftime("%s"))
        super(MedicalRecord, self).save(*args, **kwargs)

    def get_json(self, fields=None, exclude=None):

        def need_fields(medical_object):

            final_object = {}
            if fields:
                final_object = {
                    field: medical_object[field] for field in fields
                }
            else:
                final_object = medical_object

            if exclude:
                if not fields:
                    final_object = medical_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        doctor_prof = AuthCache.get_profile_from_id(user_id=self.doctor_id)
        patient_prof = AuthCache.get_profile_from_id(user_id=self.patient_id)

        if doctor_prof is None:
            data['doctor'] = {}
        else:
            data['doctor'] = doctor_prof

        if patient_prof is None:
            data["patient"] = {}
        else:
            data["patient"] = patient_prof

        if self.start_session:
            data["start_session"] = int(self.start_session.strftime("%s"))
        else:
            data["start_session"] = 0

        if self.end_session:
            data["end_session"] = int(self.end_session.strftime("%s"))
        else:
            data["end_session"] = 0

        try:
            topic = Topic.objects.get(medical_record=self.id)
            data["topic"] = topic.get_json()
        except Exception as e:
            print "medical models get_json: {}".format(str(e))
            data["topic"] = {}

        if self.close_session:
            data["close_session"] = int(self.close_session.strftime("%s"))
        else:
            data["close_session"] = 0

        if self.visit_day:
            data["visit_day"] = int(self.visit_day.strftime("%s"))

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data

    def update_close_session(self, description=None):
        self.desc = description
        self.close_session = datetime.now()
        self.status = ENDED
        self.save()

    def update_doctor_visited_list(self):
        key = "doc_{}".format(self.doctor_id)
        value = self.start_session.strftime("%s")
        if redis_conn.zcard(key) == 100:
            redis_conn.zremrangebyrank(key, 0, 0)
        redis_conn.zadd(key, self.patient_id, value)
        return

    def return_mony(self, cancel=True):
        from invoice.models import Invoice
        from user_profile.models import Profile
        from django.contrib.contenttypes.models import ContentType

        content_type = ContentType.objects.only('id').get(app_label="medical", model="medicalrecord")

        try:
            invoice = Invoice.objects.only('amount', 'status').get(object_id=self.id, content_type=content_type)
            if invoice.status == Invoice.PAID:
                Profile.inc_credit(self.patient_id, invoice.amount)
                if self.task_id:
                    revoke(self.task_id, terminate=True)
            if cancel:
                invoice.delete()
            else:
                invoice.status = Invoice.RETURN
                invoice.save()
        except Exception as e:
            print "return_mony function patientID {} -- medicalID {}: {}".format(self.patient_id, self.id, str(e))

    @classmethod
    def update_task_id(cls, medical_id, task_id=None):
        cls.objects.filter(id=medical_id).update(task_id=task_id)

    @classmethod
    def get_medical_record(cls, medical_id):
        try:
            obj = cls.objects.get(id=medical_id)
            return obj
        except Exception as e:
            print str(e), " || get_medical_record medical models"
            return None

    @classmethod
    def doctor_patients_list(cls, user_id, offset=0, limit=10):
        data = []
        key = "doc_{}".format(user_id)
        new_offset = offset + limit

        patients_id = redis_conn.zrange(key, offset, new_offset, desc=True)
        for patient_id in patients_id:
            p = AuthCache.get_profile_from_id(user_id=patient_id)
            if p is None:
                continue
            data.append(p)

        meta = {
            "next": get_next_url(url_name="api-v1-medical-doctor-patients-list", offset=new_offset + 1),
            "limit": 10,
            "total_count": 100
        }
        return data, meta

    @classmethod
    def filter_by_doctor(cls, doctor_id, offset=0, limit=10, order="-end_session"):
        new_offset = offset + limit
        records = cls.objects.filter(doctor_id=doctor_id).order_by(order)[offset:new_offset]
        data = []
        for rec in records:
            data.append(rec.get_json())

        meta = {
            "next": get_next_url(url_name="api-v1-medical-filter-by-doctor", offset=new_offset + 1),
            "limit": 10,
            "total_count": 0
        }
        return data, meta

    @classmethod
    def filter_by_patient(cls, patient_id, offset=0, limit=10, order="-id"):
        new_offset = offset + limit
        records = cls.objects.filter(patient_id=patient_id).order_by(order)[offset:new_offset]
        data = []
        for rec in records:
            data.append(rec.get_json())

        meta = {
            "next": get_next_url(url_name="api-v1-medical-filter-by-patient", offset=new_offset + 1),
            "limit": 10,
            "total_count": 0
        }
        return data, meta

    @classmethod
    def filter_by_both(cls, patient_id, doctor_id, offset=0, limit=10, order="-id"):
        new_offset = offset + limit
        records = cls.objects.filter(patient_id=patient_id, doctor_id=doctor_id).order_by(order)[offset:new_offset]
        data = []
        for rec in records:
            data.append(rec.get_json())

        meta = {
            "next": get_next_url(url_name="api-v1-medical-filter-by-both", offset=new_offset + 1),
            "limit": 10,
            "total_count": 0
        }
        return data, meta

    @classmethod
    def update_status(cls, sender, instance, *args, **kwargs):
        if kwargs['created']:
            cls.objects.filter(id=instance.medical_record).update(status=STARTED)

    @classmethod
    def is_exists_started(cls, doctor_id):
        count = cls.objects.only('id').filter(status=STARTED, doctor_id=doctor_id).count()
        exists = False
        if count > 0:
            exists = True
        return exists


class DrugType(models.Model):
    class Meta:
        db_table = 'drugType'
        verbose_name_plural = _("DrugTypes")
        verbose_name = _("DrugType")

    title = models.CharField(max_length=50, verbose_name=_("title"))
    parent = models.ForeignKey('DrugType', verbose_name=_("Drug parent"), blank=True, null=True)

    def get_json(self, fields=None, exclude=None):

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data


class Prescribe(models.Model):
    class Meta:
        db_table = 'prescribe'
        verbose_name_plural = _("Prescribes")
        verbose_name = _("Prescribe")

    medical = models.ForeignKey(MedicalRecord, verbose_name=_("Medical ID"))
    drug = models.ForeignKey("DrugType", verbose_name=_("Drug type"))
    period = models.CharField(max_length=4, verbose_name=_("Period"))
    count = models.IntegerField(default=0, verbose_name=_("Count"))
    reminder = models.BooleanField(default=False, verbose_name=_("Reminder"))
    task_id = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Task ID"))
    create_at = models.IntegerField(verbose_name=_("Create at"), default=0)

    def save(self, *args, **kwargs):
        if not self.id:
            self.create_at = int(datetime.now().strftime("%s"))
        super(Prescribe, self).save(*args, **kwargs)

    def get_json(self, fields=None, exclude=None):

        def need_fields(prescribe_object):

            final_object = {}
            if fields:
                final_object = {
                    field: prescribe_object[field] for field in fields
                }
            else:
                final_object = prescribe_object

            if exclude:
                if not fields:
                    final_object = prescribe_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['medical'] = self.medical.get_json()
        data['drug'] = self.drug.get_json()

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data

    @classmethod
    def create_prescribe(cls, medical_id, drug_type, period, count):
        try:
            obj = cls.objects.create(medical_id=medical_id, drug_id=drug_type, period=period, count=count)
            return obj
        except Exception as e:
            print str(e)
            return None

    @classmethod
    def get_prescribe_by_id(cls, prescribe_id):
        try:
            obj = cls.objects.get(id=prescribe_id)
            return obj
        except Exception:
            return None

    @classmethod
    def update_task_id(cls, task_id, prescribe_id):
        cls.objects.filter(id=prescribe_id).update(task_id=task_id)


post_save.connect(MedicalRecord.update_status, sender=Topic)
