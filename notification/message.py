# -*- coding: utf-8 -*-
import json
from django.utils.translation import ugettext as _

from api.auth_cache import AuthCache
from api.tools import get_packet_id

from chat.models_mongo import NOTIF_MESSAGE, JOIN_MESSAGE, INVITE, NEW_MEMBER,\
    ANSWER_INVITE, KICK, LEAVE, GET_HISTORY, END_CHAT, ERROR, START_CHAT


def notif_message(message, user_id):
    """
    notif message
    {"packet_id": "455454454",
     "packet_type": 5,
     "payload": {"message": "asd sddf fv dfvdf"}
    }
    """

    data = {
        "packet_id": get_packet_id(user_id),
        "packet_type": NOTIF_MESSAGE,
        "payload": {"message": message},
    }

    json_data = json.dumps(data)
    return json_data


def join_message(topic):
    """
    join data
    {"packet_id": "455454454",
     "packet_type": 3,
     "payload":{
                 "topic": {"id": "45444455444",
                           "title": "test",
                           "hashcode": "hkkjjjkj",
                           "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/" },
                           "publish": "v1/topic/s/hkkjjjkj/" },
                                    }
                }
    }
    """
    topic_data = topic.get_json()
    data = {
        "packet_id": get_packet_id(),
        "packet_type": JOIN_MESSAGE,
        "payload": {"topic": topic_data, "text": _("You joined this chat")},
    }
    json_data = json.dumps(data)

    return json_data


def start_chat_message(medical, text):
    medical_data = medical.get_json()
    data = {
        "packet_id": get_packet_id(),
        "packet_type": START_CHAT,
        "payload": {"medical": medical_data, "text": text},
    }
    json_data = json.dumps(data)

    return json_data


def invite_message(topic, from_id, user_id):
    """
    Invite data
    {"packet_id": "455454454",
     "packet_type": 2,
     "payload":{
                "from_user": {"first_name": "amir",
                              "last_name": "ghorbani",
                              "is_doctor": True,
                              "medical_number": "H121545454"},
                "topic": {"id": "45444455444",
                          "title": "test",
                          "hashcode": "hkkjjjkj",
                          "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/",
                           "publish": "v1/topic/s/hkkjjjkj/"},
                "message": "you invited in topic_title topic",
                "choices": {"yes": True, "No": False}
                }
    }
    """
    """
    response to invite data
    {
        "packet_id": "455454454",
        "packet_type": 3,
        "payload":{
                    "topic": "hashcode",
                    "user_id": "123",
                    "is_accept": True
                }
    }
    """

    topic_data = topic.get_json()
    from_user = create_from_user(user_id=from_id)

    message = "{} {} ?".format(_("would you accept invitation to"), topic.title)
    choices = {_("Yes"): True, _("No"): False}
    data = {
        "packet_id": get_packet_id(user_id),
        "packet_type": INVITE,
        "payload": {"topic": topic_data,
                    "from_user": from_user,
                    "message": message,
                    "choices": choices},
    }
    json_data = json.dumps(data)

    return json_data


def answer_invite_message(topic, new_member, answer):
    """
    Answer Invite data
    {"packet_id": "455454454",
     "packet_type": 4,
     "payload":{
                 "topic": {"id": "45444455444",
                           "title": "test",
                           "hashcode": "hkkjjjkj",
                           "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/"},
                 "new_member": {"first_name": "amir",
                                "last_name": "ghorbani",
                                "is_doctor": True,
                                "medical_number": "H121545454"},
                 "is_accept": False,
                }
    }
    """
    topic_data = topic.get_json()
    new_member_data = AuthCache.get_profile_from_id(user_id=new_member)
    if new_member_data is None:
        new_member_data = {}

    data = {
        "packet_id": get_packet_id(new_member),
        "packet_type": ANSWER_INVITE,
        "payload": {"topic": topic_data,
                    "new_member": new_member_data,
                    "is_accept": answer
                    },
    }
    json_data = json.dumps(data)

    return json_data


def new_members_message(topic, new_members):
    """
    New Member data
    {"packet_id": "455454454",
     "packet_type": 8,
     "payload":{
                 "topic": {"id": "45444455444",
                           "title": "test",
                           "hashcode": "hkkjjjkj",
                           "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/"},
                 "new_members": [{"first_name": "amir",
                          "last_name": "ghorbani",
                          "is_doctor": True,
                          "medical_number": "H121545454"},]
                }
    }
    """

    packet_id = get_packet_id()
    new_members_data = []

    from_user = create_from_user(topic.owner)
    for new_member in new_members:
        new_member_data = AuthCache.get_profile_from_id(user_id=new_member)
        if new_member_data is None:
            continue
        new_members_data.append(new_member_data)

    message = {
        "topic": topic.hashcode,
        "packet_type": NEW_MEMBER,
        "packet_id": packet_id,
        "from_user": from_user,
        "new_members": new_members_data
    }
    msg = create_new_message(message)

    data = {
        "packet_id": packet_id,
        "packet_type": NEW_MEMBER,
        "payload": msg,
    }
    json_data = json.dumps(data)

    return json_data


def kick_message(topic, kick_members):
    """
    Kick data
    {
        "payload": {
            "from_user": {
                "avatar": "dsdsd",
                "last_name": "sdsd",
                "user_id": "sdsd",
                "first_name": ""sdsd,
                "medical_number": "saeed"
            },
            "audio": {},
            "text": "",
            "reply_to_message": "",
            "photo": {},
            "video": {},
            "packet_id": "msg-H1525688838877143-dfv",
            "topic": "world",
            "document": {},
            "voice": {},
            "caption": "",
            "packet_type":6,
            "new_members": [],
            "left_members": [hick members profile data]
        },
        "packet_type": 6,
        "packet_id": "dsdsdsdsdsd"
    }
    """
    from_user = create_from_user(topic.owner)
    packet_id = get_packet_id()
    kick_members_data = []

    for member in kick_members:
        kick_member_data = AuthCache.get_profile_from_id(user_id=member)
        if kick_member_data is None:
            continue
        kick_members_data.append(kick_member_data)

    message = {
        "topic": topic.hashcode,
        "packet_type": KICK,
        "packet_id": packet_id,
        "from_user": from_user,
        "left_members": kick_members_data
    }

    msg = create_new_message(message)

    data = {
        "packet_id": packet_id,
        "packet_type": KICK,
        "payload": msg,
    }
    json_data = json.dumps(data)

    return json_data


def leave_message(topic, leave_member):
    """
    Leave data
    {
        "payload": {
            "from_user": {
                "avatar": "dsdsd",
                "last_name": "sdsd",
                "user_id": "sdsd",
                "first_name": ""sdsd,
                "medical_number": "saeed"
            },
            "audio": {},
            "text": "",
            "reply_to_message": "",
            "photo": {},
            "video": {},
            "packet_id": "msg-H1525688838877143-dfv",
            "topic": "world",
            "document": {},
            "voice": {},
            "caption": "",
            "packet_type":6,
            "new_members": [],
            "left_members": [leave member profile data]
        },
        "packet_type": 9,
        "packet_id": "dsdsdsdsdsd"
    }
    """
    from chat.models_mongo import FromUser

    from_user_json = create_from_user(user_id=leave_member)
    packet_id = get_packet_id(leave_member)

    from_user = FromUser(**from_user_json)
    message = {
        "topic": topic.hashcode,
        "packet_type": LEAVE,
        "packet_id": packet_id,
        "from_user": from_user,
    }

    msg = create_new_message(message)

    data = {
        "packet_id": packet_id,
        "packet_type": LEAVE,
        "payload": msg.get_json(),
    }
    json_data = json.dumps(data)
    return json_data


def history_message(user_id, payload):
    """
    History data
    {"packet_id": "455454454",
     "packet_type": 10,
     "payload":[msg1, msg2]
    """

    data = {
        "packet_id": get_packet_id(user_id=user_id),
        "packet_type": GET_HISTORY,
        "payload": payload
    }
    json_data = json.dumps(data)
    return json_data


def end_chat_request_message(topic):
    """
    End chat data
    {"packet_id": "455454454",
     "packet_type": 11,
     "payload":{
                 "topic": {"id": "45444455444",
                           "title": "test",
                           "hashcode": "hkkjjjkj",
                           "members": [{"first_name": "amir",
                                        "last_name": "ghorbani",
                                        "is_doctor": False,
                                        "medical_number": "H121545454"}],
                           "member_count": 2,
                           "topic": "v1/topic/hkkjjjkj/"},
                 "message": "The length of time allocated to this patient has been completed.
                             Would you like to continue your visit?",
                 "choices": {"yes": True, "No": False}
                }
    }
    """

    topic_data = topic.get_json()
    message = _("The time allocated to this patient has been finished. Want to continue your visit?")
    choices = {_("Yes"): True, _("No"): False}
    data = {
        "packet_id": get_packet_id(),
        "packet_type": END_CHAT,
        "payload": {"topic": topic_data,
                    "message": message,
                    "choices": choices},
    }
    json_data = json.dumps(data)
    return json_data


def error_message(message, packet_id=None):
    """
    Error data
    {"packet_id": "455454454",
     "packet_type": 20,
     "create_at": 1234567890
     "payload":{
                "message": "",
            }
    }
    """

    data = {
        "packet_id": get_packet_id(),
        "packet_type": ERROR,
        "payload": {
                    "message": message,
                    "packet_id": packet_id
                    },
    }
    json_data = json.dumps(data)
    return json_data


# def delete_message(msg_packet_id, topic_data):
#     """
#     Delete data
#     {"packet_id": "455454454",
#      "packet_type": 12,
#      "create_at": 1234567890
#      "payload":{
#                 "packet_id": "1235454545",
#                 "messgae": "Delete this message"
#             }
#     }
#     """

#     data = {
#         "packet_id": get_packet_id(),
#         "packet_type": DELETE,
#         "payload": {
#                     "messgae": _("This message has been deleted"),
#                     "packet_id": msg_packet_id
#                 },
#     }
#     data = {
#         "packet_id": get_packet_id(),
#         "packet_type": DELETE,
#         "payload": {"topic": topic_data,
#                     "message":  _("This message has been deleted"),
#                     "packet_id": msg_packet_id
#                 },
#     }
#     json_data = json.dumps(data)
#     return json_data

def create_new_message(data):
    from datetime import datetime
    from chat.models_mongo import Message

    message = {
        "topic": "",
        "packet_id": "",
        "create_at": datetime.now().strftime("%s"),
        "reply_to_message": "",
        "text": "",
        "caption": "",
        "from_user": {},
        "audio": {},
        "document": {},
        "photo": {},
        "video": {},
        "packet_type": 0,
        "new_members": [],
        "left_members": []
    }
    for key, value in data.items():
            message[key] = value

    new_msg = Message(**message)

    return new_msg


def create_from_user(user_id):
    profile = AuthCache.get_profile_from_id(user_id=user_id)

    if profile:
        from_user = {
            "user_id": profile["user_id"],
            "first_name": profile["first_name"],
            "last_name": profile["last_name"],
            "avatar": profile["avatar"],
            "medical_number": profile["medical_number"],
        }
    else:
        from_user = {}

    return from_user
