# -*- coding: utf-8 -*-
# import requests
# import json

from django.conf import settings
# from django.utils.translation import ugettext as _

import paho.mqtt.publish as publish
# from user_profile.models import PhoneData


class Notification(object):
    MQTT = 1
    SMS = 2
    EMAIL = 3
    GOOGLE = 4
    ALL = 5

    def __init__(self, *args, **kwargs):
        self.to = kwargs.get("to", [])
        self.msgs = kwargs.get("msgs", [])
        self.data = kwargs.get("data", {})
        self.image = kwargs.get("image", {})
        self.notif_type = kwargs.get("notif_type", 1)
        # super(Notification, self).__init__(*args, **kwargs)

    # def gcm_push(self):
    #     try:
    #         up = PhoneData.objects.get(user_id=int(self.to["user_id"]))
    #         if not up.google_token or up.google_token == 'NONE':
    #             return
    #     except PhoneData.DoesNotExist:
    #         return

    #     if self.action_type == self.START_CHAT:
    #         json_data = {
    #             "notification": {
    #                 "text": _("Start meeting with doctor {}".format("ghorbani")),
    #                 "medical": self.data["medical"],
    #                 "type": settings.START_CHAT,
    #             }
    #         }
    #         data = {
    #             "to": up.google_token,
    #             "priority": "high",
    #             "data": json_data,
    #         }

    #         headers = {
    #             'Content-Type': 'application/json',
    #             'Authorization': 'key=AIzaSyBb2hrHjni36s1UG70f3t22s9AZ7LZmrow'}

    #         res = requests.post(url='https://android.googleapis.com/gcm/send',
    #                             data=json.dumps(data),
    #                             headers=headers,
    #                             timeout=2)
    #         print data
    #         print res, res.content

    def send(self):
        if self.notif_type == self.MQTT:
            publish.multiple(self.msgs,
                             hostname=settings.MQTT_HOST,
                             port=settings.MQTT_PORT,
                             keepalive=settings.MQTTT_KEEPALIVE,
                             auth=settings.MQTT_AUTH)