# -*- coding: utf-8

from telemed.celery import app
from notif import Notification


@app.task(name="notif.send_notif")
def send_notif(msgs):
    sender = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    sender.send()
    print "--------------------send notification ----------------"
