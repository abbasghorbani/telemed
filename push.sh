#!/bin/sh

send(){
    git pull origin master
	git add .
	git commit -m "changes"
    git push origin master
	ssh telemed@81.91.158.136 -p 6768 "cd python/telemed && git pull origin master && touch reload"
    ssh root@81.91.158.136 -p 6768 "supervisorctl reload"
}

case $1 in
    send)
        send
        ;;

    *)
        echo "send\t for push and reload server"
    ;;
esac