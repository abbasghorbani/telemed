# -*- coding: utf-8 -*-
from telemed.celery import app
from notification.notif import Notification
from notification.message import notif_message
from api.tools import get_user_topic, local2UTC
from django.utils import timezone
from datetime import timedelta


@app.task(name="reminder.reminder_prescibe")
def reminder_prescibe(prescibe_message, prescibe_id, user_id):
    from medical.models import Prescribe
    try:
        obj = Prescribe.objects.only('count', 'period', 'reminder').get(id=prescibe_id)
    except Exception:
        print "prescibe_id {} not found".format(prescibe_id)
        return

    # Checking reminder for drug is off or on
    if not obj.reminder:
        return

    data = notif_message(prescibe_message, user_id)
    topic = get_user_topic(user_id)
    msgs = []
    msgs.append({
        'topic': topic,
        'payload': data,
        'qos': 1,
        'retain': False,
    })
    sender = Notification(**{"msgs": msgs, "notif_type": Notification.MQTT})
    sender.send()

    # expire_second = obj.period * 60
    expire_second = timezone.now() + timedelta(seconds=obj.period * 60)

    new_count = obj.count - 1
    if new_count > 0:
        res = reminder_prescibe.apply_async(args=[prescibe_message, new_count, obj.period, user_id], eta=local2UTC(expire_second))
        # Update task_id
        Prescribe.update_task_id(prescibe_id=prescibe_id, task_id=res.task_id)
        print "*********start new task {}".format(new_count)
    else:
        print "********************ENDED********************"
