# -*- coding: utf-8 -*-
from reminder.tasks import reminder_prescibe
from datetime import datetime


def set_drug_reminder(prescribe_message, prescribe_id, user_id, start_time="now"):
    from medical.models import Prescribe
    now_time = datetime.now()

    if start_time == "now":
        start_time = now_time
    elif start_time < now_time:
        return False, "error in start time"

    diff = start_time - now_time
    total_secodns = ((diff.days * 86400) + diff.seconds)
    res = reminder_prescibe.apply_async(args=[prescribe_message, prescribe_id, user_id],
                                        countdown=total_secodns)
    # update task_id
    Prescribe.update_task_id(prescribe_id=prescribe_id, task_id=res.task_id)

    return True, "reminder started"


def cancel_drug_reminder(task_id):
    from telemed.celery import app
    try:
        app.control.revoke(task_id)
    except Exception as e:
        print str(e), "|| cancel_drug_reminder function error"
