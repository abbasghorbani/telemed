# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext as _

# Register your models here.
from models import Ticket, Message
from datetime import datetime


class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'user', 'status', 'ticket_number', 'create_time')
    search_fields = ['ticket_number']
    raw_id_fields = ("user",)
    list_filter = ("status",)

    def create_time(self, obj):
        return datetime.fromtimestamp(int(obj.create_at))
    create_time.short_description = _('Create time')
    create_time.allow_tags = True


class MessageAdmin(admin.ModelAdmin):
    list_display = ('ticket_id', 'user', 'text', 'attach', 'create_time')
    search_fields = ['ticket']
    raw_id_fields = ("user", "ticket")

    def create_time(self, obj):
        return datetime.fromtimestamp(int(obj.create_at))
    create_time.short_description = _('Create time')
    create_time.allow_tags = True


admin.site.register(Ticket, TicketAdmin)
admin.site.register(Message, MessageAdmin)
