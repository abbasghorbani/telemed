# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class TicketingConfig(AppConfig):
    name = 'ticketing'
    verbose_name = _('Ticketing')
