# -*- coding: utf-8 -*-
from django.forms import ModelForm
from models import Ticket, Message


class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = ('title',)


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ('text', 'attach')
