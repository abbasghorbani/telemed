# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import time
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from api.tools import random_with_N_digits, simple_user_json, media_abs_url
from django.core import serializers


def ticket_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    ticket_prefix = "ticketing/"
    return '/'.join([ticket_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


class Ticket(models.Model):
    class Meta:
        db_table = 'ticket'
        verbose_name_plural = _("tickets")
        verbose_name = _("ticket")

    WAITING_FOR_ANSWER = 1
    PENDING = 2
    CLOSE = 3

    TICKET_STSTUS = (
        (PENDING, _('pending')),
        (WAITING_FOR_ANSWER, _('wating for answer')),
        (CLOSE, _('close')),
    )

    title = models.CharField(max_length=150, verbose_name=_("Title"))
    user = models.ForeignKey(User, verbose_name=_("User"))
    status = models.IntegerField(verbose_name=_("Status"), choices=TICKET_STSTUS, default=WAITING_FOR_ANSWER)
    ticket_number = models.IntegerField(verbose_name=_("Ticket number"), db_index=True)
    create_at = models.IntegerField(verbose_name=_("Create time"))

    def save(self, *args, **kwargs):
        if not self.id:
            self.create_at = int(datetime.now().strftime("%s"))
            # Create ticket number
            key_exists = True
            while key_exists:
                code = "{}".format(random_with_N_digits(5))
                key_exists = Ticket.objects.filter(ticket_number=code).exists()
                if not key_exists:
                    self.ticket_number = code
        super(Ticket, self).save(*args, **kwargs)

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data["id"] = serialized_obj[0]['pk']
        data["user"] = simple_user_json(self.user)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data


class Message(models.Model):
    class Meta:
        db_table = 'message'
        verbose_name_plural = _("messages")
        verbose_name = _("message")

    text = models.TextField(verbose_name=_("Text"))
    ticket = models.ForeignKey(Ticket, verbose_name=_("Ticket"))
    create_at = models.IntegerField(verbose_name=_("Create time"))
    attach = models.ImageField(upload_to=ticket_file_name, null=True, blank=True, verbose_name=_("Attach"))
    user = models.ForeignKey(User, verbose_name=_("User"))

    def save(self, *args, **kwargs):
        if not self.id:
            self.create_at = int(datetime.now().strftime("%s"))
        super(Message, self).save(*args, **kwargs)

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data["id"] = serialized_obj[0]['pk']
        data["user"] = simple_user_json(self.user)
        if self.attach:
            data["attach"] = media_abs_url(self.attach.url)
        data["ticket"] = self.ticket.get_json()

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data
