from django.contrib import admin
from models import Profile, ApiKey, ActivationCode, Doctor, Document, EducationType, Education, Address, Country


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'avatar', 'credit', 'phone')
    search_fields = ['first_name', 'last_name', 'phone']
    raw_id_fields = ("user",)
    list_filter = ("is_doctor",)

    # def image_tag(self, obj):
    #     if obj.avatar:
    #         return '<img src="{}" />'.format(obj.avatar.url)
    #     else:
    #         return u'<img src="" />'
    # image_tag.short_description = 'Image'
    # image_tag.allow_tags = True


class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ('user', 'token', 'create_at')
    search_fields = ['user__username', 'user__id']
    raw_id_fields = ("user",)


class ActivationCodeAdmin(admin.ModelAdmin):
    list_display = ('code', 'phone', 'create_at', "is_active")
    search_fields = ['phone']
    list_filter = ("is_active",)


class DoctorAdmin(admin.ModelAdmin):
    list_display = ('medical_license', 'user_id', 'is_approve')
    search_fields = ['user__username', "user__id"]
    raw_id_fields = ("user",)
    list_filter = ("is_approve",)


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('doctor', 'name', 'path')
    search_fields = ['doctor__id']
    raw_id_fields = ("doctor",)


class EducationTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent',)
    search_fields = ['name']
    raw_id_fields = ("parent",)


class EducationAdmin(admin.ModelAdmin):
    list_display = ('univercity', 'country', 'education_type', 'doctor')
    search_fields = ['doctor__id']
    raw_id_fields = ("country", "education_type", "doctor")


class AddressAdmin(admin.ModelAdmin):
    list_display = ('country', 'address', 'user_id')
    search_fields = ['address']
    raw_id_fields = ("country", "user")


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'search', 'parent')
    search_fields = ['title']
    raw_id_fields = ("parent",)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(ActivationCode, ActivationCodeAdmin)
admin.site.register(Doctor, DoctorAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(EducationType, EducationTypeAdmin)
admin.site.register(Education, EducationAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(Country, CountryAdmin)
