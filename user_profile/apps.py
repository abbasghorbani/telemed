from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext as _


class UserProfileConfig(AppConfig):
    name = 'user_profile'
    verbose_name = _('Profile')
