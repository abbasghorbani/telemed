# -*- coding: utf-8 -*-

from django.forms import ModelForm

from models import Profile, Address, Doctor, Document, Education, DoctorRequest


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'birthdate', 'gender', 'married', 'height', 'weight')


class AddressForm(ModelForm):
    class Meta:
        model = Address
        fields = ('country', 'postal_code', 'address', 'phone')


class DoctorForm(ModelForm):
    class Meta:
        model = Doctor
        fields = ('license_type', 'medical_license', "expertise")


class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ('name', 'path')


class EducationForm(ModelForm):
    class Meta:
        model = Education
        fields = ('univercity', 'country', 'education_type', 'file_path', 'desc')


class DoctorRequestForm(ModelForm):
    class Meta:
        model = DoctorRequest
        fields = ('text',)


class UpdateAvatarForm(ModelForm):
    class Meta:
        model= Profile
        fields = ('avatar',)