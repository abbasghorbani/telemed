# -*- coding:utf-8 -*-
import os
from django.core.management.base import BaseCommand
# from django.db import connection
from user_profile.models import Country

BASE_DIR = os.path.dirname(__file__)
csv_file = os.path.join(BASE_DIR, 'IR_city_states.csv')


class Command(BaseCommand):

    def handle(self, *args, **options):
        # cursor = connection.cursor()
        # cursor.execute("TRUNCATE TABLE `shop_address`")
        # cursor.execute("TRUNCATE TABLE `shop_city`")
        local_country, created = Country.objects.get_or_create(title=u'ایران', parent=None)
        with open(csv_file) as f:
            cs = f.readlines()
            cur_state = 0
            cur_state_object = None
            for c in cs:
                state_id, state, city_id, city = c.split(',')
                if cur_state != state_id:
                    cur_state = state_id
                    cur_state_object, created = Country.objects.get_or_create(title=state, search=state, parent=local_country)

                cur_city_object, created = Country.objects.get_or_create(title=city, search=city, parent=cur_state_object)
