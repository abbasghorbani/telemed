# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time
import os
import uuid

from datetime import datetime, date
# from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.core.cache import cache
from django.db.models import F
# from django.core.mail import send_mail
from django.core import serializers
from django.db import models
from django.db.models.signals import post_save
from mongoengine.queryset.visitor import Q

from api.tools import simple_user_json, send_sms, random_with_N_digits, get_user_topic

NOTSET = 0
OTHER = 10


MALE = 1
FEMALE = 2

NOTMARRIED = 1
MARRIED = 2

MEDICAL = 1
PHARMACIST = 2
DENTIST = 3
PSYCHOLOGIST = 4
PSYCHIATRIST = 5

QUARTER = 15
HALF = 30
HOUR = 60
HOUR_HALF = 45

APPOINTMENT_TYPE = (
    (QUARTER, _('quarter')),
    (HALF, _('half')),
    (HOUR, _('hour')),
    (HOUR_HALF, _('hour and half')),
)

GENDER = (
    (NOTSET, _('not set')),
    (MALE, _('male')),
    (FEMALE, _('female')),
    (OTHER, _('other')),
)

MARRIED_TYPE = (
    (NOTSET, _('not set')),
    (NOTMARRIED, _('not married')),
    (MARRIED, _('married')),
)

LICENSE_TYPE = (
    (NOTSET, _('not set')),
    (MEDICAL, _('medical')),
    (PHARMACIST, _('pharmacist')),
    (DENTIST, _('dentist')),
    (PSYCHOLOGIST, _('psychologist')),
    (PSYCHIATRIST, _('psychiatrist')),
)


def avatar_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    avatars_prefix = "avatars/"
    return '/'.join([avatars_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def document_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    avatars_prefix = "documents/"
    return '/'.join([avatars_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def education_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    avatars_prefix = "education/"
    return '/'.join([avatars_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


class Profile(models.Model):
    class Meta:
        db_table = 'profile'
        verbose_name_plural = _("profiles")
        verbose_name = _("profile")

    first_name = models.CharField(max_length=250,
                                  verbose_name=_("first name"),
                                  null=True, blank=True)
    last_name = models.CharField(max_length=250,
                                 verbose_name=_("last name"),
                                 null=True, blank=True)
    avatar = models.ImageField(upload_to=avatar_file_name, default="media/default.jpg",
                               null=True, blank=True,
                               verbose_name=_("Avatar"))
    user = models.OneToOneField(User, verbose_name=_("User"))
    credit = models.IntegerField(default=0, verbose_name=_("Credit"))
    height = models.FloatField(verbose_name=_("height"), blank=True, null=True)
    weight = models.FloatField(verbose_name=_("weight"), blank=True, null=True)
    is_doctor = models.BooleanField(default=False, verbose_name=_("is doctor"))
    birthdate = models.DateField(verbose_name=_("birthdate"), default=date.today)
    gender = models.IntegerField(verbose_name=_('gender'), choices=GENDER, default=NOTSET)
    married = models.IntegerField(verbose_name=_('married'), choices=MARRIED_TYPE, default=NOTSET)
    phone = models.CharField(max_length=12, verbose_name=_("Phone"), db_index=True, null=True)
    medical_number = models.CharField(max_length=17, verbose_name=_("medical number"), db_index=True, null=True)
    ssn = models.CharField(max_length=10, verbose_name=_("ssn"), db_index=True, null=True)

    def get_json(self, fields=None, exclude=None):

        def need_fields(profile_object):

            final_object = {}
            if fields:
                final_object = {
                    field: profile_object[field] for field in fields
                }
            else:
                final_object = profile_object

            if exclude:
                if not fields:
                    final_object = profile_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['user'] = simple_user_json(user=self.user)
        data["topic"] = get_user_topic(self.user_id)

        if self.birthdate:
            data["birthdate"] = int(self.birthdate.strftime("%s"))

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data

    @classmethod
    def inc_credit(cls, user_id, amount):
        from api.auth_cache import AuthCache
        from invoice.models import CreditLog
        Profile.objects.filter(user_id=user_id)\
            .update(credit=F('credit') + amount)

        # Add log into creditLog model
        CreditLog.create_credit_log(user_id=user_id, amount=amount,
                                    status=CreditLog.COMPLETED, mode=CreditLog.INCREASE)

        AuthCache.remove_profile_by_id(user_id=user_id)

    @classmethod
    def dec_credit(cls, user_id, amount):
        from api.auth_cache import AuthCache
        from invoice.models import CreditLog
        profile = AuthCache.get_profile_from_id(user_id=user_id)

        try:
            if profile["credit"] - amount < 0:
                Profile.objects.filter(id=profile["id"]).update(credit=0)
            else:
                Profile.objects.filter(id=profile["id"]).update(credit=F('credit') - amount)
        except Exception as e:
            print str(e), "model user_profile: dec_credit function"
            return False

        # Add log into creditLog model
        CreditLog.create_credit_log(user_id=user_id, amount=amount,
                                    status=CreditLog.COMPLETED, mode=CreditLog.DECREASE)

        AuthCache.remove_profile_by_id(user_id=user_id)
        return True

    @classmethod
    def create_superuser(cls, sender, instance, *args, **kwargs):
        if kwargs['created'] and instance.is_superuser:
            # Create profile
            cls.create_user_profile(instance.username, instance.username, "", instance, "", True)

    @classmethod
    def create_user_profile(cls, first_name, last_name, phone, user, ssn, is_doctor):
        key_exists = True
        key = None
        profile = None
        message = ""
        while key_exists:
            key = "H" + ''.join(datetime.now().strftime("%s%f"))
            key_exists = cls.objects.filter(medical_number=key).exists()
            if not key_exists:
                try:
                    profile = cls.objects.create(user=user,
                                                 first_name=first_name,
                                                 last_name=last_name,
                                                 ssn=ssn,
                                                 phone=phone,
                                                 medical_number=key,
                                                 is_doctor=is_doctor)
                except Exception as e:
                    print str(e), " || Profile create_user_profile function"
                    message = _("Error in profile creation")
                    return False, message, profile
        return True, message, profile

    @classmethod
    def update_username(cls, sender, instance, *args, **kwargs):
        if kwargs["created"] and not instance.user.is_superuser:
            instance.user.username = instance.medical_number
            instance.user.save()

    @classmethod
    def is_exist_ssn(cls, ssn):
        return cls.objects.filter(ssn__iexact=ssn).exists()

    @classmethod
    def is_exist_phone(cls, phone):
        return cls.objects.filter(phone__iexact=phone).exists()


class ApiKey(models.Model):
    class Meta:
        db_table = 'api_key'
        verbose_name_plural = _("ApiKey")
        verbose_name = _("ApiKey")

    user = models.ForeignKey(User, verbose_name=_("User"))
    token = models.CharField(max_length=255, unique=True, verbose_name=_("Token"))
    create_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create at"))

    def get_json(self):

        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    @classmethod
    def create_token(cls, sender, instance, *args, **kwargs):
        if kwargs['created']:
            key_exists = True
            key = None
            while key_exists:
                key = uuid.uuid4().hex
                key_exists = cls.objects.filter(token=key).exists()
                if not key_exists:
                    try:
                        cls.objects.create(user=instance, token=key)
                    except Exception as e:
                        print str(e)

    @classmethod
    def rebuild_token(cls, user):
        key_exists = True
        key = None
        while key_exists:
            key = uuid.uuid4().hex
            key_exists = cls.objects.filter(token=key).exists()
            if not key_exists:
                try:
                    apikey = cls.objects.create(user=user, token=key)
                    return apikey
                except Exception as e:
                    print str(e)
        return False

    @classmethod
    def update_token(cls, user_id):
        key_exists = True
        key = None
        while key_exists:
            key = uuid.uuid4().hex
            key_exists = cls.objects.filter(token=key).exists()
            if not key_exists:
                try:
                    cls.objects.filter(user_id=user_id).update(token=key)
                    return key
                except Exception as e:
                    print str(e)
        return None


class ActivationCode(models.Model):
    class Meta:
        db_table = 'activation_code'
        verbose_name_plural = _("ActivationCodes")
        verbose_name = _("ActivationCode")

    # user = models.ForeignKey(User, verbose_name=_("User"))
    phone = models.CharField(max_length=12, verbose_name=_("phone"), unique=True)
    code = models.CharField(max_length=255, unique=True, verbose_name=_("Code"))
    create_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create at"))
    is_active = models.BooleanField(default=False, verbose_name=_("is active"))

    @classmethod
    def create_code(cls, phone, count, resend=False):
        key_exists = True
        code = None

        try:
            exists_phone = cls.objects.only('is_active', 'code').get(phone=phone)
            if not exists_phone.is_active:
                # Send SMS
                send_sms(phone, exists_phone.code, resend)
                # Set attempts user count
                timestamp = datetime.now().strftime("%s")
                count += 1
                new_string = "{}:{}".format(count, timestamp)
                cache.set(phone, new_string)
                return True

            # Remove activation code if is_active=True
            exists_phone.delete()
        except cls.DoesNotExist:
            pass
        except Exception as e:
            print str(e), "|| ActivationCode model, create_code function"
            return False

        while key_exists:
            code = "{}".format(random_with_N_digits(4))
            key_exists = cls.objects.filter(code=code).exists()
            if not key_exists:
                try:
                    cls.objects.create(phone=phone, code=code)
                    # Send SMS
                    send_sms(phone, code, resend)
                    # Set attempts user count
                    timestamp = datetime.now().strftime("%s")
                    count += 1
                    new_string = "{}:{}".format(count, timestamp)
                    cache.set(phone, new_string)
                except Exception as e:
                    msg = "|| ActivationCode model, create_code function"
                    print str(e), msg
                    return False
        return True


class Doctor(models.Model):
    class Meta:
        db_table = 'doctor'
        verbose_name_plural = _("doctor")
        verbose_name = _("doctor")

    BUSY = 1
    AVAILABLE = 2
    DOCTOR_STATUS = (
        (BUSY, _("Busy")),
        (AVAILABLE, _("Available"))
    )
    user = models.ForeignKey(User, verbose_name=_("User"), related_name='doctor_profile')
    medical_license = models.CharField(verbose_name=_("medical license"), max_length=20, blank=True, null=True)
    is_approve = models.BooleanField(verbose_name=_("is approve"), default=False)
    desc = models.TextField(verbose_name=_("description"), blank=True, null=True)
    license_type = models.IntegerField(verbose_name=_("license type"), choices=LICENSE_TYPE, default=NOTSET)
    appointment_type = models.IntegerField(verbose_name=_("appointment type"), choices=APPOINTMENT_TYPE, default=QUARTER)
    expertise = models.CharField(verbose_name=_("expertise"), max_length=100, default="")
    is_available = models.BooleanField(verbose_name=_("Is available"), default=False)
    status = models.IntegerField(verbose_name=_("Status"), choices=DOCTOR_STATUS, default=AVAILABLE)

    def save(self, *args, **kwargs):
        from api.auth_cache import AuthCache

        if self.pk is not None:
            cur_data = Doctor.objects.get(id=self.pk)
        else:
            cur_data = None
        if cur_data is not None and self.is_approve != cur_data.is_approve:
            AuthCache.remove_profile_by_id(self.user_id)
        return super(Doctor, self).save(*args, **kwargs)

    def get_json(self, fields=None, exclude=None):
        def need_fields(profile_object):

            final_object = {}
            if fields:
                final_object = {
                    field: profile_object[field] for field in fields
                }
            else:
                final_object = profile_object

            if exclude:
                if not fields:
                    final_object = profile_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = {}
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['profile'] = self.user.profile.get_json(fields=['first_name', 'last_name', 'avatar', 'gender'])

        if data['appointment_type'] == HOUR_HALF:
            data['appointment_type'] = [HALF, HOUR]
        else:
            data['appointment_type'] = [data['appointment_type']]

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        data = need_fields(data)
        return data

    def next_available(self, appointment_type=15):
        from appointment.models_mongo import DoctorFreeDate
        from api.tools import get_current_time_int, get_time_int
        cur_time_int = get_current_time_int(appointment_type) + 1

        if appointment_type == HOUR_HALF:
            appointment_type = HALF

        try:
            if appointment_type == QUARTER:
                next_obj = DoctorFreeDate.objects.only("quarter", "day").filter(Q(user=self.user_id, day=datetime.today().date(), quarter__gt=cur_time_int) | Q(user=self.user_id, day__gt=datetime.today().date())).order_by("day").first()
            elif appointment_type == HALF:
                next_obj = DoctorFreeDate.objects.only("half", "day").filter(Q(user=self.user_id, day=datetime.today().date(), half__gt=cur_time_int) | Q(user=self.user_id, day__gt=datetime.today().date())).order_by("day").first()
            elif appointment_type == HOUR:
                next_obj = DoctorFreeDate.objects.only("hour", "day").filter(Q(user=self.user_id, day=datetime.today().date(), hour__gt=cur_time_int) | Q(user=self.user_id, day__gt=datetime.today().date())).order_by("day").first()

            if next_obj is None:
                return ""

            if appointment_type == QUARTER:
                hours = next_obj.quarter
            if appointment_type == HALF:
                hours = next_obj.half
            if appointment_type == HOUR:
                hours = next_obj.hour
            hours.sort()
            for hour in hours:
                if next_obj.day.date() > datetime.today().date() or hour > cur_time_int:
                    hour_format = get_time_int(hour, appointment_type=appointment_type, single='start')
                    next_time = datetime.strptime(next_obj.day.strftime('%Y-%m-%d') + " " + hour_format, "%Y-%m-%d %H:%M")
                    return next_time.strftime('%Y-%m-%d %H:%M')
            return ""
        except Exception as e:
            print str(e), "|| next available, user_profile models"
            return ""

    @classmethod
    def update_availability(cls, user_id, is_available):
        cls.objects.filter(user_id=user_id).update(is_available=is_available)

    @classmethod
    def update_status(cls, user_id, status):
        cls.objects.filter(user_id=user_id).update(status=status)


class Document(models.Model):
    class Meta:
        db_table = 'document'
        verbose_name_plural = _("document")
        verbose_name = _("document")

    name = models.CharField(max_length=50, verbose_name=_("file name"), blank=True, null=True)
    path = models.ImageField(upload_to=document_file_name, verbose_name=_("document"))
    doctor = models.ForeignKey(Doctor, verbose_name=_("file owner"))

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    @classmethod
    def delete_document(cls, obj_id):
        try:
            cls.objects.filter(id=obj_id).delete()
            return True
        except Exception as e:
            print str(e), " || delete_document function"
            return False

    @classmethod
    def get_document(cls, obj_id, doctor_id):
        try:
            doc = cls.objects.get(id=obj_id, doctor_id=doctor_id)
            return True, doc
        except Exception as e:
            print str(e), " || get_document function"
            return False, None


class EducationType(models.Model):
    class Meta:
        db_table = 'education_type'
        verbose_name_plural = _("education type")
        verbose_name = _("education type")

    name = models.CharField(max_length=100, verbose_name=_("education type"))
    parent = models.ForeignKey("EducationType", null=True, blank=True, verbose_name=_("parent"))

    @classmethod
    def get_type(cls, obj_id):
        if obj_id == 0:
            obj_id = None
        edu_types = cls.objects.only('name', 'id').filter(parent=obj_id)
        edu_list = []
        for edu in edu_types:
            edu_list.append(edu.get_json())
        return edu_list

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data


class Country(models.Model):
    class Meta:
        db_table = 'country'
        verbose_name_plural = _("country")
        verbose_name = _("country")

    title = models.CharField(max_length=50, verbose_name=_("title"))
    parent = models.ForeignKey('Country', verbose_name=_("country parent"), blank=True, null=True)
    search = models.CharField(max_length=100, verbose_name=_("for seach word"))

    def __unicode__(self):
        if self.parent:
            return self.parent.title + '-' + self.title
        else:
            return self.title

    def get_json(self, fields=None, exclude=None):

        def need_fields(country_object):

            final_object = {}
            if fields:
                final_object = {
                    field: country_object[field] for field in fields
                }
            else:
                final_object = country_object

            if exclude:
                if not fields:
                    final_object = country_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = []
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data = need_fields(data)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data

    @classmethod
    def get_states(cls, obj_id):
        if obj_id == 0:
            obj_id = None
        states = cls.objects.only('title', 'id').filter(parent=obj_id)
        state_list = []
        for state in states:
            state_list.append(state.get_json())
        return state_list


class Education(models.Model):
    class Meta:
        db_table = 'education'
        verbose_name_plural = _("education")
        verbose_name = _("education")

    univercity = models.CharField(max_length=250, verbose_name=_("univercity"))
    country = models.ForeignKey(Country, verbose_name=_("country"))
    education_type = models.ForeignKey(EducationType, verbose_name=_("education type"))
    desc = models.TextField(verbose_name=_("description"), null=True, blank=True)
    doctor = models.ForeignKey(User, verbose_name=_("doctor"))
    file_path = models.ImageField(upload_to=education_file_name,
                                  default=None,
                                  null=True, blank=True,
                                  verbose_name=_("Education file"))

    @classmethod
    def get_education(cls, obj_id, doctor_id):
        try:
            edu = cls.objects.get(id=obj_id, doctor_id=doctor_id)
            return True, edu
        except Exception as e:
            print str(e), " || get_education function"
            return False, None

    @classmethod
    def delete_education(cls, obj_id):
        try:
            cls.objects.filter(id=obj_id).delete()
            return True
        except Exception as e:
            print str(e), " || delete_education function"
            return False

    def get_json(self):

        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data


class Address(models.Model):
    class Meta:
        db_table = 'address'
        verbose_name_plural = _("address")
        verbose_name = _("address")

    country = models.ForeignKey(Country, verbose_name=_("country"), null=True, blank=True)
    postal_code = models.CharField(max_length=20, verbose_name=_("postal code"), null=True, blank=True)
    address = models.CharField(max_length=500, verbose_name=_("address"), null=True, blank=True)
    phone = models.CharField(max_length=20, verbose_name=_("phone"), null=True, blank=True)
    lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    user = models.ForeignKey(User, verbose_name=_("address owner"))

    def get_json(self, fields=None, exclude=None):

        def need_fields(address_object):

            final_object = {}
            if fields:
                final_object = {
                    field: address_object[field] for field in fields
                }
            else:
                final_object = address_object

            if exclude:
                if not fields:
                    final_object = address_object

                for field in exclude:
                    try:
                        final_object.pop(field)
                    except Exception:
                        pass

            return final_object

        data = []
        serialized_obj = serializers.serialize('python', [self])

        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        data['user'] = simple_user_json(user=self.user)
        data = need_fields(data)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return data


class PhoneData(models.Model):
    class Meta:
        db_table = 'phone_Data'
        verbose_name_plural = _("phone_Datas")
        verbose_name = _("phone_Data")
    user = models.ForeignKey(User, verbose_name=_("User"))
    hashcode = models.CharField(max_length=50, verbose_name=_("Hashcode"), db_index=True)
    extra_data = models.TextField(verbose_name=_("Extra data"), default="")
    push_token = models.CharField(max_length=500, verbose_name=_("Push token"), default="")
    logout = models.BooleanField(default=False, verbose_name=_("Logout"))

    @classmethod
    def update_phone_data(cls, user_id, data):
        import json
        try:
            json_data = json.loads(data)
            hashcode = json_data["hashcode"]
        except Exception as e:
            print str(e)
            return

        try:
            obj = cls.objects.get(user_id=user_id)
            obj.extra_data = data
            obj.hashcode = hashcode
            obj.save()
        except cls.DoesNotExist:
            obj = cls.objects.create(user_id=user_id, hashcode=hashcode, extra_data=data)


class DoctorRequest(models.Model):
    class Meta:
        db_table = 'doctor_request'
        verbose_name_plural = _("doctor_requests")
        verbose_name = _("doctor_request")

    user = models.ForeignKey(User, verbose_name=_("User"))
    text = models.TextField(verbose_name=_("Text"), default="")


post_save.connect(Profile.update_username, sender=Profile)
post_save.connect(ApiKey.create_token, sender=User)
post_save.connect(Profile.create_superuser, sender=User)
# post_save.connect(ActivationCode.create_code, sender=User)
